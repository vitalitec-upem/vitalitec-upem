# LightmedWebClient

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.0.

## Documentation

https://angular.io/

## Installation

Premièrement, il vous faut NodeJS, il est recommandé d'avoir une version supérieure à 10.0.0

https://nodejs.org/en/

Une fois NodeJS installé, veillez à avoir npm dans votre PATH, testez simplement dans un nouveau terminal que la commande

`$> npm -v`

fonctionne.

* Allez dans le répertoire "lightmed-web-client" situé dans vitalitec-upem/web-client/
* Lancez la commande : `$> npm install`

Le projet est maintenant correctement installé.


## Lancer le serveur

* Une fois l'installation terminée, lancez la commande : `$> npm start`
* Ouvrez votre navigateur à l'adresse `http://localhost:4200/` pour accéder à l'application.
* A chaque modification des sources, npm se chargera de recharger l'application (donc pas besoin de refaire npm start à chaque modification)

## Utilisez Material Design

https://material.angular.io/

## Emplacement des sources et explications

* Les sources se trouvent dans src/app
* Les images, et assets (présentes et à venir) se trouvent dans assets/img
* Chaque composant de l'application possède un fichier .ts avec un fichier .html & .scss associé.
* Les fichiers spec.ts sont générés automatiquement et servent aux éventuels tests. (pas besoin d'y toucher pour l'instant)
* Les composants de l'application se trouvent dans src/app/components
* Les services de l'application se trouvent dans src/app/services
* Le router est dans src/app-routing.module.ts
* Le gestionnaire de modules/composants/services est dans src/app.module.ts

## Ajouter un composant

La commande `ng generate component components/[nom du composant]` va générer le composant du nom de votre choix, mais attention, ça ne suffit pas.

Une fois votre composant ajouté, vous allez surement vouloir le lier à un autre composant, par exemple au composant du menu, ou autre.

Pour ce faire, il faut définir une route dans le fichier src/app-routing.module.ts (le router) vers votre composant.

`{ path: 'home', component: DashboardComponent, canActivate: [AuthGuardService] }`

Ceci nous définit la route 'home' donc on peut y accéder via `http://localhost:4200/home`, et cette route nous amène sur le .html lié au composant "DashboardComponent", donc src/app/components/dashboard/dashboard.component.html

Et finalement, le "canActivate" est utilisé pour spécifier qu'on a besoin d'etre authentifié pour accéder à cette route.

Chaque composant en .ts n'a qu'un seul constructor.

## Les services

`constructor(private http: HttpService) { }`

Pour faire une requete vers notre API, faites :
```
this.http.get('/admin/getUnits', params).then((resp) => {
    console.log(resp);
});
```
Ceci fonctionne de la meme manière avec POST mais remplacer get par post. (et evidemment, changez la route par une autre si vous voulez appeler une autre route.)

## Le storage

On stocke l'utilisateur en cours dans la mémoire du navigateur, ainsi vous pouvez le récupérer.

`constructor(private router: Router, private http: HttpService, private local: LocalStorageService) { }`

dans le .ts de votre composant vous permet d'instancier le router, le service http permettant de faire des requetes vers notre API et le storage permettant d'accéder à la mémoire du navigateur.

Pour récupérer l'utilisateur en cours, si votre storage s'appelle `local` il vous faut faire : 

`const user = this.local.get('user');`

## Problèmes lors des requetes

Vous allez peut etre rencontrer des erreurs au niveau des CORS à chaque appel vers l'API, dans ce cas deux choses :
* Dans le fichier proxy.conf.js à la racine du projet, ajoutez la route que vous essayez d'appeler si elle ne s'y trouve pas (la racine de l'appel suffit)
* Au niveau de votre navigateur, lancez Chrome de cette manière : `google-chrome --disable-web-security --user-data-dir="/tmp"`
* Si `google-chrome` ne marche pas, essayez `chromium-browser`, si vous etes sur windows, allez dans le dossier où se trouve chrome.exe (dans Program Files) et lancez de la meme manière : `chrome.exe --disable-web-security --user-data-dir="/tmp"`

