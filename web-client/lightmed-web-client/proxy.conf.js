const PROXY_CONFIG = [
    {
        context: [
            "/admin",
            "/login",
            "/logout",
            "/dashboard",
            "/role",
            "/unit",
            "/user",
            "/medicalFile",
            "/getProfile",
            "/getUser"
        ],
        target: "http://localhost:8080",
        secure: false
    }
]
module.exports = PROXY_CONFIG;
