import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {PatientModel} from "../../../models/patient.model";
import {catchError} from "rxjs/operators";
import ErrorHandling from "../components/error/error_handling";
import {MedicalFileModel} from "../../../models/medicalFile.model";
import { LocalStorageService } from 'angular-web-storage';

@Injectable({
  providedIn: 'root'
})

export class HttpService {


  public state = new BehaviorSubject<boolean>(false);
  private BASE_URL = "http://localhost:4200";
  constructor(private http: HttpClient, private local: LocalStorageService) {
    console.log('INSTANCIATING HTTP SERVICE');
  }

  public setState(state: boolean) {
    console.log('setting observable state: ' + state);
  
    this.state.next(state);
  }


  public isAuthenticated() {
    console.log('am i authenticated? ' + this.state.getValue());
    if (this.state.getValue() === false) {
      console.log('getting user');
      const user = this.local.get('user');
      if (user != null) {
        return true;
      }
      return false;
    }
    else
      return true;
  }

  public get(url, params) {
    return this.http.get(this.BASE_URL + url, {params: params}).toPromise();
  }

  public login(params) {
    return this.http.post(this.BASE_URL  + '/login?username=' + params.username + '&password=' + params.password, {}, {observe: 'response', withCredentials: true}).toPromise();
  }

  public post(url, params) {
    return this.http.post(this.BASE_URL + url, params).toPromise();
  }

  public postOptions(url, params, options) {
    return this.http.post(this.BASE_URL + url, params, options).toPromise();
  }
  searchDMP(params): Observable<Array<MedicalFileModel>> {
    return this.http.post<Array<MedicalFileModel>>('/medicalFile/searchByPatient', params).pipe(
      catchError(ErrorHandling.handleError<Array<MedicalFileModel>>('searchPatient'))
    );
  }

  createDMP(params): Observable<Array<MedicalFileModel>> {
    return this.http.post<Array<MedicalFileModel>>('/medicalFile/createMedicalFile', params).pipe(
      catchError(ErrorHandling.handleError<Array<MedicalFileModel>>('searchPatient'))
    );
  }
}
