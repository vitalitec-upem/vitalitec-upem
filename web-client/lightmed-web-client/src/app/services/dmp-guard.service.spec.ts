import { TestBed } from '@angular/core/testing';

import { DmpGuardService } from './dmp-guard.service';

describe('DmpGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DmpGuardService = TestBed.get(DmpGuardService);
    expect(service).toBeTruthy();
  });
});
