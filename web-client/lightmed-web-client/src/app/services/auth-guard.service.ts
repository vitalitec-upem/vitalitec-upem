import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class AuthGuardService implements CanActivate {


  constructor(private httpService: HttpService, private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    console.log('canActivate');
    if (this.httpService.isAuthenticated()) {
      console.log('authenticated');
      this.httpService.setState(true);
        return true;
    }
    this.router.navigate(['/auth']);
    return false;
  }

}
