import { Injectable } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(public toastr: ToastrManager) {}

  showSuccess(title: string, message: string) {
    this.toastr.successToastr(message, title);
  }

  showFailure(title: string, message: string) {
    this.toastr.errorToastr(message, title);
  }

  show(title: string, message: string) {
    this.toastr.infoToastr(message, title);
  }
}
