import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { HttpService } from './http.service';
import { LocalStorageService } from 'angular-web-storage';

@Injectable({
  providedIn: 'root'
})
export class DmpGuardService implements CanActivate {


  constructor(private httpService: HttpService, private router: Router, private local: LocalStorageService) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    console.log('canActivate');
    if (this.httpService.isAuthenticated()) {
      console.log('authenticated');
      const user = this.local.get('user');
      if (user['role']['name'] == 'ROLE_ADMIN')
        return false;
      this.httpService.setState(true);
      return true;
    }
    this.router.navigate(['/auth']);
    return false;
  }

}
