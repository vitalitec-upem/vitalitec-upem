
import {NgModule} from "@angular/core";
import { CommonModule } from '@angular/common';
import {
  MatButtonModule, MatCardModule, MatDialogModule, MatInputModule, MatTableModule,
  MatToolbarModule, MatMenuModule,MatIconModule, MatProgressSpinnerModule, MatListModule, MatOptionModule, MatSelectModule, MatRadioButton, MatRadioModule
} from '@angular/material';

@NgModule({
  imports: [
  CommonModule, 
  MatToolbarModule,
  MatButtonModule, 
  MatCardModule,
  MatInputModule,
  MatDialogModule,
  MatTableModule,
  MatMenuModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatListModule,
  MatOptionModule,
  MatSelectModule
  ],
  exports: [
  CommonModule,
   MatToolbarModule,
   MatOptionModule,
   MatButtonModule, 
   MatCardModule, 
   MatInputModule, 
   MatDialogModule,
   MatRadioModule,
   MatTableModule, 
   MatMenuModule,
   MatIconModule,
    MatOptionModule,  
  MatSelectModule,
     
   MatProgressSpinnerModule,
    MatListModule
   ],
})
export class CustomMaterialModule { }
