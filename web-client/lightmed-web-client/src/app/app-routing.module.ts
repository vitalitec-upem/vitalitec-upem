import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuardService } from './services/auth-guard.service';
import { AdminComponent } from './components/admin/admin.component';
import { ProfileComponent } from './components/profile/profile.component';
import { SearchMedicalFileComponent } from './components/search-medical-file/search-medical-file.component';
import { ViewMedicalFileComponent } from 'src/app/components/view-medical-file/view-medical-file.component';
import { DmpGuardService } from './services/dmp-guard.service';

const routes: Routes = [
  { path: 'home', component: DashboardComponent, canActivate: [AuthGuardService], },
  { path: 'adm', component: AdminComponent, canActivate: [AuthGuardService], },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuardService], },
  { path: 'searchMedicalFile', component: SearchMedicalFileComponent, canActivate: [AuthGuardService], },
  { path: 'auth', component: LoginComponent },
  { path: 'view-medical-file/:id', component: ViewMedicalFileComponent, canActivate: [DmpGuardService] },  
  { path: '**', component: DashboardComponent, canActivate: [AuthGuardService], },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
