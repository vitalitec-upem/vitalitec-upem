import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { CustomMaterialModule } from './material.module';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './components/header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ng6-toastr-notifications';
import { AngularWebStorageModule } from 'angular-web-storage';
import { ProfileComponent } from './components/profile/profile.component';
import { AdminComponent } from './components/admin/admin.component';
import { SearchMedicalFileComponent } from './components/search-medical-file/search-medical-file.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { CreateUnitComponent } from './components/create-unit/create-unit.component';
import { CreateRoleComponent } from './components/create-role/create-role.component';
import { CreateSkillComponent } from './components/create-skill/create-skill.component';
import { SpinnerListComponent } from './components/spinner-list/spinner-list.component';
import { ViewMedicalFileComponent } from './components/view-medical-file/view-medical-file.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    HeaderComponent,
    ProfileComponent,
    AdminComponent,
    SearchMedicalFileComponent,
    CreateUserComponent,
    CreateUnitComponent,
    CreateRoleComponent,
    CreateSkillComponent,
    SpinnerListComponent,
    ViewMedicalFileComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    CustomMaterialModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    AngularWebStorageModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
