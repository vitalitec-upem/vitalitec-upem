import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {HttpService} from "../../services/http.service";
import {ToastService} from "../../services/toast.service";
import {LocalStorageService} from "angular-web-storage";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {PatientModel} from "../../../../models/patient.model";
import {Observable} from "rxjs";
import {catchError} from "rxjs/operators";
import ErrorHandling from "../error/error_handling";
import {MedicalFileModel} from "../../../../models/medicalFile.model";


@Component({
  selector: 'app-search-medical-file',
  templateUrl: './search-medical-file.component.html',
  styleUrls: ['./search-medical-file.component.scss']
})
export class SearchMedicalFileComponent implements OnInit {
  private registerForm: FormGroup;
  public isVisible: boolean = true;
  dmps: Array<MedicalFileModel>;

  constructor(private router: Router, private http: HttpService,
              private toast: ToastService, public local: LocalStorageService) {
    this.registerForm = new FormGroup({
      secu : new FormControl(''),
      lastName : new FormControl(''),
      firstName : new FormControl(''),
      phone : new FormControl(''),
      address :new FormControl(''),
      sex: new FormControl('')
    });
  }

  ngOnInit() {
  }

 search(): void {

   let patient = {
     secu: this.registerForm.controls.secu.value,
     lastName: this.registerForm.controls.lastName.value,
     firstName: this.registerForm.controls.firstName.value,
     sex: this.registerForm.controls.sex.value === 'Homme',
     phone: this.registerForm.controls.phone.value,
     description: "",
     address: null,
     birthDate: "",
     placeOfBirth: "",
     unit: null,
     medicalHistory: null
   };

   this.http.searchDMP(patient).subscribe(medicalFile => {
      this.dmps = medicalFile;
      if(this.dmps.length <= 0){
        this.toast.show("Recherche", "Pas de DMP trouvé");
        this.isVisible = !this.isVisible;
      }else{
        this.dmps.forEach(x => console.log(x));
      }
   },error => console.log(error));
 }



  nouveauDMP() {

    let patient = {
      secu : this.registerForm.controls.secu.value,
      lastName : this.registerForm.controls.lastName.value,
      firstName : this.registerForm.controls.firstName.value,
      sex : this.registerForm.controls.sex.value === 'Homme',
      phone : this.registerForm.controls.phone.value,
      description : "",
      address :null,
      birthDate : "",
      placeOfBirth : "",
      unit : null,
      medicalHistory : null
    };

    this.http.createDMP(patient).subscribe((resp) => {
      console.log(resp);
    }, error => console.log(error));

    this.isVisible = !this.isVisible;
  }

  ouvrirDmp(id) {
    console.log('id: ' + id);
    this.router.navigate(['/view-medical-file/' + id]);
  }
}
