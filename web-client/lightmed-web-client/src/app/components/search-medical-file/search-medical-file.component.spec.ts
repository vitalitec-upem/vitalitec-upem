import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchMedicalFileComponent } from './search-medical-file.component';

describe('SearchMedicalFileComponent', () => {
  let component: SearchMedicalFileComponent;
  let fixture: ComponentFixture<SearchMedicalFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchMedicalFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchMedicalFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
