import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-spinner-list',
  templateUrl: './spinner-list.component.html',
  styleUrls: ['./spinner-list.component.scss']
})
export class SpinnerListComponent implements OnInit {
  @Input() url;
  @Input() label;
  @Output() onSelectValue = new EventEmitter<any>();
  private options: any[];

  constructor(private http: HttpService) { }

  ngOnInit() {
    
  }

  ngOnChanges() {
    //console.log(this.url);
    this.http.get(this.url, {}).then((resp: any[]) => {
      this.options = resp;
      //console.log(resp.length);
      if (this.options != null && this.options.length > 0)
        this.changedSpinner(this.options[0]['name']);
    }).catch(err => console.log(err));
  }

  private changedSpinner(value) {
    //console.log(value);
    const obj = this.options.find(elem => elem['name'] === value);
    this.onSelectValue.emit(obj);
  }
}
