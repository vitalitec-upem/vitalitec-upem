import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DateValidator } from '../validators/date.validators';
import { HttpService } from '../../services/http.service';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

  private createUserForm: FormGroup;
  private hospital = null;
  private role = null;
  private pole = null;
  private skill = null;
  private service = null;
  private urlPole = '/unit/getPolesByHospital/';
  private urlSkill = '/admin/getSkillsByService/';
  private urlService = '/unit/getServicesByPole/';

  constructor(private http: HttpService, private toast: ToastService) {

    this.createUserForm = new FormGroup({
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      tel: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      birthdate: new FormControl('', DateValidator.frDate)
    });
  }

  ngOnInit() {
  }

  selectHopital(hospital: any) {
    this.hospital = hospital;
    this.urlPole = '/unit/getPolesByHospital/' + hospital['name'];
    this.service = null;
    if (this.pole != null)
      this.selectPole(this.pole);
    //console.log(hospital);
  }

  selectRole(role: any) {
    this.role = role;
    console.log(role);
  }

  selectPole(pole: any) {
    this.pole = pole;
    this.urlService = '/unit/getServicesByPole/' + pole['name'] + '/' + this.hospital['name'];
    if (this.service != null) {
      //console.log('service not null');
      console.log('BEFORE SETTING IN SELECT: ' + this.urlSkill);
      this.urlSkill = '/admin/getSkillsByService/' + this.service['name'];
      console.log('AFTER SETTING IN SELECT: : ' + this.urlSkill);
      
      this.urlPole = '/unit/getPolesByHospital/' + this.hospital['name'];    
    }
    //console.log(pole);
  }

  selectService(service: any) {
    this.service = service;
    this.urlSkill = '/admin/getSkillsByService/' + this.service['name'];    
    //console.log(service);
  }

  selectSkill(skill: any) {
    this.skill = skill;
    //console.log(skill);
  }

  save() {
    if (this.createUserForm.invalid) {
      return ;
    }
    let user = {
      firstName: this.createUserForm.controls.firstname.value,
      lastName: this.createUserForm.controls.lastname.value,
      birthDate: this.createUserForm.controls.birthdate.value,
      password : null,
      email: this.createUserForm.controls.email.value,
      token : null,
      unit: this.service,
      address: this.createUserForm.controls.address.value,
      role: this.role,
      skills: [this.skill],
    }
    console.log(user);
    //charlesbaudelaire@aphp.fr
    //KTfYbNUdOuCE
    this.http.postOptions('/admin/createUser', user, {responseType: 'text'}).then((resp) => {
      console.log(resp);
      if (resp != null) {
        this.toast.showSuccess('Ajout', 'L\'ajout de l\'utilisateur a été effectué avec succès');
        return ;
      }
      this.toast.showFailure('Ajout', 'L\'ajout de l\'utilisateur a échoué');      
    }).catch((err) => {
      this.toast.showFailure('Ajout', 'L\'ajout de l\'utilisateur a échoué');      
      console.log(err);
    });
  }

}
