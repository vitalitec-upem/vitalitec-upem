import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpService } from 'src/app/services/http.service';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'app-create-unit',
  templateUrl: './create-unit.component.html',
  styleUrls: ['./create-unit.component.scss']
})
export class CreateUnitComponent implements OnInit {

  private createHospitalForm: FormGroup;
  private createServiceForm: FormGroup;
  private createPoleForm: FormGroup;

  private pole = null;
  private hospital = [null, null];
  private service = null;

  private urlPole = null;

  constructor(private http: HttpService, private toast: ToastService) {
    this.createHospitalForm = new FormGroup({
      hospitalname: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    });
    this.createServiceForm = new FormGroup({
      servicename: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)      
    });
    this.createPoleForm = new FormGroup({
      polename: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)      
    });
  }

  ngOnInit() {
  }

  selectHospital(hospital: any, index: number) {
    this.hospital[index] = hospital;
    this.urlPole = '/unit/getPolesByHospital/' + hospital['name'];
    if (this.pole != null)
      this.selectPole(this.pole);
    //console.log(hospital);
  }
  selectPole(pole: any) {
    this.pole = pole;
  }

  selectService(service: any) {
    this.service = service;
    //console.log(service);
  }


  private saveHospital() {
    this.http.get('/unit/getUnitByName/APHP', {}).then((resp) => {
      const aphp = resp;
      const hospital = {
        name: this.createHospitalForm.controls.hospitalname.value,
        description: this.createHospitalForm.controls.description.value,
        parent: aphp,
        type: 'hospital'
      }
      console.log(hospital);
      this.http.postOptions('/admin/createUnit', hospital, {responseType: 'text'}).then((resp) => {
        console.log(resp);
        this.toast.showSuccess('Ajout', 'L\'ajout de l\'hôpital a bien été éffectué.');
      }).catch((err) => {
        this.toast.showFailure('Erreur', 'L\'ajout de l\'hôpital a échoué.');            
        console.log(err);
      });
    }).catch((err) => {
      this.toast.showFailure('Erreur', 'L\'ajout de l\'hôpital a échoué.');            
      console.log(err);
    });    
  }

  private saveService() {
    if (this.createServiceForm.invalid || this.hospital[1] == null || this.pole == null) {
      this.toast.showFailure('Erreur', 'Veuillez remplir tous les champs et sélectionner un hôpital et un pôle associé à votre service.');      
      return ;
    }
    let service = {
      name: this.createServiceForm.controls.servicename.value,
      description: this.createServiceForm.controls.description.value,
      parent: this.pole,
      type: 'service'
    }
    console.log(service);
    this.http.postOptions('/admin/createUnit', service, {responseType: 'text'}).then((resp) => {
      console.log(resp);
      this.toast.showSuccess('Ajout', 'L\'ajout du service a bien été éffectué.');
    }).catch((err) => {
      this.toast.showFailure('Erreur', 'L\'ajout du service a échoué.');            
      console.log(err);
    });
  }

  private savePole() {
    if (this.createPoleForm.invalid || this.hospital[0] == null) {
      this.toast.showFailure('Erreur', 'Veuillez remplir tous les champs et sélectionner un hôpital associé à votre pôle.');      
      return ;
    }
    let pole = {
      name: this.createPoleForm.controls.polename.value,
      description: this.createPoleForm.controls.description.value,
      parent: this.hospital[0],
      type: 'pole'
    }
    console.log(pole);
    this.http.postOptions('/admin/createUnit', pole, {responseType: 'text'}).then((resp) => {
      console.log(resp);
      this.toast.showSuccess('Ajout', 'L\'ajout du pôle a bien été éffectué.');
    }).catch((err) => {
      this.toast.showFailure('Erreur', 'L\'ajout du pôle a échoué.');            
      console.log(err);
    });
  }



}
