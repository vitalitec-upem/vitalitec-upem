import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpService } from 'src/app/services/http.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-create-role',
  templateUrl: './create-role.component.html',
  styleUrls: ['./create-role.component.scss']
})
export class CreateRoleComponent implements OnInit {

  private createRoleForm: FormGroup;
  
  constructor(private http: HttpService, private toast: ToastService) {

    this.createRoleForm = new FormGroup({
      rolename: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
  }

  
  save() {
    if (this.createRoleForm.invalid) {
      return ;
    }
    let role = {
      name: this.createRoleForm.controls.rolename.value
    };
    console.log(role);
    this.http.postOptions('/admin/createRole', role, {responseType: 'text'}).then((resp) => {
      console.log(resp);
      if (resp != null) {
        this.toast.showSuccess('Ajout', 'L\'ajout du rôle a été effectué avec succès');
        return ;
      }
      this.toast.showFailure('Ajout', 'L\'ajout du rôle a échoué');      
    }).catch((err) => {
      this.toast.showFailure('Ajout', 'L\'ajout du rôle a échoué');      
      console.log(err);
    });
  }

}
