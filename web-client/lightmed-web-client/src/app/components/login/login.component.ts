import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast.service';
import { LocalStorageService } from 'angular-web-storage';
import { HttpHeaders } from '@angular/common/http';
import { headersToString } from 'selenium-webdriver/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private registerForm: FormGroup;


  constructor (private router: Router, private http: HttpService,
    private toast: ToastService, public local: LocalStorageService) {
    console.log('user:');
    console.log(this.local.get('user'));

    if (this.local.get('user') != null) {
      this.http.get('/getUser', {}).then((user) => {
        if (user != null) {
          this.local.set('user', user);
          this.http.setState(true);
          this.router.navigate(['/home']);
        }
      }).catch((err) => {
        console.log('not online');
      })
    }
    this.registerForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
    // this.local.clear();
    console.log('login state: ' + this.http.isAuthenticated());
  }
    
  login() : void {
    console.log('login');
    let params = {
      username: this.registerForm.controls.username.value,
      password: this.registerForm.controls.password.value
    };
    this.http.login(params).then((resp) => {
      console.log('resp: ');
      console.log(resp);
      if (resp['body'] != null && resp['body']['user'] != null) {
        const headers: HttpHeaders = resp['headers'];
        console.log('logged in');
        this.toast.showSuccess('Connexion', 'Vous etes maintenant connecté.');
        this.local.set('user', resp['body']['user'], 86400);
        this.http.setState(true);
        this.router.navigate(['/home']);
      }
    }).catch((err) => {
      console.log(err);
      if (err['error'] != null) {
        this.toast.showFailure('Connexion', 'Mauvais identifiants de connexion.');
      } else {
        this.toast.showFailure('Connexion', 'Echec de la connexion au serveur.');
      }
    });
  }
}
