import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpService } from '../../services/http.service';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'app-create-skill',
  templateUrl: './create-skill.component.html',
  styleUrls: ['./create-skill.component.scss']
})
export class CreateSkillComponent implements OnInit {

  private createSkillForm: FormGroup;
  private unit = null;
  
  constructor(private http: HttpService, private toast: ToastService) {

    this.createSkillForm = new FormGroup({
      skillname: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
  }

  selectService(service: any) {
    this.unit = service;
    //console.log(service);
  }
  
  save() {
    if (this.createSkillForm.invalid || this.unit == null) {
      this.toast.showFailure('Erreur', 'Veuillez remplir tous les champs et sélectionner un service.');
      return ;
    }
    let skill = {
      name: this.createSkillForm.controls.skillname.value,
      parent: this.unit
    };
    console.log(skill);
    this.http.postOptions('/admin/createSkill', skill, {responseType: 'text'}).then((resp) => {
      console.log(resp);
      if (resp != null) {
        this.toast.showSuccess('Ajout', 'L\'ajout du savoir-faire a été effectué avec succès');
        return ;
      }
      this.toast.showFailure('Ajout', 'L\'ajout du savoir-faire a échoué');      
    }).catch((err) => {
      this.toast.showFailure('Ajout', 'L\'ajout du savoir-faire a échoué');      
      console.log(err);
    });
  }


}
