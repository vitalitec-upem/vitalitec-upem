import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewMedicalFileComponent } from './view-medical-file.component';

describe('ViewMedicalFileComponent', () => {
  let component: ViewMedicalFileComponent;
  let fixture: ComponentFixture<ViewMedicalFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewMedicalFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewMedicalFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
