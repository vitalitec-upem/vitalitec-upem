import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { HttpService } from '../../services/http.service';

@Component({
  selector: 'app-view-medical-file',
  templateUrl: './view-medical-file.component.html',
  styleUrls: ['./view-medical-file.component.scss']
})
export class ViewMedicalFileComponent implements OnInit {
  private id_dmp: string;
  private dmp: any = null;

  constructor(private route: ActivatedRoute, private http: HttpService,
    private router: Router) { }

  ngOnInit() {
    console.log('view dmp');
    this.id_dmp = this.route.snapshot.paramMap.get('id');
    this.http.get('/medicalFile/findById/' + this.id_dmp, {}).then((resp) => {
      this.dmp = resp;
      console.log(this.dmp);
    }).catch((err) => console.log(err));
  }

}
