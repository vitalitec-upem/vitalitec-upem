import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import * as moment from 'moment';
import {ToastService} from "../../services/toast.service";
import {LocalStorageService} from "angular-web-storage";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  private firstName: string;
  private lastName: string;
  private role: string;
  private birthdate: string;
  private email: string;
  private user = null;
  constructor(private http: HttpService,
              private toast: ToastService, public local: LocalStorageService) {

  }

  ngOnInit() {
    this.user = this.local.get('user');
    console.log(this.user);
    this.birthdate = moment(this.user['birthDate']).format('DD/MM/YYYY');    
    /*this.firstName = user['firstName'];
    this.lastName = user['lastName'];
    this.role = user['role']['name'];
    this.email = user['email'];*/
  }
}
