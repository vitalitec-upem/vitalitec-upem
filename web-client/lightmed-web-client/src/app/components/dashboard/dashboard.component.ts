import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { LocalStorageService } from 'angular-web-storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private http: HttpService, private local: LocalStorageService, private router: Router) { }

  ngOnInit() {
    const user = this.local.get('user');
    if (user['role']['name'] === 'ROLE_ADMIN') {
      this.router.navigateByUrl('/adm');
    } else {
      this.router.navigateByUrl('/searchMedicalFile');      
    }
  }

}
