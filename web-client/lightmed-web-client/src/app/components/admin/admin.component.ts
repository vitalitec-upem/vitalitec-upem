import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  private tab = [true, false, false, false];

  constructor() { }

  ngOnInit() {
  }

  changeActive(i) {
    this.tab.fill(false);
    this.tab[i] = true; 
  }

}
