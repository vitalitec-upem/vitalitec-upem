import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { LocalStorageService } from 'angular-web-storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  private state: boolean = false;
  private role: string;
  constructor(private router: Router, private http: HttpService, private local: LocalStorageService) { }

  ngOnInit() {
    this.http.state.subscribe((state) => {
      console.log('state changed: ' + state);
      this.state = state;
      if (state == true) {
        const user = this.local.get('user');
        if (user != null)
          this.role = user['role']['name'];
        else {
          console.log('user is null');
        }
      }
    });
    
    console.log(this.local.get('user'));
  }

  public logout() {
    this.local.clear();
    this.http.get("/logout", {}).then((logoutResponse) => {
      console.log(logoutResponse);
      this.http.setState(false);
      this.router.navigate(['/auth']);
    });
  }
}
