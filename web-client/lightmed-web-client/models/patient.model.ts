export interface PatientModel {
  socialSecurityNumber : string,
  lastName : string,
  firstName : string,
  sex : boolean,
  phoneNumber : string,
  description : string,
  address : string,
  birthDate : string,
  placeOfBirth : string,
  unit : string,
  medicalHistory : string
}
