import {PatientModel} from "./patient.model";

export interface MedicalFileModel {
  id:number;


  published: boolean
  archived: boolean;
  archivedOn: string;
  updatedOn: string;
  createdOn: string;

  patient: PatientModel;

  createdBy:string;

  acts:string;
}
