// @author: chilowi@u-pem.fr

document.getElementById("url").value = document.location + "notifications/android";
document.getElementById("watchButton").addEventListener("click", function(event) {
    // initialize the websocket
    let url = document.getElementById("url").value.replace("http", "ws");
    var socket = new WebSocket(url);
    socket.onopen = function(event) {
        document.getElementById("webSocketStatus").textContent = "WebSocket communication opened";
        socket.send("pushSince:0");
    }
    socket.onmessage = function(wsEvent) {
        let msg = JSON.parse(wsEvent.data);
        // todo
        let a1 = document.createElement("li");
        let a2 = document.createElement("ol");
        a1.appendChild(a2);
        for (var property in msg) {
            let a3 = document.createElement("li");
            a3.textContent = property + "=" + msg[property];
            a2.appendChild(a3);
        }
        document.getElementById("receivedMessages").appendChild(a1);
    }
    return false;
});

document.getElementById("submitMessage").addEventListener("click", function(event) {
    let url = document.getElementById("url").value
        + "?id=" + encodeURI(document.getElementById("messageID").value)
        + "&latitude=" + encodeURI(document.getElementById("messageLatitude").value)
        + "&longitude=" + encodeURI(document.getElementById("messageLongitude").value)
        + "&radius=" + encodeURI(document.getElementById("messageRadius").value);
    let headers = new Headers({
        "X-Password": document.getElementById("password").value
    })
    let body = document.getElementById("messageContent").value;
    fetch(url, {method: "POST", headers: headers, mode: "cors", body: body}).then(function(response) {
        if (response.ok) {
            document.getElementById("submissionStatus").textContent = "Submission succeeded";
        } else {
            document.getElementById("submissionStatus").textContent = "Error " + response.status + ": " + response.statusText;
        }
    })
    return false;
})