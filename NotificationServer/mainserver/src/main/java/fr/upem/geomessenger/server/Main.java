package fr.upem.geomessenger.server;

import org.webbitserver.*;
import org.webbitserver.handler.EmbeddedResourceHandler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Main class for the geomessenger server
 *
 * @author chilowi@u-pem.fr
 */
public class Main {

    static class MainHandler implements HttpHandler {
        private static Pattern URI_PATTERN = Pattern.compile("^/notifications/([a-zA-Z0-9_]+)");

        @Override
        public void handleHttpRequest(HttpRequest request, HttpResponse response, HttpControl control) throws Exception {
            System.err.println("Request arrived to " + request.uri());
            // find the channel
            String channel;
            Matcher m = URI_PATTERN.matcher(request.uri());
            if (m.find()) {
                channel = m.group(1);
            } else {
                control.nextHandler();
                return;
            }
            // if POST method: receive the message
            if (request.method().equals("POST"))
                new MessageReceiverHandler(channel).handleHttpRequest(request, response, control);
            else {
                control.nextHandler();
                return;
            }
        }
    }

    public static void main(String[] args) throws IOException {
        System.out.println(args[0]);
        int port = Integer.parseInt(args[0]);

        // initialize the channels
        MessageDispatcher.getInstance().initChannel("android", "password");
        /*Path channelFile = Paths.get(args[1]);
        Files.lines(channelFile)
                .map(x -> x.trim().split(":"))
                .filter(x -> x.length == 2 && !x[0].isEmpty() && !x[1].isEmpty())
                .forEach( x -> MessageDispatcher.getInstance().initChannel(x[0], x[1]) );*/
        System.out.println("Initialized channels");
        // initialize web server
        WebServer webServer = WebServers.createWebServer(port);
        webServer.add(new MainHandler());
        //webServer.add(new EmbeddedResourceHandler("fr/upem/geomessenger/server/html"));
        System.out.println(MessageDispatcher.getInstance().getChannelNames());
        for (String channelName: MessageDispatcher.getInstance().getChannelNames()) {
            System.out.println("Channels: " + channelName);
            webServer.add("/notifications/" + channelName, new MessengerWebSocketHandler(channelName));
        }
        System.err.println("Starting server...");
        webServer.start();
    }
}
