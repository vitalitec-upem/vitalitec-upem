package fr.upem.geomessenger.server;

import org.webbitserver.HttpControl;
import org.webbitserver.HttpHandler;
import org.webbitserver.HttpRequest;
import org.webbitserver.HttpResponse;

/** A HTTP handler receiving messages on channels
 *
 * @author chilowi@u-pem.fr
 */
public class MessageReceiverHandler implements HttpHandler {
    private final String channel;

    public MessageReceiverHandler(String channel) {
        this.channel = channel;
    }

    private double getDouble(HttpRequest request, String key) {
        try {
            return Double.parseDouble(request.queryParam(key));
        } catch (NumberFormatException e)
        {
            return Double.NaN;
        }
    }

    @Override
    public void handleHttpRequest(HttpRequest request, HttpResponse response, HttpControl control) throws Exception {
//        if (! MessageDispatcher.getInstance().checkPassword(channel, request.header("X-Password")))
//            response.status(401).content("Invalid password").end();
//        else {
            String id = request.queryParam("id");
            String message = request.queryParam("message");
            Message msg = new Message(id, message);
            MessageDispatcher.getInstance().push(channel, msg);
            response.status(200).content("OK").end();
//        }
    }
}
