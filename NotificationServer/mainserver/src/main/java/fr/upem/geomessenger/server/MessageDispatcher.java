package fr.upem.geomessenger.server;

import org.webbitserver.WebSocketConnection;
import org.webbitserver.WebSocketHandler;

import java.util.*;
import java.util.logging.Logger;

/**
 * Manage channels and dispatch messages.
 *
 * @author chilowi@u-pem.fr
 */
public class MessageDispatcher {
    final static Logger logger = Logger.getLogger(MessageDispatcher.class.getName());

    private static final MessageDispatcher INSTANCE =
            new MessageDispatcher();

    /** Number of kept messages for each channel */
    public static final int KEPT_MESSAGES_NUMBER = 32;

    static class Channel {
        String password;
        Set<WebSocketConnection> registeredConnections = new HashSet<>();
        LinkedHashMap<String, Message> lastMessages = new LinkedHashMap<>();

        public Channel(String password) {
            this.password = password;
        }
    }

    /** Map of channels */
    private Map<String, Channel> channels = new HashMap<>();

    /** Return singleton instance */
    public static MessageDispatcher getInstance() {
        return INSTANCE;
    }

    public void initChannel(String channelName, String password) {
        channels.put(channelName, new Channel(password));
    }

    public Set<String> getChannelNames() {
        return channels.keySet();
    }

    /** Check the password of the channel */
    public boolean checkPassword(String channelName, String proposed) {
        Channel channel = channels.get(channelName);
        return channel != null && channel.password.equals(proposed);
    }

    /** Push a new message inside a channel */
    public void push(String channelName, Message msg) {
        logger.info("Having received message " + msg + " transmitted on channel " + channelName);
        Channel channel = channels.get(channelName);
        if (channel != null) {
            channel.lastMessages.put(msg.id, msg);
            logger.info("found channel");
            logger.info("Registered connections: " + channel.registeredConnections.size());
            // send the message on each subscribed connection
            for (WebSocketConnection conn: channel.registeredConnections) {
                logger.info("Sending to chan: " + conn);
                conn.send(msg.toJSON());
            }

            // Clean the map if there is too much messages (oldest messages are removed)
            int toBeRemoved = channel.lastMessages.size() - KEPT_MESSAGES_NUMBER;
            if (toBeRemoved > 0)
                for (Iterator<String> it = channel.lastMessages.keySet().iterator(); toBeRemoved > 0; toBeRemoved--) {
                    it.next();
                    it.remove();
                }
        }
    }

    /** Register a new connection */
    public void register(String channelName, WebSocketConnection connection, long since) {
        Channel channel = channels.get(channelName);
        if (channel != null) {
            channel.registeredConnections.add(connection);

            // send messages from the past
            for (Message msg: channel.lastMessages.values())
                if (msg.timestamp >= since)
                    connection.send(msg.toJSON());
        }
    }

    /** Unregister a connection */
    public void unregister(String channelName, WebSocketConnection connection) {
        Channel channel = channels.get(channelName);
        if (channel != null) {
            channel.registeredConnections.remove(connection);
        }
    }
}