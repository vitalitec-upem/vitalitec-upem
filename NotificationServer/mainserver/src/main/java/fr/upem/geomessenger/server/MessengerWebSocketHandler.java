package fr.upem.geomessenger.server;

import org.webbitserver.WebSocketConnection;
import org.webbitserver.WebSocketHandler;

import java.util.logging.Logger;

/**
 * A handler for the websocket that does not do lots of things
 * since we mostly use the websocket to send messages rather to receive messages.
 *
 * @author chilowi@u-pem.fr
 */
public class MessengerWebSocketHandler implements WebSocketHandler {

    final static Logger logger = Logger.getLogger(MessengerWebSocketHandler.class.getName());

    private final String channel;

    public MessengerWebSocketHandler(String channel) {
        this.channel = channel;
    }

    @Override
    public void onOpen(WebSocketConnection connection) throws Throwable {
        logger.info("New connection: " + connection);
        long since = System.currentTimeMillis();
        MessageDispatcher.getInstance().register(channel, connection, since);
    }

    @Override
    public void onClose(WebSocketConnection connection) throws Throwable {
        logger.info("Closing connection: connection");
        MessageDispatcher.getInstance().unregister(channel, connection);
    }

    @Override
    public void onMessage(WebSocketConnection connection, String msg) throws Throwable {
        logger.fine("Message on connection " + connection + ": " + msg);
        long since = System.currentTimeMillis();
        logger.info("Registering connection " + connection + " on channel " + channel);
        MessageDispatcher.getInstance().register(channel, connection, since);
    }

    @Override
    public void onMessage(WebSocketConnection connection, byte[] msg) throws Throwable {
        logger.info("Binary message on connection " + connection + ": " + msg);
    }

    @Override
    public void onPing(WebSocketConnection connection, byte[] msg) throws Throwable {
        logger.fine("Ping on " + connection);
    }

    @Override
    public void onPong(WebSocketConnection connection, byte[] msg) throws Throwable {
        logger.fine("Pong on " + connection);
    }
}
