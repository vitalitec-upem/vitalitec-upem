package fr.upem.geomessenger.server;

import org.json.JSONObject;

/** A class representing a message with its attributes
 *
 * @author chilowi@u-pem.fr
 */
public class Message {
    public final long timestamp;
    public final String id;
    public final String content;

    public Message(String id, String content) {
        this.id = id;
        this.content = content;
        this.timestamp = System.currentTimeMillis() / 1000; // timestamp in seconds
    }

    public String toJSON() {
        JSONObject obj = new JSONObject();
        obj.put("timestamp", timestamp);
        obj.put("id", id);
        obj.put("content", content);
        return obj.toString();
    }

    @Override
    public String toString() {
        return "Message{" +
                "timestamp=" + timestamp +
                ", id='" + id + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
