package com.vitalitec.lightmed;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vitalitec.lightmed.model.MedicalFile;
import com.vitalitec.lightmed.model.Patient;
import com.vitalitec.lightmed.service.MedicalFileService;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@WithMockUser
public class MedicalFileControllerTest {

	
	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private MedicalFileService service;
	
	Optional<MedicalFile> mf;
	Patient patient;
	
	private static String ROOT = "/medicalFile";
	
	public static String asJsonString(final Object obj) {
	    try {
	        final ObjectMapper mapper = new ObjectMapper();
	        final String jsonContent = mapper.writeValueAsString(obj);
	        return jsonContent;
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}  
	
    @Before
    public void setUpMedicalFile() throws Exception{
    	
        mf = Optional.of(new MedicalFile());
        mf.get().setId(38);
        patient = new Patient();
        patient.setFirstName("Kévin");
        patient.setLastName("R");
        patient.setSex(Patient.MALE);
        patient.setPhoneNumber("0762603079");
    }
    
	@Test
	public void testHTTPOK() throws Exception {
		
		mvc.perform(post(ROOT + "/searchByPatient")
			.param("idDMP", "38")
			.contentType(MediaType.APPLICATION_JSON)
		).andExpect(status().isOk());
	}
	
	@Test
	public void testCorrectMedicalFile() throws Exception {
		
		when(service.findById(38)).thenReturn(mf);
		
		mvc.perform(post(ROOT + "/searchByPatient")
			.content(asJsonString(patient))
			.param("idDMP", "38")
			.contentType(MediaType.APPLICATION_JSON)
		).andExpect(status().isOk())
		.andExpect(jsonPath("$", hasSize(1)))
		.andExpect(jsonPath("$[0].id", is(38)));
	}
	
	@Test
	public void testCorrectMedicalFile2() throws Exception {
		
		when(service.searchMedicalFiles(patient)).thenReturn(Collections.singletonList(mf.get()));
		
		mvc.perform(post(ROOT + "/searchByPatient")
			.content(asJsonString(patient))
			.contentType(MediaType.APPLICATION_JSON)
		).andExpect(status().isOk())
		.andExpect(jsonPath("$", hasSize(1)))
		.andExpect(jsonPath("$[0].id", is(38)));
	}
	
}
