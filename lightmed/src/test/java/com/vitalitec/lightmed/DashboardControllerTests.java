package com.vitalitec.lightmed;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.vitalitec.lightmed.model.MedicalFile;
import com.vitalitec.lightmed.service.MedicalFileService;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@WithMockUser
public class DashboardControllerTests {
	
	private static final Logger log = LoggerFactory.getLogger(DashboardControllerTests.class);

	@Autowired
    private MockMvc mvc;
	
	@MockBean
    private MedicalFileService medicalFileService;
	
	Optional<MedicalFile> mf;
    @Before
    public void setUpMedicalFile() throws Exception{
        mf = Optional.of(new MedicalFile());
        mf.get().setId(38);
    }

	@Test
	public void testGetCorrectMedicalFile() throws Exception {
		log.info("MedicalFile: " + mf.get().getId());
		when(medicalFileService.findById(38)).thenReturn(mf);
		this.mvc.perform(get("/dashboard/getMedicalFile/{id}", 38).contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.id", is(new Integer((int) mf.get().getId()))));
	}
}
