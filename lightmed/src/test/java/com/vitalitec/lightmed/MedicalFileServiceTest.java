package com.vitalitec.lightmed;

import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import com.vitalitec.config.SecurityConfig;
import com.vitalitec.lightmed.model.Address;
import com.vitalitec.lightmed.model.MedicalFile;
import com.vitalitec.lightmed.model.Patient;
import com.vitalitec.lightmed.repository.MedicalFileRepository;
import com.vitalitec.lightmed.service.MedicalFileService;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@Import(SecurityConfig.class)
@SpringBootTest
public class MedicalFileServiceTest {
			

	@Autowired
	private MedicalFileService medicalFileService;
	
	@MockBean
    private MedicalFileRepository medicalFileRepository;
	
	@Before
	public void setUp() {
		
		
		final Patient patient = new Patient();
		
		patient.setFirstName("Kévin");
		patient.setLastName("RAMANANKANDRASANA");
		patient.setPhoneNumber("0762603079");
		patient.setSocialSecurityNumber("666");
		
		final Address address = new Address();
		address.setFullAddress("8 avenue France Bondy 93140");
		
		patient.setAddress(address);
		
		final MedicalFile mFile = new MedicalFile();
		mFile.setPatient(patient);
		mFile.setId(42);
		
		//Configuration of repository's behavior 
		Mockito.when(medicalFileRepository.findById(42)).thenReturn(Optional.of(mFile));
		Mockito.when(medicalFileRepository.findByPatientAddressFullAddress(patient.getAddress().getFullAddress()))
				.thenReturn(Collections.singletonList(mFile));
		Mockito.when(medicalFileRepository.findByPatientFirstName(patient.getFirstName())).thenReturn(Collections.singletonList(mFile));
		Mockito.when(medicalFileRepository.findByPatientLastName(patient.getLastName())).thenReturn(Collections.singletonList(mFile));
		Mockito.when(medicalFileRepository.findByPatientPhoneNumber(patient.getPhoneNumber())).thenReturn(Collections.singletonList(mFile));
		Mockito.when(medicalFileRepository.findByPatientSocialSecurityNumber(patient.getSocialSecurityNumber())).thenReturn(Collections.singletonList(mFile));
		
		
	}
	
	@Test
	public void whenIdNotNull_thenMFileShouldBeFound() {
		
		Optional<MedicalFile> founds = medicalFileService.findById(42);
		
		assertTrue(founds.get().getId() == 42);
	}
	

	@Test
	public void whenIdIsNullButNotPatientFirstName_thenMFileMayBeFound() {
		
		final Patient patient = new Patient();
		patient.setFirstName("Kévin");
		
		final List<MedicalFile> founds = medicalFileService.searchMedicalFiles(patient);
		
		assertTrue(founds.size() > 0);
	}
	
	@Test
	public void whenPatientSocialSecurityNumberAndFirstNameAreCorrect_thenMFileMayBeFound() {
		
		final Patient patient = new Patient();
		patient.setFirstName("Kévin");
		patient.setSocialSecurityNumber("666");
		
		final List<MedicalFile> founds = medicalFileService.searchMedicalFiles(patient);
		
		assertTrue(founds.size() > 0);
	}
	
	@Test
	public void whenPatientFirstNameIsCorrectButNotOthersField1_thenNoMFileMustBeFound() {
		
		final Patient patient = new Patient();
		patient.setFirstName("Kévin");
		patient.setSocialSecurityNumber("466");
		
		final List<MedicalFile> founds = medicalFileService.searchMedicalFiles(patient);
		
		assertTrue(founds.size() == 0);
	}
}
