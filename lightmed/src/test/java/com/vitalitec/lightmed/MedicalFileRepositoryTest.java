package com.vitalitec.lightmed;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.vitalitec.lightmed.model.Address;
import com.vitalitec.lightmed.model.MedicalFile;
import com.vitalitec.lightmed.model.Patient;
import com.vitalitec.lightmed.repository.MedicalFileRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class MedicalFileRepositoryTest {
	
	@Autowired
	private TestEntityManager entityManager;
	
	
	@Autowired
	private MedicalFileRepository medicalFileRepository;
	
	
	@Before
	public void setUp() {
		
		final Patient patient = new Patient();
		final Address address = new Address();
		address.setCity("Bondy");
		address.setCountry("France");
		address.setNum("8");
		address.setStreet("Avenue");
		address.setZip("93140");
		address.setFullAddress("8 avenue France Bondy 93140");
		
		patient.setFirstName("Kévin");
		patient.setLastName("RAMANANKANDRASANA");
		patient.setPhoneNumber("0762603079");
		patient.setSocialSecurityNumber("666");
		patient.setAddress(address);
		
		final MedicalFile medicalFile = new MedicalFile();
		medicalFile.setPatient(patient);
		
		entityManager.persist(address);
		entityManager.persist(patient);
		entityManager.persist(medicalFile); 	
		entityManager.flush();
	}
	
	@Test
	public void whenFindByPatientFirstName_thenReturnMedicalFile() {
		

		final List<MedicalFile> founds = medicalFileRepository.findByPatientFirstName("Kévin");
			
		assertTrue(founds.size() > 0);
			
	}
	
	@Test
	public void whenFindByPatientLastName_thenReturnMedicalFile() {
		
		final List<MedicalFile> founds = medicalFileRepository.findByPatientLastName("RAMANANKANDRASANA");
				
		assertTrue(founds.size() > 0);
			
	}
	
	@Test
	public void whenFindByPatientAddressFullAddress_thenReturnMedicalFile() {
				
		final List<MedicalFile> founds = medicalFileRepository.findByPatientAddressFullAddress("8 avenue France Bondy 93140");
				
		assertTrue(founds.size() > 0);
			
	}
	
	@Test
	public void whenFindByPatientPhoneNumber_thenReturnMedicalFile() {
				
		final List<MedicalFile> founds = medicalFileRepository.findByPatientPhoneNumber("0762603079");
				
		assertTrue(founds.size() > 0);
			
	}
	
	@Test
	public void whenFindByPatientSocialSecurityNumber_thenReturnMedicalFile() {
				
		final List<MedicalFile> founds = medicalFileRepository.findByPatientSocialSecurityNumber("666");
				
		assertTrue(founds.size() > 0);
			
	}
	
	@Test
	public void whenFindByPatientFirstNameAndALastName_thenReturnMedicalFile() {
				
		final List<MedicalFile> founds = medicalFileRepository.findByPatientFirstNameAndPatientLastName("Kévin", "RAMANANKANDRASANA");
				
		assertTrue(founds.size() > 0);
			
	}
	
	@Test
	public void whenFindBySomethingNotInDB_thenReturn0MedicalFile() {
				
		final List<MedicalFile> founds = medicalFileRepository.findByPatientFirstNameAndPatientLastName("Brigitte", "RAMANANKANDRASANA");
				
		assertTrue(founds.size() == 0);
			
	}
	
	
}
