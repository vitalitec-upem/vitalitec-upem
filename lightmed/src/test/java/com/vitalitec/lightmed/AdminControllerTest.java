package com.vitalitec.lightmed;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.vitalitec.lightmed.model.Role;
import com.vitalitec.lightmed.model.Skill;
import com.vitalitec.lightmed.model.Unit;
import com.vitalitec.lightmed.service.RoleService;
import com.vitalitec.lightmed.service.SkillService;
import com.vitalitec.lightmed.service.UnitService;
import com.vitalitec.lightmed.utils.TestUtils;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@WithMockUser
public class AdminControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private RoleService roleService;
	
	@MockBean
	private SkillService skillService;
	
	@MockBean
	private UnitService unitService;
	
	private final static String ROOT = "/admin"; 
	
	private final Unit unit = new Unit();
	private final Role role = new Role();
	private final Skill skill = new Skill();
	
	@Test
	public void testCreateRoleHTTPOK() throws Exception {

		mvc.perform(post(ROOT + "/createRole")
			.content(TestUtils.asJsonString(role))
			.contentType(MediaType.APPLICATION_JSON)
		).andExpect(status().isOk());
	}

	@Test
	public void testCreateRoleHTTPBadRequest() throws Exception {

		mvc.perform(post(ROOT + "/createRole")
			.contentType(MediaType.APPLICATION_JSON)
		).andExpect(status().isBadRequest());
	}
	


	@Test
	public void testCreateSkillHTTPOK() throws Exception {

		mvc.perform(post(ROOT + "/createSkill")
			.content(TestUtils.asJsonString(skill))
			.contentType(MediaType.APPLICATION_JSON)
		).andExpect(status().isOk());
	}
	
	@Test
	public void testCreateUnitHTTPOK() throws Exception {

		mvc.perform(post(ROOT + "/createUnit")
			.content(TestUtils.asJsonString(unit))
			.contentType(MediaType.APPLICATION_JSON)
		).andExpect(status().isOk());
	}

	@Test
	public void testCreateUnitHTTPBadRequest() throws Exception {

		mvc.perform(post(ROOT + "/createUnit")
			.contentType(MediaType.APPLICATION_JSON)
		).andExpect(status().isBadRequest());
	}
}
