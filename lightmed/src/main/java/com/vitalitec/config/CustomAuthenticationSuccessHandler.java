package com.vitalitec.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vitalitec.lightmed.model.AppUser;
import com.vitalitec.lightmed.repository.RoleRepository;
import com.vitalitec.lightmed.repository.UserRepository;
import org.springframework.security.core.userdetails.User;

@Service
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	RoleRepository roleRepository;
	
	private ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		String username = authentication.getName();
		System.out.println("Logged with role: " + authentication.getAuthorities());
		Optional<AppUser> optionalUser = userRepository.findByEmail(username);
		if (optionalUser.isPresent()) {
			response.setStatus(HttpServletResponse.SC_OK);
			Map<String, Object> data = new HashMap<>();
			data.put("user", optionalUser.get());
			HttpSession session = request.getSession();
	        User authUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	        session.setAttribute("username", authUser.getUsername());
	        session.setAttribute("authorities", authentication.getAuthorities());
			response.getOutputStream().println(objectMapper.writeValueAsString(data));
		} else {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		}

	}
}