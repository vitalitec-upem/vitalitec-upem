package com.vitalitec.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.session.HttpSessionEventPublisher;

import com.vitalitec.lightmed.service.UserDetailsServiceImpl;

@Configuration
@EnableAutoConfiguration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;

	@Autowired
	private AuthenticationFailureHandler authenticationFailureHandler;

	@Autowired
	private AuthenticationSuccessHandler authenticationSuccessHandler;

	@Autowired
	private UserDetailsService userDetailsService;

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return bCryptPasswordEncoder;
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

		// Setting Service to find User in the database.
		// And Setting PassswordEncoder
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());

	}


	@Bean
	public LogoutSuccessHandler logoutSuccessHandler() {
	    return new CustomLogoutSuccessHandler();
	}


	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
		http.httpBasic();
		http.authorizeRequests().antMatchers("/admin/**").hasRole("ADMIN");
		http.authorizeRequests().antMatchers("/unit/**").authenticated();
		http.authorizeRequests().antMatchers("/skill/**").authenticated();
		http.authorizeRequests().antMatchers("/role/**").authenticated();
		http.authorizeRequests().antMatchers("/medicalFile/**").authenticated();
		http.authorizeRequests().antMatchers("/notification/**").permitAll();
		http.authorizeRequests().antMatchers("/client").permitAll();
		http.authorizeRequests().antMatchers("/getUser").permitAll();
		http.cors().and().authorizeRequests();
		http.authorizeRequests().antMatchers("/getProfile/**").authenticated();
		http.authorizeRequests().antMatchers("/act/**").authenticated();
		http.authorizeRequests()
		    .antMatchers("/").permitAll()
		    .anyRequest().authenticated()
		    .and()
			.formLogin().successHandler(authenticationSuccessHandler)
			    .loginPage("/login").failureHandler(authenticationFailureHandler)
			    .permitAll()
			    .and()
			.logout().deleteCookies("JSESSIONID")
			.logoutSuccessHandler(logoutSuccessHandler())
	        .and()
	        .rememberMe().key("uniqueAndSecret").tokenValiditySeconds(86400);
	}

	@Bean
	public AuthenticationManager customAuthenticationManager() throws Exception {
		return authenticationManager();
	}

	@Bean
	public HttpSessionEventPublisher httpSessionEventPublisher() {
		return new HttpSessionEventPublisher();
	}

}