package com.vitalitec.lightmed.model;

public interface Acts {
	public static final int VITAL_SIGNS_READING = 0;
	public static final int RADIOGRAPHY = 1;
	public static final int SURGERY = 2;
	public static final int MEDICAL_IMAGING = 3;
}
