package com.vitalitec.lightmed.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.vitalitec.lightmed.service.CustomDateDeserialize;

@Entity
@Table(name = "USER_TABLE")
public class AppUser {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(unique = true)
	private String email;

	private String firstName;
	private String lastName;

	@JsonDeserialize(using = CustomDateDeserialize.class)
	private Date birthDate;

	private String phoneNumber;
	private String token;
	private String password;

	@ManyToOne(cascade = CascadeType.MERGE)
	private Unit unit;

	@ManyToOne(cascade = CascadeType.MERGE)
	private Role role;

	@ManyToOne(cascade = CascadeType.MERGE)
	private Address address;

	@ManyToMany(cascade = CascadeType.MERGE)
	@JoinTable(name = "user_skill", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "skill_id"))
	private Set<Skill> skills;

	public AppUser() {
	}

	public AppUser(String firstName, String lastName, Date birthDate, String phoneNumber, String token, String email,
			String password, Unit unit, Address address, Role role, Set<Skill> skills) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.phoneNumber = phoneNumber;
		this.token = token;
		this.email = email;
		this.password = password;
		this.unit = unit;
		this.address = address;
		this.role = role;
		this.skills = skills;
	}

	public AppUser(String firstName, String lastName, Date birthDate, String phoneNumber, String email, String password,
			Role role) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.password = password;
		this.role = role;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}
