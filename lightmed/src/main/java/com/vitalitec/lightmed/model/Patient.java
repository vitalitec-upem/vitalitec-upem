package com.vitalitec.lightmed.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "PATIENT_TABLE")
public class Patient {
	
	public static boolean MALE = Boolean.FALSE;
	public static boolean FEMALE = Boolean.TRUE;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String socialSecurityNumber;
	private String lastName;
	private String firstName;
	private Boolean sex;
	private String phoneNumber;
	private String description;
	private Date birthDate;
	private String placeOfBirth;

	@ManyToOne(cascade = { CascadeType.ALL })
	private Address address;

	@ManyToOne(cascade = { CascadeType.ALL })
	private Unit unit;

	@OneToMany(mappedBy = "patient", cascade = { CascadeType.ALL })
	private List<MedicalHistory> medicalHistory;

	public Patient() {
	}

	public Patient(String socialSecurityNumber, String lastName, String firstName, Boolean sex, String phoneNumber,
			String description, Address address, Unit unit, Date birthDate, String placeOfBirth,
			List<MedicalHistory> medicalHistory) {
		this.socialSecurityNumber = socialSecurityNumber;
		this.lastName = lastName;
		this.firstName = firstName;
		this.sex = sex;
		this.phoneNumber = phoneNumber;
		this.description = description;
		this.address = address;
		this.birthDate = birthDate;
		this.placeOfBirth = placeOfBirth;
		this.unit = unit;
		this.medicalHistory = medicalHistory;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Boolean getSex() {
		return sex;
	}

	public void setSex(Boolean sex) {
		this.sex = sex;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public List<MedicalHistory> getMedicalHistory() {
		return medicalHistory;
	}

	public void setMedicalHistory(List<MedicalHistory> medicalHistory) {
		this.medicalHistory = medicalHistory;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object o) {

		if (!(o instanceof Patient)) {
			return false;
		}

		if (this.firstName == null || this.lastName == null || this.socialSecurityNumber == null) {
			return false;
		}

		Patient p = (Patient) o;
		return this.firstName.equals(p.firstName) && this.lastName.equals(p.lastName)
				&& this.phoneNumber.equals(p.phoneNumber) && this.socialSecurityNumber.equals(p.socialSecurityNumber);
	}

}
