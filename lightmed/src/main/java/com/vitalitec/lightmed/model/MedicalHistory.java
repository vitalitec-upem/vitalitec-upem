package com.vitalitec.lightmed.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MEDICAL_HISTORY_TABLE")
public class MedicalHistory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String description;
	private Date medicalHistoryDate;
	
	@ManyToOne
	private Patient patient;
	
	public MedicalHistory() {}
	public MedicalHistory(String description, Date medicalHistoryDate, Patient patient) {
		this.description = description;
		this.medicalHistoryDate = medicalHistoryDate;
		this.patient = patient;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Date getMedicalHistoryDate() {
		return medicalHistoryDate;
	}
	
	public void setMedicalHistoryDate(Date date) {
		this.medicalHistoryDate = date;
	}
	
	public Patient getPatient() {
		return patient;
	}
	
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	
	
	
}
