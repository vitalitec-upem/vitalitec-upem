package com.vitalitec.lightmed.model;

import java.util.Date;

public class Agenda {
	
	public Long id;
	public Date date;
	public String title;
	public String message;
	
	public Agenda(Long id, Date date, String title, String message) {
		this.id = id;
		this.date = date;
		this.title = title;
		this.message = message;
	}
	
	
}
