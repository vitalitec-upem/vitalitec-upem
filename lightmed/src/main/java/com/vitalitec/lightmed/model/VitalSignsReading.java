package com.vitalitec.lightmed.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "VITAL_SIGNS_READING_TABLE")
public class VitalSignsReading extends Act {

	private float temperature;
	private float bloodPresure;
	private float oxygenLevel;
	private float pulse;
	private int respiratoryRate;

	public VitalSignsReading() {
		super();
	}

	public VitalSignsReading(String note, Date createdOn, Date realisationDate, boolean published, boolean result,
			MedicalFile medicalFile, AppUser createdBy, AppUser assignated, List<Document> documents, float temperature,
			float bloodPresure, float oxygenLevel, float pulse, int respiratoryRate) {
		super(note, createdOn, realisationDate, published, result, medicalFile, createdBy, assignated, documents);
		this.setActType(Acts.VITAL_SIGNS_READING);
		this.temperature = temperature;
		this.bloodPresure = bloodPresure;
		this.oxygenLevel = oxygenLevel;
		this.pulse = pulse;
		this.respiratoryRate = respiratoryRate;
	}

	public float getTemperature() {
		return temperature;
	}

	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}

	public float getBloodPresure() {
		return bloodPresure;
	}

	public void setBloodPresure(float bloodPresure) {
		this.bloodPresure = bloodPresure;
	}

	public float getOxygenLevel() {
		return oxygenLevel;
	}

	public void setOxygenLevel(float oxygenLevel) {
		this.oxygenLevel = oxygenLevel;
	}

	public float getPulse() {
		return pulse;
	}

	public void setPulse(float pulse) {
		this.pulse = pulse;
	}

	public int getRespiratoryRate() {
		return respiratoryRate;
	}

	public void setRespiratoryRate(int respiratoryRate) {
		this.respiratoryRate = respiratoryRate;
	}

}
