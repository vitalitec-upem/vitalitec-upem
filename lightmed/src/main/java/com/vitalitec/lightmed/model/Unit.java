package com.vitalitec.lightmed.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "UNIT_TABLE")
@JsonIgnoreProperties(value = { "users", "patients" })
public class Unit {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String name;
	private String description;
	private String type;
	
	@ManyToOne
	private Unit parent;
	
	@OneToMany(mappedBy="unit")
	private List<AppUser> users;
	
	@OneToMany(mappedBy="unit")
	private List<Patient> patients;
	
	public Unit() {}
	public Unit(String name, String description, String type, Unit parent, List<AppUser> users, List<Patient> patients) {
		this.name = name;
		this.description = description;
		this.type = type;
		this.parent = parent;
		this.users = users;
		this.patients = patients;
	}
	
	public Unit(String name, String description, String type) {
		this.name = name;
		this.description = description;
		this.type = type;
		this.parent = null;
		this.users = null;
		this.patients = null;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public Unit getParent() {
		return parent;
	}
	
	public void setParent(Unit parent) {
		this.parent = parent;
	}
	
	public List<AppUser> getUsers() {
		return users;
	}
	
	public void setUsers(List<AppUser> users) {
		this.users = users;
	}
	
	public List<Patient> getPatients() {
		return patients;
	}
	
	public void setPatients(List<Patient> patients) {
		this.patients = patients;
	}
	
}
