package com.vitalitec.lightmed.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "MEDICAL_IMAGING_TABLE")
public class MedicalImaging extends Act {

	public MedicalImaging() {
		super();
		this.setActType(Acts.MEDICAL_IMAGING);
	}
	
}
