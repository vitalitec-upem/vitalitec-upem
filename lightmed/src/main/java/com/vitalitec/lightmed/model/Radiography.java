package com.vitalitec.lightmed.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "RADIOGRAPHY_TABLE")
public class Radiography extends Act {

	public Radiography() {
		super();
	}

	public Radiography(String note, Date createdOn, Date realisationDate, boolean published, boolean result,
			MedicalFile medicalFile, AppUser createdBy, AppUser assignated, List<Document> documents) {
		super(note, createdOn, realisationDate, published, result, medicalFile, createdBy, assignated, documents);
		this.setActType(Acts.RADIOGRAPHY);
	}
}
