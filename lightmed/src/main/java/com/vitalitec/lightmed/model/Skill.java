package com.vitalitec.lightmed.model;

import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "SKILL_TABLE")
@JsonIgnoreProperties(value = { "users" })
public class Skill {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String name;
	
	@ManyToMany(mappedBy = "skills")
    private Set<AppUser> users;

	@ManyToOne
	private Unit parent;
	
	public Skill() {}

	public Skill(String name, Unit parent, Set<AppUser> users) {
		this.parent = parent;
		this.name = name;
		this.users = users;
	}

	public long getId() {
		return id;
	}
	
	public Unit getParent() {
		return parent;
	}
	
	public void setParent(Unit parent) {
		this.parent = parent;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<AppUser> getUsers() {
		return users;
	}

	public void setUsers(Set<AppUser> users) {
		this.users = users;
	}
	
}
