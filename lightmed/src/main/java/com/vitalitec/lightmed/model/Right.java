package com.vitalitec.lightmed.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "RIGHT_TABLE")
public class Right {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String name;
	private String entity;
	private boolean canRead;
	private boolean canWrite;
	private boolean canCreate;
	private boolean canDelete;
	
	@ManyToOne
	private Role role;
	
	public Right() {}
	public Right(String name, String entity, boolean canRead, boolean canWrite, boolean canCreate, boolean canDelete, Role role) {
		this.name = name;
		this.entity = entity;
		this.canRead = canRead;
		this.canWrite = canWrite;
		this.canCreate = canCreate;
		this.canDelete = canDelete;
		this.role = role;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEntity() {
		return entity;
	}
	
	public void setEntity(String entity) {
		this.entity = entity;
	}
	
	public boolean isCanRead() {
		return canRead;
	}
	
	public void setCanRead(boolean canRead) {
		this.canRead = canRead;
	}
	
	public boolean isCanWrite() {
		return canWrite;
	}
	
	public void setCanWrite(boolean canWrite) {
		this.canWrite = canWrite;
	}
	
	public boolean isCanCreate() {
		return canCreate;
	}
	
	public void setCanCreate(boolean canCreate) {
		this.canCreate = canCreate;
	}
	
	public boolean isCanDelete() {
		return canDelete;
	}
	
	public void setCanDelete(boolean canDelete) {
		this.canDelete = canDelete;
	}
	
	public Role getRole() {
		return role;
	}
	
	public void setRole(Role role) {
		this.role = role;
	}
	
}
