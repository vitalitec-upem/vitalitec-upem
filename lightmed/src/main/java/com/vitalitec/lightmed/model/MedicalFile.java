package com.vitalitec.lightmed.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "MEDICAL_FILE_TABLE")
public class MedicalFile {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private boolean published;
	private boolean archived;
	private Date archivedOn;
	private Date updatedOn;
	private Date createdOn;
	
	@ManyToOne (cascade = {CascadeType.ALL})
	private Patient patient;
	
	@ManyToOne
	private AppUser createdBy;
	
	@OneToMany(mappedBy="medicalFile", cascade = {CascadeType.ALL})
	private List<Act> acts;
	
	public MedicalFile() {}

	public MedicalFile(boolean published, boolean archived, Date archivedOn, Date updatedOn, Date createdOn, Patient patient, AppUser createdBy, List<Act> acts) {
		this.published = published;
		this.archived = archived;
		this.archivedOn = archivedOn;
		this.updatedOn = updatedOn;
		this.createdOn = createdOn;
		this.patient = patient;
		this.createdBy = createdBy;
		this.acts = acts;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isPublished() {
		return published;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public Date getArchivedOn() {
		return archivedOn;
	}

	public void setArchivedOn(Date archivedOn) {
		this.archivedOn = archivedOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public AppUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AppUser createdBy) {
		this.createdBy = createdBy;
	}

	public List<Act> getActs() {
		return acts;
	}

	public void setActs(List<Act> acts) {
		this.acts = acts;
	}
	
}
