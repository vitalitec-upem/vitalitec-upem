package com.vitalitec.lightmed.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ACT_TABLE")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@JsonIgnoreProperties(value = { "medicalFile" })
public class Act {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private int actType;
	private String note;
	private Date createdOn;
	private Date realisationDate;
	private boolean published;
	private boolean result;
	
	@ManyToOne
	private MedicalFile medicalFile;
	
	@ManyToOne
	private AppUser createdBy;
	
	@ManyToOne
	private AppUser assignated;
	
	@OneToMany(mappedBy="act")
	private List<Document> documents;
	
	public Act() {}
	public Act(String note, Date createdOn, Date realisationDate, boolean published, boolean result, MedicalFile medicalFile, AppUser createdBy, AppUser assignated, List<Document> documents) {
		super();
		this.note = note;
		this.createdOn = createdOn;
		this.realisationDate = realisationDate;
		this.published = published;
		this.result = result;
		this.medicalFile = medicalFile;
		this.createdBy = createdBy;
		this.assignated = assignated;
		this.documents = documents;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getNote() {
		return note;
	}
	
	public void setNote(String note) {
		this.note = note;
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}
	
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Date getRealisationDate() {
		return realisationDate;
	}
	
	public void setRealisationDate(Date realisationDate) {
		this.realisationDate = realisationDate;
	}
	
	public boolean isPublished() {
		return published;
	}
	
	public void setPublished(boolean published) {
		this.published = published;
	}
	
	public boolean isResult() {
		return result;
	}
	
	public void setResult(boolean result) {
		this.result = result;
	}
	
	public MedicalFile getMedicalFile() {
		return medicalFile;
	}
	
	public void setMedicalFile(MedicalFile medicalFile) {
		this.medicalFile = medicalFile;
	}
	
	public AppUser getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(AppUser createdBy) {
		this.createdBy = createdBy;
	}
	
	public AppUser getAssignated() {
		return assignated;
	}
	
	public void setAssignated(AppUser assignated) {
		this.assignated = assignated;
	}
	
	public List<Document> getDocuments() {
		return documents;
	}
	
	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}
	public int getActType() {
		return actType;
	}
	public void setActType(int type) {
		this.actType = type;
	}
	
}
