package com.vitalitec.lightmed.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vitalitec.lightmed.model.Address;

public interface AddressRepository extends JpaRepository<Address, Long > {

}
