package com.vitalitec.lightmed.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vitalitec.lightmed.model.Patient;

public interface PatientRepository extends JpaRepository<Patient, Long> {

}
