package com.vitalitec.lightmed.repository;

import com.vitalitec.lightmed.model.Address;
import com.vitalitec.lightmed.model.Document;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentRepository extends JpaRepository<Document, Long > {
}
