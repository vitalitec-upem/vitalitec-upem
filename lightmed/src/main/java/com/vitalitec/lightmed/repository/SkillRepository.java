package com.vitalitec.lightmed.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vitalitec.lightmed.model.Skill;
import com.vitalitec.lightmed.model.Unit;

public interface SkillRepository extends JpaRepository<Skill, Long> {
	Optional<Skill> findByName(String name);
	List<Skill> findByParent(Unit parent);
	
}
