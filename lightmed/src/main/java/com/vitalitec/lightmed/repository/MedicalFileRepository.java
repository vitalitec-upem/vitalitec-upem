package com.vitalitec.lightmed.repository;

import java.util.List;
import java.util.Optional;

import com.vitalitec.lightmed.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vitalitec.lightmed.model.MedicalFile;

@Repository
public interface MedicalFileRepository extends JpaRepository<MedicalFile, Long> {
	public Optional<MedicalFile> findById(long id);

	public List<MedicalFile> findByPublished(boolean published);

	public MedicalFile findByPatient(Patient patient);

	public List<MedicalFile> findByPatientPhoneNumber(String phoneNumber);

	public List<MedicalFile> findByPatientSocialSecurityNumber(String socialSecurityNumber);

	public List<MedicalFile> findByPatientFirstName(String firstName);

	public List<MedicalFile> findByPatientLastName(String lastName);

	public List<MedicalFile> findByPatientFirstNameAndPatientLastName(String firstName, String lastName);

	public List<MedicalFile> findByPatientAddressFullAddress(String fullAddress);
	
	public List<MedicalFile> findByPatientSex(boolean sex);
}
