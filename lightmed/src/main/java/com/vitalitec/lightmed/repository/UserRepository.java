package com.vitalitec.lightmed.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vitalitec.lightmed.model.AppUser;

@Repository
public interface UserRepository extends JpaRepository<AppUser, Long> {
	List<AppUser> findByFirstName(String firstName);
	Optional<AppUser> findByEmail(String email);
	List<AppUser> findByPhoneNumber(String number);
}
