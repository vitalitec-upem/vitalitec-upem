package com.vitalitec.lightmed.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vitalitec.lightmed.model.Unit;

public interface UnitRepository extends JpaRepository<Unit, Long> {
	Optional<Unit> findByName(String name);
	List<Unit> findByType(String type);
	List<Unit> findByParent(Unit parent);
}
