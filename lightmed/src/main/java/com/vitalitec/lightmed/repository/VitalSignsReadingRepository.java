package com.vitalitec.lightmed.repository;

import com.vitalitec.lightmed.model.VitalSignsReading;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VitalSignsReadingRepository extends JpaRepository<VitalSignsReading, Long> {
}
