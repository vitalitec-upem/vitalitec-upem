package com.vitalitec.lightmed.repository;

import com.vitalitec.lightmed.model.MedicalFile;
import com.vitalitec.lightmed.model.Radiography;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RadiographyRepository extends JpaRepository<Radiography, Long> {

    public Optional<MedicalFile> findById(long id);
}
