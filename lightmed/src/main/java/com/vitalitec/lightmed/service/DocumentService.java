package com.vitalitec.lightmed.service;

public interface DocumentService {
    void createDocument(String path, String type, Long actId);
}
