package com.vitalitec.lightmed.service;


import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


public interface StorageService {
	
	public void init();

	boolean store(MultipartFile file) throws IllegalArgumentException, IOException;

	public void deleteAll();
}
