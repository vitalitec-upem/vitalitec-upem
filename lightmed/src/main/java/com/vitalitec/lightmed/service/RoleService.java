package com.vitalitec.lightmed.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vitalitec.lightmed.model.Role;
import com.vitalitec.lightmed.repository.RoleRepository;

@Service
public class RoleService {
	@Autowired
	RoleRepository roleRepository;

	public Optional<Role> getRoleByName(String name) {
		return roleRepository.findByName(name);
	}

	public void createRole(Role role) throws IllegalStateException  {
		Objects.requireNonNull(role);

		if (role.getName() == null || role.getName().isEmpty()) {
			throw new IllegalArgumentException("Role name is empty or null");
		}

		if (roleRepository.findByName(role.getName()).isPresent()) {
			throw new IllegalStateException("Role already exist");
		}


		roleRepository.save(role);
	}

	public List<Role> getAllRoles(){

		return roleRepository.findAll();
	}
}
