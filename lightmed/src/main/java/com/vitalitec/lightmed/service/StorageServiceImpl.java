package com.vitalitec.lightmed.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class StorageServiceImpl implements StorageService {

	private final Path rootLocation;

    @Autowired
    public StorageServiceImpl(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    @Autowired
    public FileChecker fileChecker;

    @Override
    public boolean store(MultipartFile file) throws IllegalArgumentException, IOException {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());

        if(!fileChecker.checkName(file)) {
            throw new IllegalArgumentException("Empty file or wrong file name");
        }

        if(!fileChecker.checkAllowedExtension(file)){
            throw new IllegalArgumentException("The file is not authorized");
        }

        if(!fileChecker.checkSize(file)){
            throw new IllegalArgumentException("The file is too big");
        }

        file.transferTo(this.rootLocation.resolve(filename));

        return true;
}

    @Override
    public void init() {
        try {
            Files.createDirectories(rootLocation);
        }
        catch (IOException e) {
            return;
        }
}

	@Override
	public void deleteAll() {
		FileSystemUtils.deleteRecursively(rootLocation.toFile());
	}

}
