package com.vitalitec.lightmed.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vitalitec.lightmed.model.Unit;
import com.vitalitec.lightmed.repository.UnitRepository;

@Service
public class UnitService {
	@Autowired
	UnitRepository unitRepository;

	public Optional<Unit> getUnitByName(String name) {
		return unitRepository.findByName(name);
	}
	
	public List<Unit> getUnitByParent(Unit parent){
		return unitRepository.findByParent(parent);
	}
	
	public List<Unit> getPolesByHospital(Unit hospital) {
		return unitRepository.findByParent(hospital);
	}
	
	public List<Unit> getAllServices() {
		return unitRepository.findByType("service");
	}
	
	public void createUnit(Unit unit) {

		Objects.requireNonNull(unit);
		if (unit.getName() == null || unit.getName().isEmpty()) {
			throw new IllegalArgumentException("Unit name is null or empty");
		}
		if (unitRepository.findByName(unit.getName()).isPresent()) {
			throw new IllegalStateException("Unit already exist");
		}
		if (unit.getParent() != null)
			System.out.println("Parent: " + unit.getParent().getName());
		else
			System.out.println("No parent...");
		unitRepository.save(unit);
	}

	public List<Unit> getAllUnits() {

		return unitRepository.findAll();
	}

}
