package com.vitalitec.lightmed.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.vitalitec.lightmed.model.AppUser;
import com.vitalitec.lightmed.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public Optional<AppUser> findByEmail(String email) {
		return this.userRepository.findByEmail(email);
	}

	public AppUser getProfile(String email) {
 		if (email == null) {
			throw new IllegalArgumentException("GetProfile : email is null");
		}

		AppUser res = userRepository.findByEmail(email).orElse(null);
		res.setPassword("");

		return res;
	}

	public AppUser getUser() {
		SecurityContext context = SecurityContextHolder.getContext();
		if (context == null) {
			return null;
		}
        Authentication authentication = context.getAuthentication();
        Optional<AppUser> user = this.findByEmail(authentication.getName());
        return user.isPresent() ? user.get() : null;
	}
}
