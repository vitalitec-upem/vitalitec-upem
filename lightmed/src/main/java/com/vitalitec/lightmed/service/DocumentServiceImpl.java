package com.vitalitec.lightmed.service;

import com.vitalitec.lightmed.model.Document;
import com.vitalitec.lightmed.model.Radiography;
import com.vitalitec.lightmed.repository.DocumentRepository;
import com.vitalitec.lightmed.repository.RadiographyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    RadiographyRepository radiographyRepository;

    @Autowired
    DocumentRepository documentRepository;

    @Override
    public void createDocument(String path, String type, Long actId) {
        Radiography radiography = radiographyRepository.findById(actId).orElse(null);
        Document document = new Document(path, type, radiography);
        documentRepository.save(document);
    }
}
