package com.vitalitec.lightmed.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Component
public class FileChecker {

    @Autowired
    private Environment environment;

    private List<String> allowedExtensions = new ArrayList<>();

    public FileChecker(){
        // IMAGE
        allowedExtensions.add("jpeg");
        allowedExtensions.add("png");
        allowedExtensions.add("gif");
        allowedExtensions.add("jpg");
        // PDF
        allowedExtensions.add("pdf");
        // VIDEO
        allowedExtensions.add("mp4");
        allowedExtensions.add("avi");
    }

    public boolean checkName(MultipartFile file){
        String fileName = getFileName(file);

        if (file.isEmpty() || fileName.contains("..")) {
            return false;
        }

        return true;
    }

    public boolean checkAllowedExtension(MultipartFile file){
        return allowedExtensions.contains(getExtension(file));
    }

    private String getFileName(MultipartFile file){
        return StringUtils.cleanPath(file.getOriginalFilename());
    }

    private String getExtension(MultipartFile file){
        String fileName = getFileName(file);
        String extension = "";

        int index = fileName.lastIndexOf('.');

        if(index > 0){
            extension = fileName.substring(index + 1);
        }

        return extension;
    }

    public boolean checkSize(MultipartFile file){
        long maxSize = Long.valueOf(environment.getProperty("spring.servlet.multipart.max-file-size"));

        if(file.getSize() > maxSize){
            return false;
        }

        return true;
    }
}
