package com.vitalitec.lightmed.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vitalitec.lightmed.model.Skill;
import com.vitalitec.lightmed.model.Unit;
import com.vitalitec.lightmed.repository.SkillRepository;

@Service
public class SkillService {


	@Autowired
	SkillRepository skillRepository;


	public void createSkill(Skill skill) throws IllegalStateException  {

		Objects.requireNonNull(skill);

		if (skill.getName() == null || skill.getName().isEmpty()) {
			throw new IllegalArgumentException("Skill name is null or empty");
		}

		if (skillRepository.findByName(skill.getName()).isPresent()) {
			throw new IllegalStateException("Skill already exist");
		}

		skillRepository.save(skill);
	}

	public List<Skill> getAllSkills(){

		return skillRepository.findAll();
	}
	
	public Optional<Skill> getSkillByName(String name) {
		return skillRepository.findByName(name);
	}
	
	public List<Skill> getUnitByParent(Unit parent){
		return skillRepository.findByParent(parent);
	}
}
