package com.vitalitec.lightmed.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.vitalitec.lightmed.model.AppUser;
import com.vitalitec.lightmed.model.Role;
import com.vitalitec.lightmed.repository.RoleRepository;
import com.vitalitec.lightmed.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService  {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
        Optional<AppUser> optionalUser = this.userRepository.findByEmail(username);
		 
        if (!optionalUser.isPresent()) {
            System.out.println("User not found! " + username);
            throw new UsernameNotFoundException("User " + username + " was not found in the database");
        }
        AppUser appUser = optionalUser.get();
        System.out.println("Found User: " + appUser);
 
        // [ROLE_USER, ROLE_ADMIN,..]
        Optional<Role> roleName = this.roleRepository.findByName(appUser.getRole().getName());
 
        List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
        if (roleName.isPresent()) {
        	GrantedAuthority authority = new SimpleGrantedAuthority(roleName.get().getName());
            grantList.add(authority);
        }
 
        UserDetails userDetails = (UserDetails) new User(appUser.getEmail(), //
                appUser.getPassword(), grantList);
        System.out.println("Returning user with " + grantList.toString());
        System.out.println("Mdp: " + appUser.getPassword());
        return userDetails;
	}

}
