package com.vitalitec.lightmed.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vitalitec.lightmed.model.Acts;
import com.vitalitec.lightmed.model.MedicalFile;
import com.vitalitec.lightmed.model.Radiography;
import com.vitalitec.lightmed.model.VitalSignsReading;
import com.vitalitec.lightmed.repository.RadiographyRepository;
import com.vitalitec.lightmed.repository.VitalSignsReadingRepository;

@Service
public class ActServiceImpl implements ActService {

	@Autowired
	private VitalSignsReadingRepository vitalSignsReadingRepository;

	@Autowired
	private RadiographyRepository radiographyRepository;

	@Autowired
	private MedicalFileService medicalFileService;
	
	@Autowired
	private UserService userService;

	@Override
	public int[] getAvailableActs() {
		return new int[] { Acts.VITAL_SIGNS_READING, Acts.RADIOGRAPHY};
	}

	private Date getRealisationDate(String date) {
		Date realisationDate;
		try {
			realisationDate = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE).parse(date);
			System.out.println(realisationDate);
		} catch (ParseException e) {
			realisationDate = null;
		}
		return realisationDate;
	}

	public void createVitalSignsReading(String note, String realisationDateString, float temperature,
			float bloodPressure, float oxygenLevel, float pulse, int respiratoryRate, long medicalFileId) {
		Date realisationDate = getRealisationDate(realisationDateString);

		MedicalFile medicalFile = medicalFileService.findById(medicalFileId).orElse(null);

		VitalSignsReading vitalSignsReading = new VitalSignsReading(note, new Date(), realisationDate, true, true,
				medicalFile, userService.getUser(), null, null, temperature, bloodPressure, oxygenLevel, pulse, respiratoryRate);

		vitalSignsReadingRepository.save(vitalSignsReading);

	}

	public long createRadiography(String note, String realisationDateString, long medicalFileId) {
		Date realisationDate = getRealisationDate(realisationDateString);
		MedicalFile medicalFile = medicalFileService.findById(medicalFileId).orElse(null);

		Radiography radiography = new Radiography(note, new Date(), realisationDate, true, true, medicalFile, userService.getUser(),
				null, null);

		return radiographyRepository.save(radiography).getId();
	}

	@Override
	public VitalSignsReading findVitalSignsReadingById(Long id) {
		return vitalSignsReadingRepository.findById(id).orElse(null);
	}
}
