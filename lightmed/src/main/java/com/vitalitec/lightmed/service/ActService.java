package com.vitalitec.lightmed.service;

import com.vitalitec.lightmed.model.VitalSignsReading;

public interface ActService {
	void createVitalSignsReading(String note, String realisationDateString, float temperature, float bloodPressure,
			float oxygenLevel, float pulse, int respiratoryRate, long medicalFileId);

	long createRadiography(String note, String realisationDateString, long medicalFileId);

	int[] getAvailableActs();
	
	VitalSignsReading findVitalSignsReadingById(Long id);
}
