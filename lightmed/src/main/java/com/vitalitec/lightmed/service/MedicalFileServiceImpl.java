package com.vitalitec.lightmed.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.vitalitec.lightmed.controller.NotificationController;
import com.vitalitec.lightmed.model.MedicalFile;
import com.vitalitec.lightmed.model.Patient;
import com.vitalitec.lightmed.repository.MedicalFileRepository;

@Service
public class MedicalFileServiceImpl implements MedicalFileService {

	@Autowired
	private MedicalFileRepository medicalFileRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Optional<MedicalFile> findById(long id) {
		return medicalFileRepository.findById(id);
	}

	@Override
	public List<MedicalFile> searchMedicalFiles(Patient patient) {
		TypedQuery<MedicalFile> query = getQuery(patient);
		if (query != null) {
			return query.getResultList();
		} else {
			return new ArrayList<MedicalFile>();
		}
	}

	private TypedQuery<MedicalFile> getQuery(Patient patient) {

		ArrayList<String> conditions = new ArrayList<>();
		HashMap<String, Object> parameters = new HashMap<>();
		TypedQuery<MedicalFile> query;
		String queryString = "SELECT file FROM MedicalFile file LEFT JOIN Patient p ON file.patient = p.id "
				+ "LEFT JOIN Address a ON p.address = a.id ";

		if (patient.getSocialSecurityNumber() != null && !patient.getSocialSecurityNumber().isEmpty()) {
			conditions.add("p.socialSecurityNumber LIKE :socialSecurityNumber");
			parameters.put("socialSecurityNumber", patient.getSocialSecurityNumber());
		}

		if (patient.getAddress() != null && patient.getAddress().getFullAddress() != null
				&& !patient.getAddress().getFullAddress().isEmpty()) {
			conditions.add("a.fullAddress LIKE :fullAddress");
			parameters.put("fullAddress", patient.getAddress().getFullAddress());
		}

		if (patient.getPhoneNumber() != null && !patient.getPhoneNumber().isEmpty()) {
			conditions.add("p.phoneNumber LIKE :phoneNumber");
			parameters.put("phoneNumber", patient.getPhoneNumber());
		}

		if (patient.getFirstName() != null && !patient.getFirstName().isEmpty()) {
			conditions.add("p.firstName LIKE :firstName");
			parameters.put("firstName", patient.getFirstName());
		}

		if (patient.getLastName() != null && !patient.getLastName().isEmpty()) {
			conditions.add("p.lastName LIKE :lastName");
			parameters.put("lastName", patient.getLastName());
		}

		if (patient.getSex() != null) {
			conditions.add("p.sex = :sex");
			parameters.put("sex", patient.getSex());
		}

		if (!conditions.isEmpty()) {
			query = entityManager.createQuery(queryString.concat("WHERE " + String.join(" AND ", conditions)),
					MedicalFile.class);
			parameters.entrySet().forEach(e -> query.setParameter(e.getKey(), e.getValue()));
		} else {
			query = null;
		}

		return query;
	}

	public void updatePatient(Patient patient) {
		RestTemplate restTemplate = new RestTemplate();
		MedicalFile medicalFile = medicalFileRepository.findByPatient(patient);

		if (medicalFile != null) {
			String message = "Le patient " + patient.getFirstName() + " " + patient.getLastName() + " a été modifié.";
			String uri = NotificationController.NOTIF_ROOT_URL + "/notifications/android?id=1&message=" + message;
			medicalFile.setPatient(patient);
			medicalFileRepository.save(medicalFile);
			restTemplate.postForObject(uri, null, String.class);
		}
	}

}
