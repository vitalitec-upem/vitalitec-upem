package com.vitalitec.lightmed.service;

import java.util.List;
import java.util.Optional;

import com.vitalitec.lightmed.model.MedicalFile;
import com.vitalitec.lightmed.model.Patient;

public interface MedicalFileService {
	Optional<MedicalFile> findById(long id);
	
	public List<MedicalFile> searchMedicalFiles(Patient patient);

	public void updatePatient(Patient patient);
}

