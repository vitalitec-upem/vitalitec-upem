package com.vitalitec.lightmed.service;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.vitalitec.lightmed.model.AppUser;
import com.vitalitec.lightmed.model.Role;
import com.vitalitec.lightmed.model.Skill;
import com.vitalitec.lightmed.model.Unit;
import com.vitalitec.lightmed.repository.RoleRepository;
import com.vitalitec.lightmed.repository.SkillRepository;
import com.vitalitec.lightmed.repository.UnitRepository;
import com.vitalitec.lightmed.repository.UserRepository;

@Service
public class AdminService {

	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	UnitRepository unitRepository;
	
	@Autowired
	SkillRepository skillRepository;
	
	@Autowired
	UserRepository userRepository;
	
	public List<Unit> getAllServices() {
		return unitRepository.findByType("service");
	}
	
	public List<Unit> getAllHospitals() {
		return unitRepository.findByType("hospital");
	}
	
	public List<Role> getAllRoles() {
		return roleRepository.findAll();
	}
	
	public List<Unit> getAllUnits() {
		return unitRepository.findAll();
	}
	
	public List<Skill> getAllSkills() {
		return skillRepository.findAll();
	}
	
	private String generatePassword() {
	    int length = 12;
	    boolean useLetters = true;
	    boolean useNumbers = false;
	    return RandomStringUtils.random(length, useLetters, useNumbers);	 
	}
	
	public String createUser(AppUser user) {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

		if (!user.getEmail().isEmpty() && userRepository.findByEmail(user.getEmail()).isPresent())
			return null;
		String generatedString = generatePassword();
	    user.setPassword(bCryptPasswordEncoder.encode(generatedString));
		userRepository.save(user);
		return generatedString;
	}
	
}
