package com.vitalitec.lightmed;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import com.vitalitec.lightmed.service.StorageProperties;
import com.vitalitec.lightmed.service.StorageService;

@SpringBootApplication(scanBasePackages = { "com.vitalitec" })
@EnableConfigurationProperties(StorageProperties.class)
public class LightmedApplication {


	public static void main(String[] args) {
		SpringApplication.run(LightmedApplication.class, args);
	}
	
	@Bean
    CommandLineRunner init(StorageService storageService) {
        return (args) -> {
            storageService.deleteAll();
            storageService.init();
        };
	}

}
