package com.vitalitec.lightmed.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vitalitec.lightmed.model.Act;
import com.vitalitec.lightmed.model.AppUser;
import com.vitalitec.lightmed.model.MedicalFile;
import com.vitalitec.lightmed.model.Patient;
import com.vitalitec.lightmed.repository.MedicalFileRepository;
import com.vitalitec.lightmed.service.MedicalFileService;
import com.vitalitec.lightmed.service.UserService;

@RestController
@RequestMapping("/medicalFile")
public class MedicalFileController {

	@Autowired
	private MedicalFileService medicalFileService;

	@Autowired
	private MedicalFileRepository medicalFileRepository;

	@Autowired
	private UserService userService;

	@PostMapping("/searchByPatient")
	public List<MedicalFile> searchByPatient(@RequestBody final Patient patient) {
		return medicalFileService.searchMedicalFiles(patient);
	}

	@PostMapping("/createMedicalFile")
	public Long createMedicalFile(@RequestBody final Patient patient) {
		Date now = new Date();
		ArrayList<Act> acts = new ArrayList<>();
		AppUser user = userService.getUser();

		MedicalFile medicalFile = new MedicalFile(false, false, now, now, now, patient, user, acts);
		medicalFileRepository.save(medicalFile);
		return medicalFile.getId();
	}

	@GetMapping("/findById/{id}")
	public MedicalFile findById(@PathVariable final Long id) {
		return medicalFileService.findById(id).orElse(null);
	}

	@PostMapping("/updatePatient")
	public void updatePatient(@RequestBody final Patient patient){
		medicalFileService.updatePatient(patient);
	}

}
