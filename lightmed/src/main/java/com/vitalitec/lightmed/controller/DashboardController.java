package com.vitalitec.lightmed.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vitalitec.lightmed.model.MedicalFile;
import com.vitalitec.lightmed.repository.MedicalFileRepository;
import com.vitalitec.lightmed.service.MedicalFileService;

@Controller
@RequestMapping("/dashboard")
public class DashboardController {
	private static final Logger log = LoggerFactory.getLogger(DashboardController.class);

	@Autowired
	private MedicalFileService medicalFileService;
	
	@GetMapping("/getMedicalFile/{id}")
	public @ResponseBody MedicalFile getMedicalFileFromDashboard(@PathVariable long id) {
		Optional<MedicalFile> dmp = medicalFileService.findById(id);
		if (dmp.isPresent())
			return dmp.get();
		return null;
	}

}