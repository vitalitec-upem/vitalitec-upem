package com.vitalitec.lightmed.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.vitalitec.lightmed.service.StorageService;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class FileUploadController {
	
	@Autowired
	private StorageService storageService;

	@PostMapping("/upload")
    public Map<String, String> uploadFileController(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
        HashMap<String, String> map = new HashMap<>();

        try {
            storageService.store(file);
        } catch (IllegalArgumentException | IOException e){
            map.put("success", "false");
            map.put("cause", e.getMessage());
            return map;
        }

        map.put("success", "true");

        return map;
	}
}
