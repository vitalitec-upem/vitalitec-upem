package com.vitalitec.lightmed.controller;

import com.vitalitec.lightmed.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vitalitec.lightmed.model.VitalSignsReading;
import com.vitalitec.lightmed.service.ActService;

@RestController
@RequestMapping("/act")
public class ActController {

	@Autowired
	ActService actService;

	@Autowired
	DocumentService documentService;

	@GetMapping(value = "/getAvailableActs")
	public int[] getAvailableActs() {
		return actService.getAvailableActs();
	}

	@PostMapping(value = "/createVitalSignsReading")
	public void createVitalSignsReading(@RequestParam("note") String note,
										@RequestParam("realisationDate") String realisationDate, @RequestParam("temperature") float temperature,
										@RequestParam("bloodPressure") float bloodPressure, @RequestParam("oxygenLevel") float oxygenLevel,
										@RequestParam("pulse") float pulse, @RequestParam("respiratoryRate") int respiratoryRate,
										@RequestParam("medicalFileId") long medicalFileId) {
		actService.createVitalSignsReading(note, realisationDate.replace("\"", ""), temperature, bloodPressure,
				oxygenLevel, pulse, respiratoryRate, medicalFileId);
	}

	@PostMapping(value = "/createRadiography")
	public Long createRadiography(@RequestParam("note") String note,
								  @RequestParam("realisationDate") String realisationDate,
								  @RequestParam("medicalFileId") long medicalFileId) {
		return actService.createRadiography(note, realisationDate.replace("\"", ""), medicalFileId);
	}


	@GetMapping("/getVitalSignsReading/{id}")
	public VitalSignsReading getVitalSignsReading(@PathVariable("id") Long id) {
		return actService.findVitalSignsReadingById(id);
	}

	@PostMapping(value = "/createDocument")
	public void createDocument(@RequestParam("path") String path,
							   @RequestParam("type") String type,
							   @RequestParam("actId") long actId) {
		documentService.createDocument(path, type, actId);
	}
}
