package com.vitalitec.lightmed.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/notification")
public class NotificationController {
	
	public static final String NOTIF_ROOT_URL = "http://localhost:1818";
	
	@GetMapping("/notify/{message}")
    public String getNotification(@PathVariable String message) {
		System.out.println("in notify method");
        final String uri = "http://localhost:1818/notifications/android?id=1&message=" + message;
             
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.postForObject(uri, null, String.class);
        System.out.println(result);
        return "Notification sent !";
    }
}
