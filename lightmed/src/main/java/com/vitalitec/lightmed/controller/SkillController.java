package com.vitalitec.lightmed.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vitalitec.lightmed.model.Skill;
import com.vitalitec.lightmed.service.SkillService;

@RestController
@RequestMapping("/skill")
public class SkillController {

	@Autowired
	SkillService skillService;

	@GetMapping("/getAllSkills")
	public List<Skill> getAllSkills(){

		return skillService.getAllSkills();
	}

	@GetMapping("/getSkillByName/{skillName}")
	public Optional<Skill> getSkillByName(@PathVariable String skillName) {
		return skillService.getSkillByName(skillName);
	}

}
