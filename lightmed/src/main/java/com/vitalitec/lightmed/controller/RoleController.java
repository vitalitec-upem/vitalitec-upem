package com.vitalitec.lightmed.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vitalitec.lightmed.model.Role;
import com.vitalitec.lightmed.service.RoleService;

@RestController
@RequestMapping("/role")
public class RoleController {

	@Autowired
	RoleService roleService;

	@GetMapping("/getRoleByName/{name}")
	public Role getRoleByName(@PathVariable String name) {
		Optional<Role> ret = roleService.getRoleByName(name);
		if (ret.isPresent()) {
			return ret.get();
		}
		return null;
	}


	@GetMapping("/getAllRoles")
	public List<Role> getAllRoles(){
		return roleService.getAllRoles();
	}
}
