package com.vitalitec.lightmed.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vitalitec.lightmed.model.Unit;
import com.vitalitec.lightmed.service.UnitService;

@RestController
@RequestMapping("/unit")
public class UnitController {

	@Autowired
	UnitService unitService;

	@GetMapping("/getUnitByName/{name}")
	public Unit getUnitByName(@PathVariable String name) {
		Optional<Unit> unit = unitService.getUnitByName(name);
		if (unit.isPresent()) {
			return unit.get();
		}
		return null;
	}
	
	@GetMapping("/getAllServices")
	public List<Unit> getAllServices(){
		return unitService.getAllServices();
	}

	@GetMapping("/getAllUnits")
	public List<Unit> getAllUnits(){
		return unitService.getAllUnits();
	}

	@GetMapping("/getServicesByPole/{poleName}/{hospitalName}")
	public List<Unit> getServicesByPole(@PathVariable String poleName, @PathVariable String hospitalName) {
		Optional<Unit> hospital = unitService.getUnitByName(hospitalName);
		if(hospital.isPresent()) {
			List<Unit> poles = unitService.getUnitByParent(hospital.get());
			Optional<Unit> pole = poles.stream().filter(x -> x.getName().equals(poleName)).findFirst(); 
			if(pole.isPresent()) {
				Unit p = pole.get();
				List<Unit> services = unitService.getUnitByParent(p);
				return services;
			} 
		}
		return null;
	}
	
	@GetMapping("/getPolesByHospital/{hospitalName}")
	public List<Unit> getPolesByHospital(@PathVariable String hospitalName) {
		System.out.println("Nom de lhopital " +hospitalName);
		Optional<Unit> hospital = unitService.getUnitByName(hospitalName);
		if (hospital.isPresent()) {
			List<Unit> units = unitService.getPolesByHospital(hospital.get());
			return units.stream().filter(x -> x.getType().equals("pole")).collect(Collectors.toList());
		}
		return null;
	}
}
