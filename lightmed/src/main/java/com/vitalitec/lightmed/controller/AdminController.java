package com.vitalitec.lightmed.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vitalitec.lightmed.model.AppUser;
import com.vitalitec.lightmed.model.Role;
import com.vitalitec.lightmed.model.Skill;
import com.vitalitec.lightmed.model.Unit;
import com.vitalitec.lightmed.service.AdminService;
import com.vitalitec.lightmed.service.RoleService;
import com.vitalitec.lightmed.service.SkillService;
import com.vitalitec.lightmed.service.UnitService;

@RestController
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private AdminService adminService;

	@Autowired
	private SkillService skillService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private UnitService unitService;

	@GetMapping("/getAllHospitals")
	public List<Unit> getAllHospitals() {
		return adminService.getAllHospitals();
	}
	
	@GetMapping("/getAllServices")
	public List<Unit> getAllServices() {
		return adminService.getAllServices();
	}
	
	@GetMapping("/getAllTypes")
	public List<String> getAllTypes() {
		return new ArrayList<String>(Arrays.asList("Hospitalière", "Fonctionnelle"));
	}
	
	@GetMapping("/getAllRoles")
	public List<Role> getAllRoles() {
		return adminService.getAllRoles();
	}

	@GetMapping("/getAllUnits")
	public List<Unit> getAllUnits() {
		return adminService.getAllUnits();
	}

	@GetMapping("/getAllSkills")
	public List<Skill> getAllSkills() {
		return adminService.getAllSkills();
	}
	
	@GetMapping("/getSkillsByService/{serviceName}")
	public List<Skill> getSkillsByService(@PathVariable String serviceName) {
		Optional<Unit> service = unitService.getUnitByName(serviceName);
		if(service.isPresent()) {
			System.out.println("Service present");
			List<Skill> skills = skillService.getUnitByParent(service.get());
			return skills;
		}
		return null;
	}

	@PostMapping("/createUser")
	@ResponseBody
	public ResponseEntity<String> createUser(@RequestBody AppUser user) {
		String password = adminService.createUser(user);
		if (password != null)
			return new ResponseEntity<String>(password, HttpStatus.OK);
		return new ResponseEntity<String>(HttpStatus.NOT_ACCEPTABLE);
	}

	@PostMapping("/createSkill")
	public ResponseEntity<String> createSkill(@RequestBody final Skill skill){

		if (skill == null) {
			return ResponseEntity.badRequest().body("Skill must not be null");
		}

		try {

			skillService.createSkill(skill);
			return ResponseEntity.ok("Skill was created");

		} catch (IllegalStateException e) {
			return ResponseEntity.badRequest().body("Error occured");
		}
	}

	@PostMapping("/createRole")
	public ResponseEntity<String> createRole(@RequestBody final Role role){

		if (role == null) {
			return ResponseEntity.badRequest().body("Role must not be null");
		}

		try {
			roleService.createRole(role);
			return ResponseEntity.ok("Role was created");

		} catch (IllegalStateException e) {
			return ResponseEntity.badRequest().body("Error occured");
		}
	}

	@PostMapping("/createUnit")
	public ResponseEntity<String> createUnit(@RequestBody final Unit unit){

		if (unit == null) {
			return ResponseEntity.badRequest().body("Unit must not be null");
		}

		try {
			unitService.createUnit(unit);
			return ResponseEntity.ok("unit was created");

		} catch (IllegalStateException e) {
			return ResponseEntity.badRequest().body("Error occured");
		}
	}

}
