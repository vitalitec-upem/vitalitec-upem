package com.vitalitec.lightmed.controller;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vitalitec.lightmed.model.AppUser;	
import com.vitalitec.lightmed.service.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping(value = "/login")
	public @ResponseBody String login(Model model, String error, String logout) {
		if (error != null) {
			System.out.println("Error when loggin in");
			model.addAttribute("error", "Your username and password is invalid.");
			return "failure";
		}

		if (logout != null) {
			System.out.println("Logging out.");
			model.addAttribute("message", "You have been logged out successfully.");
		}
		System.out.println("Login success !");
		return "success";
	}

	@GetMapping("/getUser")
	public AppUser getUser() {
		return userService.getUser();
	}

	@GetMapping("/getProfile")
	public AppUser getProfile() {

		User authUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		System.out.println("FOOOOOOO: " + authUser.toString());

		return userService.getProfile(authUser.getUsername());
	}

}
