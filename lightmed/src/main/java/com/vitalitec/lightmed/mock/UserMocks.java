package com.vitalitec.lightmed.mock;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.vitalitec.lightmed.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.vitalitec.lightmed.repository.RoleRepository;
import com.vitalitec.lightmed.repository.UserRepository;

@Component
public class UserMocks {

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	private static final Logger log = LoggerFactory.getLogger(MedicalFileMock.class);

	@EventListener(ApplicationReadyEvent.class)
	public void createUserMocks() {
		log.info("Creating a user if there isn't any");
		if (userRepository.findAll().size() == 0) {
			List<AppUser> users = new ArrayList<AppUser>();
			Role userRole = new Role("ROLE_USER", users);
			Role adminRole = new Role("ROLE_ADMIN", users);
			Calendar today = Calendar.getInstance();
			today.set(Calendar.HOUR_OF_DAY, 0);
			BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
			Address ad = new Address("1", "rue de la paix", "Paris", "75001", "France");
			AppUser user2 = new AppUser("R. J.", "MacReady", new Date(1951, 3, 17), "0601020304", "macready@aphp.com",
					bcrypt.encode("MacReady"), userRole);
			AppUser user3 = new AppUser("Wilford", "Brimley", new Date(1934, 9, 27), "0601020304", "brimley@aphp.com",
					bcrypt.encode("Brimley"), userRole);
			AppUser user4 = new AppUser("Vance", "Norris", new Date(1943, 7, 29), "0601020304", "norris@aphp.com",
					bcrypt.encode("Norris"), userRole);
			AppUser user5 = new AppUser("George", "Bennings", new Date(1944, 10, 23), "0601020304", "bennings@aphp.com",
					bcrypt.encode("Bennings"), userRole);
			AppUser user6 = new AppUser("Thomas", "Waites", new Date(1955, 1, 8), "0601020304", "waites@aphp.com",
					bcrypt.encode("Waites"), adminRole);

			AppUser ichem = new AppUser("ichem", "shafie", today.getTime(), "0601020304", "toto@gmail.com",
					bcrypt.encode("password"), userRole);
			users.add(ichem);
			users.add(user2);
			users.add(user3);
			users.add(user4);
			users.add(user5);
			users.add(user6);

			userRole.setUsers(users);
			System.out.println("hello world, I have just started up");
			roleRepository.save(userRole);

			for (AppUser u : users)
				userRepository.save(u);
			System.out.println("done with the sabin,g next is loading");
			List<AppUser> test = userRepository.findByPhoneNumber("0601020304");
			// should show all users
			for (AppUser u : test) {
				log.info("test is : " + u.getEmail() + " >" + u.getFirstName() + " - " + u.getLastName());
			}
		}
	}
}