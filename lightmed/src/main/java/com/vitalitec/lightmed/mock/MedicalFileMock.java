package com.vitalitec.lightmed.mock;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.vitalitec.lightmed.model.Address;
import com.vitalitec.lightmed.model.AppUser;
import com.vitalitec.lightmed.model.MedicalFile;
import com.vitalitec.lightmed.model.Patient;
import com.vitalitec.lightmed.model.Role;
import com.vitalitec.lightmed.model.Skill;
import com.vitalitec.lightmed.model.Unit;
import com.vitalitec.lightmed.repository.AddressRepository;
import com.vitalitec.lightmed.repository.MedicalFileRepository;
import com.vitalitec.lightmed.repository.PatientRepository;
import com.vitalitec.lightmed.repository.RoleRepository;
import com.vitalitec.lightmed.repository.SkillRepository;
import com.vitalitec.lightmed.repository.UnitRepository;
import com.vitalitec.lightmed.repository.UserRepository;

@Component
public class MedicalFileMock {

	@Autowired
	private MedicalFileRepository mFileRep;
	@Autowired
	private AddressRepository addressRep;
	@Autowired
	private UserRepository userRep;
	@Autowired
	private PatientRepository patientRep;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private SkillRepository skillRepository;

	@Autowired
	private UnitRepository unitRepository;

	private static final Logger log = LoggerFactory.getLogger(MedicalFileMock.class);

	@EventListener(ApplicationReadyEvent.class)
	public void start() {
		log.info("Medical File mock STARTED");
		if (mFileRep.findAll().size() == 0) {
			// HOPITAL POLE SERVICE
			Unit aphp = new Unit("APHP", "Assistance publique hopitaux de Paris", "aphp", null, null, null);
			unitRepository.save(aphp);
			Unit hospital1 = new Unit("Kremlin-Bicetre", "Hopital du Kremlin-Bicetre", "hospital", aphp, null, null);
			unitRepository.save(hospital1);
			Unit pole1 = new Unit("Neuroscience", "Pole de neuroscience", "pole", hospital1, null, null);
			unitRepository.save(pole1);
			Unit service1 = new Unit("Neurochirurgie", "Service de neurochirurgie", "service", pole1, null, null);
			unitRepository.save(service1);
			Unit pole2 = new Unit("Cardio-vasculaire", "Pole cardio-vasculaire", "pole", hospital1, null, null);
			unitRepository.save(pole2);
			Unit service2 = new Unit("Cardiologie", "Service de cardiologie", "service", pole2, null, null);
			unitRepository.save(service2);

			// ADDRESS
			Address address = new Address("8 alle tassini, bondy, 93140, France");
			Address address2 = new Address("4, avenue MontMartre, Bof, 77900, France");
			Address address3 = new Address("12, allée des érables, Bondy, 93140, France");
			Address address4 = new Address("35 ter, boulevard des Peaky BLINDERS, Paris 15, 75015, France");
			Address address5 = new Address("43 bis, rue de la casa de papel, Vincennes, 94190, France");
			Address address6 = new Address("9680, allée des développeurs, Champs-sur-marne, 77120, France");

			// PATIENT

			Patient patient = new Patient("0000000", "Toto", "Tata", Patient.MALE, "0101010101", "...", address, null,
					null, null, null);
			Patient patient2 = new Patient("13141512", "Brice", "Granola", Patient.FEMALE, "0752013265",
					"Au bord de la mort", address2, null, null, null, null);
			Patient patient3 = new Patient("99871104", "Marie", "Joseph", Patient.FEMALE, "1050469152", "Pète la forme",
					address3, null, null, null, null);
			Patient patient4 = new Patient("194079555232589", "Antoine", "Macron", Patient.MALE, "0603456754",
					"Crise d'utilisation trop intensive du téléphone", address4, null, null, null, null);
			Patient patient5 = new Patient("193079355232589", "Therese", "Bonaparte", Patient.FEMALE, "0653254754",
					"Pète et éternue en même temps", address5, null, null, null, null);
			Patient patient6 = new Patient("184079555232689", "Alphonse", "De Gaule", Patient.MALE, "0673756754",
					"Conjonctivite", address6, null, null, null, null);

			// SKILLS

			skillRepository.save(new Skill("Spécialiste cardiologie", service2, null));
			skillRepository.save(new Skill("Spécialiste neurologie", service1, null));

			// USERS

			Calendar today = Calendar.getInstance();
			today.set(Calendar.HOUR_OF_DAY, 0);
			BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
			List<AppUser> users = new ArrayList<>();
			List<AppUser> admins = new ArrayList<>();
			Role userRole = new Role("ROLE_MEDECIN", users);
			Role adminRole = new Role("ROLE_ADMIN", admins);

			AppUser admin = new AppUser("Catherine", "Dupont", new Date(1955, 1, 8), "0601020304", "catherinedupont@aphp.fr",
					bcrypt.encode("vitalitec01"), adminRole);
			AppUser ichem = new AppUser("Maxime", "Chateau", today.getTime(), "0601020304", "maximechateau@aphp.fr",
					bcrypt.encode("password01"), userRole);

			users.add(ichem);
			admins.add(admin);
			roleRepository.save(userRole);
			roleRepository.save(adminRole);

			try {
				// Medical Files
				mFileRep.save(new MedicalFile(false, false, new Date(), new Date(), new Date(), patient, ichem, null));
				mFileRep.save(new MedicalFile(false, true, new Date(), new Date(), new Date(), patient2, ichem, null));
				mFileRep.save(new MedicalFile(true, false, new Date(), new Date(), new Date(), patient3, ichem, null));
				mFileRep.save(new MedicalFile(true, false, new Date(), new Date(), new Date(), patient4, ichem, null));
				mFileRep.save(new MedicalFile(true, false, new Date(), new Date(), new Date(), patient5, ichem, null));
				mFileRep.save(new MedicalFile(true, false, new Date(), new Date(), new Date(), patient6, ichem, null));
			} catch (Exception e) {
				log.warn("Medical file mocking failed" + e);
			}

		}
		log.info("Medical File mock ENDED");
	}

}
