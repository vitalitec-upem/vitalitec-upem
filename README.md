# Projet Délire

## Installation back

### Installer PostgreSQL sur Windows

Il vous faudra installer PostgreSQL, pour cela :

https://www.enterprisedb.com/downloads/postgres-postgresql-downloads

Puis créer une base de donnée locale.

le fichier application.properties du projet définit:

spring.datasource.url=jdbc:postgresql://localhost:5432/lightmed

spring.datasource.username= vitalitec

spring.datasource.password= Vitalitec01

Lors de l'installation, vous devrez choisir un mot de passe, gardez le en mémoire.
Le port est 5432.



Sur Windows, une fois PostgreSQL installé vous devez éxecuter "C:\Program Files\PostgreSQL\10\scripts\runpsql.bat" (où ailleurs si vous avez installé PostgreSQL ailleurs).

Laissez tous les champs par défaut sauf le mot de passe, entrez celui que vous avez spécifié lors de l'installation.

Si votre mot de passe est correct vous pouvez entrer des commandes postgresql.

Lancez ces commandes :

CREATE USER vitalitec WITH ENCRYPTED PASSWORD 'Vitalitec01';

CREATE DATABASE lightmed OWNER vitalitec;

Vous pouvez ensuite quitter.

### Installer les dépendances maven

Allez dans le dossier du projet que vous avez clone, puis dans le dossier lightmed (là où se trouve le pom.xml)

$> mvn install

Si ça ne fonctionne pas, essayez de relancer en tant qu'admin.