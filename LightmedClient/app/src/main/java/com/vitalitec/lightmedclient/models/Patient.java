package com.vitalitec.lightmedclient.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Patient implements Serializable {

    public static boolean MALE = Boolean.FALSE;
    public static boolean FEMALE = Boolean.TRUE;

    private long id;
    private static final long serialVersionUID = -311865905565780225L;
    private String socialSecurityNumber;
    private String lastName;
    private String firstName;
    private Boolean sex;
    private String phoneNumber;
    private String description;

    private Date birthDate;
    private String placeOfBirth;
    private Address address;

    private Unit unit;

    private List<MedicalHistory> medicalHistory;

    public Patient() {
    }

    public Patient(String socialSecurityNumber, String lastName, String firstName, Boolean sex, String phoneNumber,
                   String description, Address address, Unit unit, Date birthDate, String placeOfBirth,
                   List<MedicalHistory> medicalHistory) {
        this.socialSecurityNumber = socialSecurityNumber;
        this.lastName = lastName;
        this.firstName = firstName;
        this.sex = sex;
        this.phoneNumber = phoneNumber;
        this.description = description;
        this.address = address;
        this.birthDate = birthDate;
        this.placeOfBirth = placeOfBirth;
        this.unit = unit;
        this.medicalHistory = medicalHistory;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Boolean getSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public List<MedicalHistory> getMedicalHistory() {
        return medicalHistory;
    }

    public Date getBirthDate() { return birthDate; }

    public void setBirthDate(Date birthDate) { this.birthDate = birthDate; }

    public String getPlaceOfBirth() { return placeOfBirth; }

    public void setPlaceOfBirth(String placeOfBirth) { this.placeOfBirth = placeOfBirth; }

    public void setMedicalHistory(List<MedicalHistory> medicalHistory) {
        this.medicalHistory = medicalHistory;
    }
}
