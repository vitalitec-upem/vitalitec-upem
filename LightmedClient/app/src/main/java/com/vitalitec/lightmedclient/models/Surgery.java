package com.vitalitec.lightmedclient.models;

import java.util.Date;
import java.util.List;

public class Surgery extends Act {
	
	private String type;
	private float duration;
	private float oxygenLevel;
	
	public Surgery() {
		super();
	}
	public Surgery(String note, Date createdOn, Date realisationDate, boolean published, boolean result, MedicalFile medicalFile, AppUser createdBy, AppUser assignated, List<Document> documents, String type, float duration, float oxygenLevel) {
		super(2, note, createdOn, realisationDate, published, result, medicalFile, createdBy, assignated, documents);
		this.type = type;
		this.duration = duration;
		this.oxygenLevel = oxygenLevel;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public float getDuration() {
		return duration;
	}
	
	public void setDuration(float duration) {
		this.duration = duration;
	}
	
	public float getOxygenLevel() {
		return oxygenLevel;
	}
	
	public void setOxygenLevel(float oxygenLevel) {
		this.oxygenLevel = oxygenLevel;
	}
	
}
