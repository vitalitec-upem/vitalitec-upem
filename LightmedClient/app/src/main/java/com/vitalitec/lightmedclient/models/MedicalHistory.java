package com.vitalitec.lightmedclient.models;

import java.io.Serializable;
import java.util.Date;

public class MedicalHistory implements Serializable {

    private long id;
    private static final long serialVersionUID = 1L;
    private String description;
    private Date medicalHistoryDate;

    private Patient patient;

    public MedicalHistory() {}
    public MedicalHistory(String description, Date medicalHistoryDate, Patient patient) {
        this.description = description;
        this.medicalHistoryDate = medicalHistoryDate;
        this.patient = patient;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getMedicalHistoryDate() {
        return medicalHistoryDate;
    }

    public void setMedicalHistoryDate(Date date) {
        this.medicalHistoryDate = date;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
