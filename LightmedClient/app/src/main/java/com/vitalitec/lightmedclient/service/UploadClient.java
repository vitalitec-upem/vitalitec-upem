package com.vitalitec.lightmedclient.service;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface UploadClient {

    @Multipart
    @POST("upload")
    Call<UploadResponse> uploadPhoto(@Part("description")RequestBody description, @Part MultipartBody.Part file);

}
