package com.vitalitec.lightmedclient.service;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface LogoutClient {
    @GET("logout")
    Call<ResponseBody> logout();
}
