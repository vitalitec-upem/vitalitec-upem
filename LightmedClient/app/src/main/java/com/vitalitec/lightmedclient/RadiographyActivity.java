package com.vitalitec.lightmedclient;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vitalitec.lightmedclient.models.Document;
import com.vitalitec.lightmedclient.service.HttpClientService;
import com.vitalitec.lightmedclient.service.IAct;
import com.vitalitec.lightmedclient.service.RealisationDateManager;
import com.vitalitec.lightmedclient.service.UploadClient;
import com.vitalitec.lightmedclient.service.UploadResponse;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.vitalitec.lightmedclient.ContentUtils.getPath;

public class RadiographyActivity extends AppCompatActivity {
    private final int STORAGE_REQUEST_CODE = 120;
    private boolean storagePermission;
    private CoordinatorLayout coordinatorLayout;
    private EditText realisationDateEditText;
    private EditText noteEditText;
    private EditText fileNameEditText;
    private Button selectFileButton;

    private Long medicalFileId;

    private Uri fileUri;
    private String path;
    private String shortName;
    private Long actId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        setContentView(R.layout.activity_radiography);
        getSupportActionBar().setTitle(getResources().getString(R.string.radiography));
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        medicalFileId = getIntent().getExtras().getLong("medicalFileId");
        noteEditText = findViewById(R.id.note);
        fileNameEditText = findViewById(R.id.file_name);
        fileNameEditText.setFocusable(false);
        realisationDateEditText = findViewById(R.id.realisationDate);
        realisationDateEditText.setFocusable(false);
        selectFileButton = findViewById(R.id.selectFileButton);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_REQUEST_CODE);
        } else {
            storagePermission = true;
        }

        realisationDateEditText.setOnClickListener(v -> {
            RealisationDateManager.createNewDatePicker(RadiographyActivity.this, realisationDateEditText);
        });

        selectFileButton.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*"); //On upload une radio donc une image
            startActivityForResult(intent, 2);
        });

        FloatingActionButton saveButton = findViewById(R.id.saveButton);
        saveButton.setOnClickListener(v -> {
            if(validate()){
                createAct();
                createDocument();
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case STORAGE_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    storagePermission = true;
                } else {
                    storagePermission = false;
                }
                return;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        if (requestCode == 2 && resultCode == RESULT_OK) {
            if (resultData != null) {
                fileUri = resultData.getData();
                path = getPath(this, fileUri);
                shortName = path.substring(path.lastIndexOf("/") + 1);
                fileNameEditText.setText(shortName);

            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    private void createAct() {
        Retrofit retrofit = HttpClientService.getRetrofit(this);
        IAct client = retrofit.create(IAct.class);
        Call<Long> call = client.createAct(noteEditText.getText().toString(), RealisationDateManager.getRealisationDate(), medicalFileId);
        call.enqueue(new Callback<Long>() {
            @Override
            public void onResponse(Call<Long> call, Response<Long> response) {
                if (response.isSuccessful()) {
                    actId = response.body();
                    createDocument();
                }
            }

            @Override
            public void onFailure(Call<Long> call, Throwable t) {
                Snackbar.make(coordinatorLayout,
                        getResources().getString(R.string.ActCreationFailed),
                        Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void createDocument(){
        if(actId != null){
            Retrofit retrofit = HttpClientService.getRetrofit(this);
            IAct client = retrofit.create(IAct.class);
            Call<Void> call = client.createDocument(shortName, "image", actId);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.isSuccessful()) {
                        uploadFile();
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Snackbar.make(coordinatorLayout,
                            getResources().getString(R.string.ActCreationFailed),
                            Snackbar.LENGTH_LONG).show();
                }
            });
        }
    }

    private boolean validate() {
        boolean valid = true;

        String realisationDate = realisationDateEditText.getText().toString();
        String fileName = fileNameEditText.getText().toString();

        realisationDateEditText.setError(null);
        fileNameEditText.setError(null);

        if (realisationDate.isEmpty()) {
            realisationDateEditText.setError(getResources().getString(R.string.requiredField));
            valid = false;
        }

        if(fileName.isEmpty()){
            fileNameEditText.setError(getResources().getString(R.string.requiredField));
            valid = false;
        }

        return valid;
    }

    private void uploadFile() {
        if (actId != null) {
            File originalFile = new File(path);

            RequestBody descriptionPart = RequestBody.create(MultipartBody.FORM, originalFile.getName());
            RequestBody filePart = RequestBody.create(MediaType.parse(getContentResolver().getType(fileUri)), originalFile);

            MultipartBody.Part file = MultipartBody.Part.createFormData("file", originalFile.getName(), filePart);

            Retrofit retrofit = HttpClientService.getRetrofit(this);

            UploadClient client = retrofit.create(UploadClient.class);

            Call<UploadResponse> call = client.uploadPhoto(descriptionPart, file);
            call.enqueue(new Callback<UploadResponse>() {
                @Override
                public void onResponse(Call<UploadResponse> call, Response<UploadResponse> response) {
                    UploadResponse uploadReponse = response.body();
                    if (response.code() == 200 && uploadReponse.getSuccess()) {
                        finish();
                    } else {
                        Snackbar.make(coordinatorLayout,
                                getResources().getString(R.string.ActCreationFailed),
                                Snackbar.LENGTH_LONG).show();
                    }

                }

                @Override
                public void onFailure(Call<UploadResponse> call, Throwable t) {
                    Snackbar.make(coordinatorLayout,
                            getResources().getString(R.string.ActCreationFailed),
                            Snackbar.LENGTH_LONG).show();
                }

            });

        }
    }
}
