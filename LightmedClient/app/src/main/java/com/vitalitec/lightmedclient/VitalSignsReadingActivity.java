package com.vitalitec.lightmedclient;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.EditText;

import com.vitalitec.lightmedclient.models.VitalSignsReading;
import com.vitalitec.lightmedclient.service.HttpClientService;
import com.vitalitec.lightmedclient.service.IAct;
import com.vitalitec.lightmedclient.service.RealisationDateManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class VitalSignsReadingActivity extends AppCompatActivity {

    private CoordinatorLayout coordinatorLayout;

    private EditText temperatureEditText;
    private EditText bloodPressureEditText;
    private EditText oxygenLevelEditText;
    private EditText pulseEditText;
    private EditText respiratoryRateEditText;
    private EditText realisationDateEditText;
    private EditText noteEditText;

    private VitalSignsReading vitalSignsReading;
    private float temperature;
    private float bloodPressure;
    private float oxygenLevel;
    private float pulse;
    private int respiratoryRate;
    private String note;

    private Long medicalFileId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vital_signs_reading);
        getSupportActionBar().setTitle(getResources().getString(R.string.vitalSignsReading));
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        temperatureEditText = findViewById(R.id.temperature);
        bloodPressureEditText = findViewById(R.id.bloodPresure);
        oxygenLevelEditText = findViewById(R.id.oxygenLevel);
        pulseEditText = findViewById(R.id.pulse);
        respiratoryRateEditText = findViewById(R.id.respiratoryRate);
        noteEditText = findViewById(R.id.note);
        medicalFileId = getIntent().getExtras().getLong("medicalFileId");
        realisationDateEditText = findViewById(R.id.realisationDate);
        realisationDateEditText.setFocusable(false);
        realisationDateEditText.setOnClickListener(v -> RealisationDateManager.createNewDatePicker(VitalSignsReadingActivity.this, realisationDateEditText));

        FloatingActionButton saveButton = findViewById(R.id.saveButton);
        saveButton.setOnClickListener(v -> {
            if (validate()) {
                getFieldsValues(temperatureEditText, bloodPressureEditText, oxygenLevelEditText, pulseEditText, respiratoryRateEditText, realisationDateEditText, noteEditText);
                createAct();
            }
        });

        if (getIntent().getExtras().containsKey("id")) {
            temperatureEditText.setEnabled(false);
            bloodPressureEditText.setEnabled(false);
            oxygenLevelEditText.setEnabled(false);
            pulseEditText.setEnabled(false);
            respiratoryRateEditText.setEnabled(false);
            noteEditText.setEnabled(false);
            realisationDateEditText.setEnabled(false);
            saveButton.hide();
            getAct(getIntent().getExtras().getLong("id"));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    private boolean validate() {
        boolean valid = true;
        String realisationDate = realisationDateEditText.getText().toString();
        realisationDateEditText.setError(null);
        if (realisationDate.isEmpty()) {
            realisationDateEditText.setError(getResources().getString(R.string.requiredField));
            valid = false;
        }
        return valid;
    }

    private void getFieldsValues(EditText temperatureEditText, EditText bloodPressureEditText, EditText oxygenLevelEditText, EditText pulseEditText, EditText respiratoryRateEditText, EditText realisationDateEditText, EditText noteEditText) {
        temperature = temperatureEditText.getText().toString().isEmpty() ? 0 : Float.valueOf(temperatureEditText.getText().toString());
        bloodPressure = bloodPressureEditText.getText().toString().isEmpty() ? 0 : Float.valueOf(bloodPressureEditText.getText().toString());
        oxygenLevel = oxygenLevelEditText.getText().toString().isEmpty() ? 0 : Float.valueOf(oxygenLevelEditText.getText().toString());
        pulse = pulseEditText.getText().toString().isEmpty() ? 0 : Float.valueOf(pulseEditText.getText().toString());
        respiratoryRate = respiratoryRateEditText.getText().toString().isEmpty() ? 0 : Integer.valueOf(respiratoryRateEditText.getText().toString());
        note = noteEditText.getText().toString().isEmpty() ? "" : noteEditText.getText().toString();
    }

    private void createAct() {
        Retrofit retrofit = HttpClientService.getRetrofit(this);
        IAct client = retrofit.create(IAct.class);
        Call<Void> call = client.createAct(note, RealisationDateManager.getRealisationDate(), temperature, bloodPressure, oxygenLevel, pulse, respiratoryRate, medicalFileId);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Snackbar.make(coordinatorLayout,
                        getResources().getString(R.string.ActCreationFailed),
                        Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void getAct(Long id) {
        Retrofit retrofit = HttpClientService.getRetrofit(this);
        IAct client = retrofit.create(IAct.class);
        Call<VitalSignsReading> call = client.getVitalSignsReading(id);

        call.enqueue(new Callback<VitalSignsReading>() {
            @Override
            public void onResponse(Call<VitalSignsReading> call, Response<VitalSignsReading> response) {
                if (response.isSuccessful()) {
                    vitalSignsReading = response.body();
                    populate();
                }
            }

            @Override
            public void onFailure(Call<VitalSignsReading> call, Throwable t) {
                finish();
            }
        });
    }

    private void populate() {
        if (vitalSignsReading != null) {
            temperatureEditText.setText(String.valueOf(vitalSignsReading.getTemperature()));
            noteEditText.setText(vitalSignsReading.getNote());
            realisationDateEditText.setText(vitalSignsReading.getRealisationDate().toString());
            pulseEditText.setText(String.valueOf(vitalSignsReading.getPulse()));
            respiratoryRateEditText.setText(String.valueOf(vitalSignsReading.getRespiratoryRate()));
            oxygenLevelEditText.setText(String.valueOf(vitalSignsReading.getOxygenLevel()));
            bloodPressureEditText.setText(String.valueOf(vitalSignsReading.getBloodPresure()));
        }
    }
}
