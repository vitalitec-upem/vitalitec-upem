package com.vitalitec.lightmedclient;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vitalitec.lightmedclient.models.MedicalFile;
import com.vitalitec.lightmedclient.service.HttpClientService;
import com.vitalitec.lightmedclient.service.IAct;
import com.vitalitec.lightmedclient.service.IMedicalFile;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DMPActivity extends AppCompatActivity {
    private Call<MedicalFile> medicalFileRequest;
    private Call<int[]> actsRequest;

    private MedicalFile medicalFile;
    private CoordinatorLayout coordinatorLayout;
    private FloatingActionButton createButton;
    LinearLayout actList;
    private TextView title;
    private TextView socSec;
    private int[] availableActs = {};
    private Callback<MedicalFile> medicalFileCallBack;
    private Callback<int[]> actsCallback;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_dmp);
        findViewById(R.id.card_layout).setTransitionName(getIntent().getExtras().getString("transitionCardName"));
        findViewById(R.id.dmp_image).setTransitionName(getIntent().getExtras().getString("transitionImageName"));
        getSupportActionBar().setTitle(
                getResources().getString(R.string.medicalFileActivityTitle, getIntent().getExtras().getLong("id"))
        );
        ImageView editPatient = findViewById(R.id.editPatient);
        createButton = findViewById(R.id.createButton);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        actList = findViewById(R.id.ActList);
        title = findViewById(R.id.dmp_title);
        socSec = findViewById(R.id.SocSecNumber);

        editPatient.setOnClickListener(v -> {
            if (medicalFile == null) {
                Toast.makeText(this, getResources().getString(R.string.unknown), Toast.LENGTH_SHORT);
            } else {
                Intent i = new Intent(this, PatientActivity.class)
                        .putExtra("id", medicalFile.getPatient().getId())
                        .putExtra("socialSec", medicalFile.getPatient().getSocialSecurityNumber())
                        .putExtra("firstName", medicalFile.getPatient().getFirstName())
                        .putExtra("lastName", medicalFile.getPatient().getLastName())
                        .putExtra("phone", medicalFile.getPatient().getPhoneNumber())
                        .putExtra("description", medicalFile.getPatient().getDescription());
                if (medicalFile.getPatient().getSex() != null) {
                    i.putExtra("sex", medicalFile.getPatient().getSex());
                }
                startActivity(i);
            }
        });

        createButton.setOnClickListener(v -> {
            String[] labels = ActsMap.getLabels(this, availableActs);
            new AlertDialog.Builder(this, R.style.Dialog)
                    .setTitle("Choose an act")
                    .setSingleChoiceItems(labels, 0, null)
                    .setPositiveButton("Ok", (dialog, which) -> {
                        ListView view = ((AlertDialog) dialog).getListView();
                        Intent i = new Intent(this, ActsMap.getActivity(availableActs[view.getCheckedItemPosition()]));
                        i.putExtra("medicalFileId", medicalFile.getId());
                        startActivity(i);
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        });

        Retrofit retrofit = HttpClientService.getRetrofit(this);
        IMedicalFile iMedicalFile = retrofit.create(IMedicalFile.class);
        medicalFileRequest = iMedicalFile.findById(getIntent().getExtras().getLong("id", -1L));
        medicalFileCallBack = onMedicalFileResponse();

        IAct iAct = retrofit.create(IAct.class);
        actsRequest = iAct.getAvailableActs();
        actsCallback = onActsResponse();
    }

    private void fetch() {
        if (ContentUtils.isNetworkAvailable(this)) {
            medicalFileRequest.clone().enqueue(medicalFileCallBack);
            actsRequest.clone().enqueue(actsCallback);
        } else {
            RecentlyOpenedMedicalFiles recentlyOpened = new RecentlyOpenedMedicalFiles(getApplicationContext());
            medicalFile = recentlyOpened.getMedicalFiles().stream().filter(x -> x.getId() == getIntent().getExtras().getLong("id")).findFirst().orElse(null);
            setMedicalFileInfo();
            createButton.hide();
        }
    }

    private Callback<MedicalFile> onMedicalFileResponse() {
        return new Callback<MedicalFile>() {
            @Override
            public void onResponse(Call<MedicalFile> call, Response<MedicalFile> response) {
                if (response.isSuccessful()) {
                    medicalFile = response.body();
                    setMedicalFileInfo();
                    new RecentlyOpenedMedicalFiles(getApplicationContext()).addMedicalFile(medicalFile);
                } else {
                    onLoadingFailed();
                }
            }

            @Override
            public void onFailure(Call<MedicalFile> call, Throwable t) {
                onLoadingFailed();
            }
        };
    }

    private Callback<int[]> onActsResponse() {
        return new Callback<int[]>() {
            @Override
            public void onResponse(Call<int[]> call, Response<int[]> response) {
                if (response.isSuccessful()) {
                    availableActs = response.body();
                }
            }

            @Override
            public void onFailure(Call<int[]> call, Throwable t) {

            }
        };
    }

    private void onLoadingFailed() {
        Snackbar.make(coordinatorLayout,
                getResources().getString(R.string.medicalFileLoadingFailed),
                Snackbar.LENGTH_LONG).show();
        new Handler().postDelayed(this::finish, 2000);
    }

    private void setMedicalFileInfo() {
        actList.removeAllViews();

        title.setText(medicalFile.getPatient().getFirstName() + " " + medicalFile.getPatient().getLastName());
        socSec.setText(medicalFile.getPatient().getSocialSecurityNumber());

        if (!medicalFile.getActs().isEmpty()) {
            ActCardViewGenerator cardViewGenerator = new ActCardViewGenerator(this);
            medicalFile.getActs().forEach(act -> {
                View card = cardViewGenerator.generate(act);
                actList.addView(card);
            });
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetch();
    }

}