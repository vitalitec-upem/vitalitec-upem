package com.vitalitec.lightmedclient.service;

import android.content.Context;
import android.preference.PreferenceManager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vitalitec.lightmedclient.models.AppUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class HttpClientService {

    static final String REST_API_ADDRESS = "http://localhost:8080/";

    static class CallbackFuture extends CompletableFuture<Response> implements Callback {
        @Override
        public void onFailure(Call call, IOException e) {
            super.completeExceptionally(e);
        }

        @Override
        public void onResponse(Call call, Response response) {
            super.complete(response);
        }

    }

    public String get(Context context, String url) {
        try {
            return new GetTask(context).execute(url).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getWithOneArg(Context context, String url, String arg) {
        try {
            return new GetWithOneArgTask(context).execute(url, arg).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String postObject(Context context, String route, String json) {
        System.out.println("JSON TO SEND: " + json);
        String ret = null;
        try {
            ret = new PostObjectTask(context).execute(route, json).get();
            System.out.println("Ret: " + ret);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public AppUser loginRequest(Context context, String username, String password) {

        try {
            String loggedIn = new LoginTask(context).execute(username, password).get();
            System.out.println("loggedin : " + loggedIn);
            JSONObject jsonObject = new JSONObject(loggedIn);
            ObjectMapper objectMapper = new ObjectMapper();
            if (jsonObject.has("user")) {
                JSONObject userObject = jsonObject.getJSONObject("user");
                AppUser user = objectMapper.readValue(userObject.toString(), AppUser.class);
                return user;
            } else {
                return null;
            }
        } catch (InterruptedException | ExecutionException | JSONException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Retrofit getRetrofit(Context context) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.addInterceptor(
                chain -> {
                    Request original = chain.request();

                    Request request = original.newBuilder()
                            .header("Cookie", PreferenceManager
                                    .getDefaultSharedPreferences(context)
                                    .getString("auth", "none"))
                            .method(original.method(), original.body())
                            .build();

                    return chain.proceed(request);
                }
        );

        return new Retrofit.Builder()
                .baseUrl(REST_API_ADDRESS)
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build())
                .build();
    }
}
