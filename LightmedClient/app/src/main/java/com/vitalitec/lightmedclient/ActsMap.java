package com.vitalitec.lightmedclient;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;

import java.util.HashMap;

public class ActsMap {

    private static HashMap<Integer, Pair<Class<? extends AppCompatActivity>, Integer>> map = new HashMap<>();

    static {
        map.put(Integer.valueOf(0), Pair.create(VitalSignsReadingActivity.class, R.string.vitalSignsReading));
        map.put(Integer.valueOf(1), Pair.create(RadiographyActivity.class, R.string.radiography));
    }

    public static Class<? extends  AppCompatActivity> getActivity(Integer type) {
        return map.get(type).first;
    }

    public static String[] getLabels(Activity context, int[] array) {
        String[] labels = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            labels[i] = context.getResources().getString(map.get(i).second);
        }
        return labels;
    }

    public static String getLabel(Activity context, int actType) {
        return context.getResources().getString(map.get(actType).second);
    }
}
