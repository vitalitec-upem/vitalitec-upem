package com.vitalitec.lightmedclient.service;

import com.vitalitec.lightmedclient.models.VitalSignsReading;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface IAct {

    @GET("act/getAvailableActs")
    Call<int[]> getAvailableActs();

    @Multipart
    @POST("act/createVitalSignsReading")
    Call<Void> createAct(@Part("note") String note,
                         @Part("realisationDate") String realisationDate,
                         @Part("temperature") float temperature,
                         @Part("bloodPressure") float bloodPressure,
                         @Part("oxygenLevel") float oxygenLevel,
                         @Part("pulse") float pulse,
                         @Part("respiratoryRate") int respiratoryRate,
                         @Part("medicalFileId") long medicalFileId);

    @GET("act/getVitalSignsReading/{id}")
    Call<VitalSignsReading> getVitalSignsReading(@Path("id") Long id);

    @Multipart
    @POST("act/createRadiography")
    Call<Long> createAct(@Part("note") String note,
                         @Part("realisationDate") String realisationDate,
                         @Part("medicalFileId") long medicalFileId);

    @Multipart
    @POST("act/createDocument")
    Call<Void> createDocument(@Part("path") String path,
                              @Part("type") String type,
                              @Part("actId") Long actId);

    
}
