package com.vitalitec.lightmedclient.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.vitalitec.lightmedclient.DMPSearchResultsActivity;
import com.vitalitec.lightmedclient.R;
import com.vitalitec.lightmedclient.models.Patient;


public class DMPFragment extends Fragment implements PlaceSelectionListener {

    private Boolean sex = null;

    public DMPFragment() {
    }

    public static DMPFragment newInstance() {
        return new DMPFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        FloatingActionButton search = getView().findViewById(R.id.searchButton);
        TextInputEditText idDmp = getView().findViewById(R.id.idDMP);
        TextInputEditText secuSociale = getView().findViewById(R.id.numSecuSociale);
        TextInputEditText firstName = getView().findViewById(R.id.Firstname);
        TextInputEditText lastName = getView().findViewById(R.id.Lastname);
        TextInputEditText phone = getView().findViewById(R.id.PhoneNumber);
        EditText address = getView().findViewById(R.id.place_autocomplete_search_input);
        RadioGroup radioGroup = getView().findViewById(R.id.radioGroup);

        address.setHint("Adresse");
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.radioUnknown:
                    sex = null;
                    break;
                case R.id.radioMale:
                    sex = Patient.MALE;
                    break;
                case R.id.radioFemale:
                    sex = Patient.FEMALE;
                    break;
            }
        });
        search.setOnClickListener((View v) -> {
            Intent i = new Intent(getActivity(), DMPSearchResultsActivity.class)
                    .putExtra("id", idDmp.getText().toString())
                    .putExtra("secuSociale", secuSociale.getText().toString())
                    .putExtra("lastName", lastName.getText().toString())
                    .putExtra("firstName", firstName.getText().toString())
                    .putExtra("phone", phone.getText().toString())
                    .putExtra("address", address.getText().toString());
            if (sex != null) {
                i.putExtra("sex", sex);
            }
            startActivity(i);
        });
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dmp, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onPlaceSelected(Place place) {
    }

    @Override
    public void onError(Status status) {
        Toast.makeText(getActivity(), getResources().getString(R.string.errPlaceAutocomplete),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroyView() {

        FragmentManager fm = getFragmentManager();

        Fragment xmlFragment = fm.findFragmentById(R.id.autocomplete_fragment);
        if (xmlFragment != null) {
            fm.beginTransaction().remove(xmlFragment).commit();
        }

        super.onDestroyView();
    }


}
