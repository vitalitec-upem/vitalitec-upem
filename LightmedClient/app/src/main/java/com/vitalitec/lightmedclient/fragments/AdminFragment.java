package com.vitalitec.lightmedclient.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.vitalitec.lightmedclient.CreateRoleActivity;
import com.vitalitec.lightmedclient.CreateSkillActivity;
import com.vitalitec.lightmedclient.CreateUnitActivity;
import com.vitalitec.lightmedclient.CreateUserActivity;
import com.vitalitec.lightmedclient.ManageTreeActivity;
import com.vitalitec.lightmedclient.R;

public class AdminFragment extends Fragment {

    public AdminFragment() {
    }

    public static AdminFragment newInstance() {
        return new AdminFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_admin, container, false);
        Button createUser = v.findViewById(R.id.addUserButton);
        createUser.setOnClickListener((View view) -> {
            Intent createUserIntent = new Intent(getActivity(), CreateUserActivity.class);
            startActivity(createUserIntent);
        });
        Button createUnit = v.findViewById(R.id.addUnitButton);
        createUnit.setOnClickListener((View view) -> {
            Intent createUnitIntent = new Intent(getActivity(), ManageTreeActivity.class);
            startActivity(createUnitIntent);
        });
        Button createSkill = v.findViewById(R.id.addSkillButton);
        createSkill.setOnClickListener((View view) -> {
            Intent createSkillIntent = new Intent(getActivity(), CreateSkillActivity.class);
            startActivity(createSkillIntent);
        });
        Button createRole = v.findViewById(R.id.addRoleButton);
        createRole.setOnClickListener((View view) -> {
            Intent createRoleIntent = new Intent(getActivity(), CreateRoleActivity.class);
            startActivity(createRoleIntent);
        });
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
