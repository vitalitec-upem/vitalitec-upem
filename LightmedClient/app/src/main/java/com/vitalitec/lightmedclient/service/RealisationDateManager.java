package com.vitalitec.lightmedclient.service;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.EditText;

import com.vitalitec.lightmedclient.R;
import com.vitalitec.lightmedclient.VitalSignsReadingActivity;

import java.util.Calendar;

public class RealisationDateManager {

    private static String realisationDate;

    private static void setRealisationDate(String date){
        realisationDate = date;
    }

    public static String getRealisationDate(){
        return realisationDate;
    }

    public static void createNewDatePicker(Context context, EditText realisationDateEditText){
        android.app.DatePickerDialog.OnDateSetListener dateSetListener;
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(android.widget.DatePicker view, int year, int month, int dayOfMonth) {
                String date;
                month = month + 1;

                String dayOfMonthText = dayOfMonth < 10 ? "0" + dayOfMonth : String.valueOf(dayOfMonth);
                String monthText = month < 10 ? "0" + month : String.valueOf(month);

                date = dayOfMonthText + "/" + monthText + "/" + year;
                setRealisationDate(date);
                realisationDateEditText.setText(date);
            }
        };

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        new android.app.DatePickerDialog(
                context,
                R.style.Dialog,
                dateSetListener,
                year, month, day)
                .show();
    }
}
