package com.vitalitec.lightmedclient;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vitalitec.lightmedclient.models.Address;
import com.vitalitec.lightmedclient.models.AppUser;
import com.vitalitec.lightmedclient.models.Role;
import com.vitalitec.lightmedclient.models.Skill;
import com.vitalitec.lightmedclient.models.Unit;
import com.vitalitec.lightmedclient.service.HttpClientService;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class CreateUserActivity extends AppCompatActivity implements PlaceSelectionListener {

    HttpClientService service = new HttpClientService();
    ObjectMapper objectMapper = new ObjectMapper();
    Place place = null;
    Spinner spinnerListRoles;
    Spinner spinnerListHospital;
    Spinner spinnerListPole;
    Spinner spinnerListService;
    List<Spinner> listSpinnerListSkill;
    Spinner spinnerListSkill;
    String serviceSelected;
    TextInputLayout firstNameEditText;
    TextInputLayout lastNameEditText;
    TextInputLayout birthdateEditText;
    TextInputLayout phoneNumberEditText;
    TextInputLayout emailEditText;
    CoordinatorLayout coordinatorLayout;

    private <T> void setSpinner(Spinner spinnerListAdminModel, String url, Class<T> classType) {
        if(ContentUtils.isNetworkAvailable(this)){
            ArrayList<String> listAdminModel = new ArrayList<>();
            String response = service.get(this, url);
            System.out.println("URL: " + url + ", RESPONSE: " + response);
            try {
                List<T> myObjects = objectMapper.readValue(response, new TypeReference<List<T>>(){});
                System.out.println(myObjects);
                for (T a : myObjects) {
                    LinkedHashMap<String, String> test = (LinkedHashMap)a;
                    listAdminModel.add(test.get("name"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            String[] arrRoles = new String[listAdminModel.size()];
            arrRoles = listAdminModel.toArray(arrRoles);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrRoles);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerListAdminModel.setAdapter(adapter);
        } else {
            Snackbar.make(coordinatorLayout,
                    getResources().getString(R.string.connectionFailed),
                    Snackbar.LENGTH_LONG).show();
        }

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);
        listSpinnerListSkill = new ArrayList<Spinner>();
        spinnerListRoles = findViewById(R.id.createUserListRoles);
        spinnerListHospital = findViewById(R.id.createUserListHospital);
        spinnerListPole = findViewById(R.id.createUserListPole);
        spinnerListService = findViewById(R.id.createUserListService);
        spinnerListSkill = findViewById(R.id.createUserListSkill);
        firstNameEditText = findViewById(R.id.createUserFirstName);
        lastNameEditText = findViewById(R.id.createUserLastName);
        birthdateEditText = findViewById(R.id.createUserBirthdate);
        phoneNumberEditText = findViewById(R.id.createUserPhoneNumber);
        emailEditText = findViewById(R.id.createUserEmail);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        listSpinnerListSkill.add(spinnerListSkill);
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteFragment.setOnPlaceSelectedListener(this);
        EditText et = (EditText)findViewById(R.id.place_autocomplete_search_input);
        et.setHint("Adresse");
        setSpinner(findViewById(R.id.createUserListRoles), "admin/getAllRoles", Role.class);
        setSpinner(findViewById(R.id.createUserListHospital), "admin/getAllHospitals", Unit.class);

        spinnerListHospital.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String hospitalSelected = (String) spinnerListHospital.getItemAtPosition(position);
                setSpinner(spinnerListPole, "unit/getPolesByHospital/"+hospitalSelected, Unit.class);
                setSpinner(spinnerListService, "unit/getServicesByPole/"+spinnerListPole.getSelectedItem()+"/"+spinnerListHospital.getSelectedItem(), Unit.class);
                setSpinner(spinnerListSkill, "admin/getSkillsByService/"+spinnerListService.getSelectedItem(), Skill.class);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spinnerListPole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String poleSelected = (String) spinnerListPole.getItemAtPosition(position);
                setSpinner(spinnerListService, "unit/getServicesByPole/"+poleSelected+"/"+spinnerListHospital.getSelectedItem(), Unit.class);
                setSpinner(spinnerListSkill, "admin/getSkillsByService/"+spinnerListService.getSelectedItem(), Skill.class);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spinnerListService.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                serviceSelected = (String) spinnerListService.getItemAtPosition(position);
                listSpinnerListSkill.stream().forEach(spinner -> {
                    setSpinner(spinner, "admin/getSkillsByService/" + serviceSelected, Skill.class);
                });

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        //setSpinner(findViewById(R.id.createUserListSkill), "admin/getSkillsByService/" + serviceSelected, Skill.class);
        FloatingActionButton addSkillBtn = findViewById(R.id.addSkillBtn);
        Button saveBtn = findViewById(R.id.createUserSaveBtn);

        saveBtn.setOnClickListener(v -> saveNewUser());

        addSkillBtn.setOnClickListener(v -> addSpinner());
    }

    private void addSpinner() {
        LinearLayout linearLayout = findViewById(R.id.linearLayoutSpinner);
        Spinner spinner = new Spinner(this);
        listSpinnerListSkill.add(spinner);
        linearLayout.addView(spinner);
        setSpinner(spinner, "admin/getSkillsByService/" + serviceSelected, Skill.class);
    }

    private void saveNewUser() {
        if (!isMailClientPresent(this)) {
            Toast.makeText(this, getResources().getString(R.string.errNoMail),
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if (!validate()) {
            Toast.makeText(this, getResources().getString(R.string.errCreateUserForm),
                    Toast.LENGTH_SHORT).show();
            return;
        }
        Address address;
        Role role = null;
        Unit unit = null;

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        Date birthdate = null;
        if(ContentUtils.isNetworkAvailable(this)){
            try {
                role = objectMapper.readValue(service.getWithOneArg(this, "role/getRoleByName", spinnerListRoles.getSelectedItem().toString()), Role.class);
                unit = objectMapper.readValue(service.getWithOneArg(this, "unit/getUnitByName", spinnerListService.getSelectedItem().toString()), Unit.class);
                birthdate = sdf.parse(birthdateEditText.getEditText().getText().toString());
            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }
            Set<Skill> skills = new HashSet<Skill>();
            listSpinnerListSkill.stream().forEach(spinner -> {
                Skill skill = null;
                try {
                    skill = objectMapper.readValue(service.get(this, "skill/getSkillByName/" + spinner.getSelectedItem().toString()) , Skill.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                skills.add(skill);
            });
            if (role == null || unit == null) {
                Toast.makeText(this, getResources().getString(R.string.errCreateUserForm),
                        Toast.LENGTH_SHORT).show();
                return ;
            }
            if (place != null)
                address = new Address(place.getAddress().toString());
            else
                address = new Address("N/A");

            AppUser user = new AppUser(firstNameEditText.getEditText().getText().toString(),
                    lastNameEditText.getEditText().getText().toString(),
                    birthdate, phoneNumberEditText.getEditText().getText().toString(),
                    null, emailEditText.getEditText().getText().toString(),
                    null, unit, address, role, skills);
            Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
            System.out.println("USER:");
            System.out.println(gson.toJson(user));
            String pwd = service.postObject(this, "admin/createUser", gson.toJson(user));
            System.out.println("pwd: " + pwd);
            Toast.makeText(this, getResources().getString(R.string.successfullyAdded),
                    Toast.LENGTH_SHORT).show();
            // sendEmail(pwd, emailEditText.getEditText().getText().toString());
        } else {
            Snackbar.make(coordinatorLayout,
                    getResources().getString(R.string.connectionFailed),
                    Snackbar.LENGTH_LONG).show();
        }

    }

    private void sendEmail(String body, String email) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{email});
        i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.mailSubjectPwd));
        i.putExtra(Intent.EXTRA_TEXT   , getResources().getString(R.string.mailBodyPwd) + " " + body);
        try {
            startActivity(Intent.createChooser(i, getResources().getString(R.string.sendMail)));
        } catch (android.content.ActivityNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public static boolean isMailClientPresent(Context context){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/html");
        final PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, 0);
        if (list.size() == 0) {
            Toast.makeText(context, context.getResources().getString(R.string.errNoMail),
                    Toast.LENGTH_SHORT).show();
        }
        return list.size() != 0;
    }

    public boolean validate() {
        boolean valid = true;
        String firstName = firstNameEditText.getEditText().getText().toString();
        String lastName = lastNameEditText.getEditText().getText().toString();
        String birthDate = birthdateEditText.getEditText().getText().toString();
        String phoneNumber = phoneNumberEditText.getEditText().getText().toString();
        String email = emailEditText.getEditText().getText().toString();

        if(spinnerListService.getSelectedItem() == null){
            valid = false;
        }

        if(spinnerListRoles.getSelectedItem()== null){
            valid = false;
        }

        if(spinnerListHospital.getSelectedItem() == null){
            valid = false;
        }

        if(spinnerListSkill.getSelectedItem() == null){
            valid = false;
        }

        if(spinnerListPole.getSelectedItem() == null){
            valid = false;
        }

        firstNameEditText.setError(null);
        lastNameEditText.setError(null);
        birthdateEditText.setError(null);
        phoneNumberEditText.setError(null);
        emailEditText.setError(null);


        if (firstName.isEmpty()) {
            firstNameEditText.setError(getResources().getString(R.string.requiredField));
            valid = false;
        }
        if (lastName.isEmpty()) {
            lastNameEditText.setError(getResources().getString(R.string.requiredField));
            valid = false;
        }
        if (birthDate.isEmpty()) {
            birthdateEditText.setError(getResources().getString(R.string.requiredField));
            valid = false;
        }
        if (phoneNumber.isEmpty()) {
            phoneNumberEditText.setError(getResources().getString(R.string.requiredField));
            valid = false;
        }
        if (email.isEmpty()) {
            emailEditText.setError(getResources().getString(R.string.requiredField));
            valid = false;
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailEditText.setError(getResources().getString(R.string.invalidAddress));
            valid = false;
        }
        if (!Patterns.PHONE.matcher(phoneNumber).matches()) {
            phoneNumberEditText.setError(getResources().getString(R.string.invalidNumber));
            valid = false;
        }
        if (!birthDate.matches("\\d{2}/\\d{2}/\\d{4}")) {
            birthdateEditText.setError(getResources().getString(R.string.invalidDate));
            System.out.println("invalid date");
            valid = false;
        }
        return valid;
    }

    @Override
    public void onPlaceSelected(Place place) {
        this.place = place;
        System.out.println("place: " + place.getAddress());
    }

    @Override
    public void onError(Status status) {
        Toast.makeText(this, getResources().getString(R.string.errPlaceAutocomplete),
                Toast.LENGTH_SHORT).show();
    }
}
