package com.vitalitec.lightmedclient.service;

public class UploadResponse {
    private Boolean success;
    private String cause;

    public Boolean getSuccess(){
        return success;
    }
    public String getCause(){
        return cause;
    }
}
