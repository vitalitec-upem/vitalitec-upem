package com.vitalitec.lightmedclient;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vitalitec.lightmedclient.models.AppUser;
import com.vitalitec.lightmedclient.models.Role;
import com.vitalitec.lightmedclient.service.HttpClientService;

import java.util.ArrayList;

public class CreateRoleActivity extends AppCompatActivity {

    HttpClientService service = new HttpClientService();
    TextInputEditText etRole;
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_role);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        etRole = findViewById(R.id.RoleNameText);

        Button saveBtn = findViewById(R.id.createRoleSendBtn);
        saveBtn.setOnClickListener(v -> saveNewRole(etRole.getText().toString()));
    }

    private void saveNewRole(String strRole) {
        if (!validate()) {
            Toast.makeText(this, getResources().getString(R.string.errCreateUserForm),
                    Toast.LENGTH_SHORT).show();
            return;
        }
        Gson gson = new Gson();
        Role role = new Role(strRole, new ArrayList<AppUser>());
        if (ContentUtils.isNetworkAvailable(this)) {
            String pwd = service.postObject(this, "admin/createRole", gson.toJson(role));
            Toast.makeText(this, getResources().getString(R.string.successfullyAdded),
                    Toast.LENGTH_SHORT).show();
        } else {
            Snackbar.make(coordinatorLayout,
                    getResources().getString(R.string.connectionFailed),
                    Snackbar.LENGTH_LONG).show();
        }
    }


    public boolean validate() {
        boolean valid = true;
        String roleName = etRole.getText().toString();

        etRole.setError(null);

        if (roleName.isEmpty()) {
            etRole.setError(getResources().getString(R.string.requiredField));
            valid = false;
        }

        return valid;
    }
}
