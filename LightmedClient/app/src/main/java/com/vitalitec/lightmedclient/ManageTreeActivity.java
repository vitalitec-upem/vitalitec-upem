package com.vitalitec.lightmedclient;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.vitalitec.lightmedclient.models.Unit;

import java.io.IOException;

public class ManageTreeActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_tree);

        Button createHospital = findViewById(R.id.createHospital);
        Button createPole = findViewById(R.id.createPole);
        Button createService = findViewById(R.id.createService);

        createHospital.setOnClickListener((View v) -> {
            Intent createUnitIntent = new Intent(this, CreateUnitActivity.class);
            createUnitIntent.putExtra("type","hospital");
            startActivity(createUnitIntent);
        });

        createPole.setOnClickListener((View v) -> {
            Intent createUnitIntent = new Intent(this, CreateUnitActivity.class);
            createUnitIntent.putExtra("type","pole");
            startActivity(createUnitIntent);
        });

        createService.setOnClickListener((View v) -> {
            Intent createUnitIntent = new Intent(this, CreateUnitActivity.class);
            createUnitIntent.putExtra("type","service");
            startActivity(createUnitIntent);
        });
    }

}
