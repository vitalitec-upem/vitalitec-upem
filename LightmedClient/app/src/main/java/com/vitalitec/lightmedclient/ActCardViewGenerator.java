package com.vitalitec.lightmedclient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;

import com.vitalitec.lightmedclient.models.Act;

import java.util.Objects;

public class ActCardViewGenerator {

    private final Context context;
    private final LayoutInflater inflater;

    public ActCardViewGenerator(Context context) {
        this.context = Objects.requireNonNull(context);
        this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public View generate(Act act) {
        Long id = act.getId();
        int type = act.getActType();
        View card = inflater.inflate(R.layout.card_act_dmp, null);
        ((AppCompatTextView) card.findViewById(R.id.CardTitle))
                .setText(ActsMap.getLabel((Activity) context, type));
        card.setOnClickListener(generateCardOnClickListener(card, id, type));
        return card;
    }

    private View.OnClickListener generateCardOnClickListener(View card, Long id, int type) {
        return v -> {
            Intent i = new Intent(this.context, ActsMap.getActivity(type))
                    .putExtra("transitionCardName", "transitionCard" + id)
                    .putExtra("transitionTextName", "transitionText" + id)
                    .putExtra("id", id);

            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(
                            (Activity) this.context,
                            Pair.create(card.findViewById(R.id.card_layout), "transitionCard" + id),
                            Pair.create(card.findViewById(R.id.CardTitle), "transitionText" + id)
                    );
            this.context.startActivity(i, options.toBundle());
        };
    }
}
