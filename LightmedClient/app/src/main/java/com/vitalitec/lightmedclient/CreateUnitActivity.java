package com.vitalitec.lightmedclient;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.vitalitec.lightmedclient.models.Unit;
import com.vitalitec.lightmedclient.service.HttpClientService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class CreateUnitActivity extends AppCompatActivity {

    HttpClientService service = new HttpClientService();
    ObjectMapper objectMapper = new ObjectMapper();

    TextInputEditText etName;
    TextInputEditText etDescription;
    CoordinatorLayout coordinatorLayout;
    Spinner spinnerCreateParentList;
    Spinner spinnerCreateHospitalList;
    Unit unitParent;

    private <T> void setSpinner(int id, String url, Class<T> classType) {
        Spinner spinnerListAdminModel = findViewById(id);
        ArrayList<String> listAdminModel = new ArrayList<>();
        if(ContentUtils.isNetworkAvailable(this)){
            String response = service.get(this, url);
            System.out.println("URL: " + url + ", RESPONSE: " + response);
            try {
                System.out.println("RESPONSE: " + response);
                List<T> myObjects = objectMapper.readValue(response, new TypeReference<List<T>>(){});
                System.out.println(myObjects);
                for (T a : myObjects) {
                    LinkedHashMap<String, String> test = (LinkedHashMap)a;
                    listAdminModel.add(test.get("name"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            String[] arrRoles = new String[listAdminModel.size()];
            arrRoles = listAdminModel.toArray(arrRoles);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrRoles);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerListAdminModel.setAdapter(adapter);
        } else {
            Snackbar.make(coordinatorLayout,
                    getResources().getString(R.string.connectionFailed),
                    Snackbar.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_unit);
        Intent i = getIntent();
        String typeOfCreated = i.getStringExtra("type");
        TextView title = findViewById(R.id.toolbarTitle);
        switch (typeOfCreated) {
            case "hospital":
                title.setText(R.string.createHospital);
                break;
            case "pole":
                title.setText(R.string.createPole);
                break;
            case "service":
                title.setText(R.string.createService);
                break;
        }
        etName = findViewById(R.id.NameText);
        etDescription = findViewById(R.id.DescriptionText);
        spinnerCreateParentList = findViewById(R.id.createParentList);
        spinnerCreateHospitalList = findViewById(R.id.createHospitalList);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        System.out.println(typeOfCreated);
        switch (typeOfCreated) {
            case "pole":
                spinnerCreateHospitalList.setVisibility(View.GONE);
                setSpinner(R.id.createParentList, "admin/getAllHospitals", Unit.class);
                break;
            case "service":
                setSpinner(R.id.createHospitalList, "admin/getAllHospitals", Unit.class);
                break;
            case "hospital":
                // spinnerCreateParentList.setVisibility(View.GONE);
                spinnerCreateHospitalList.setVisibility(View.GONE);
                //setSpinner(R.id.createParentList, "admin/getAllHospitals", Unit.class);
                //PAS DE SPINNER il faut le rattacher à l'APHP
                break;
        }

        try {
            if (spinnerCreateParentList.getSelectedItem() != null)
                if(ContentUtils.isNetworkAvailable(this)) {
                    unitParent = objectMapper.readValue(service.getWithOneArg(this, "unit/getUnitByName", spinnerCreateParentList.getSelectedItem().toString()), Unit.class);
                } else {
                    Snackbar.make(coordinatorLayout,
                            getResources().getString(R.string.connectionFailed),
                            Snackbar.LENGTH_LONG).show();
                }
        } catch (IOException e) {
            e.printStackTrace();
        }

        spinnerCreateHospitalList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String hospitalSelected = (String) spinnerCreateHospitalList.getItemAtPosition(position);
                setSpinner(R.id.createParentList, "unit/getPolesByHospital/" + hospitalSelected, Unit.class);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        Button saveBtn = findViewById(R.id.createUnitSaveBtn);
        saveBtn.setOnClickListener(v -> {
            try {
                if (spinnerCreateParentList.getSelectedItem() != null && !typeOfCreated.equals("hospital")) {
                    unitParent = objectMapper.readValue(service.getWithOneArg(getApplicationContext(), "unit/getUnitByName", spinnerCreateParentList.getSelectedItem().toString()), Unit.class);
                } else if (typeOfCreated.equals("hospital")) {
                    System.out.println("Je vais chercher l'APHP");
                    unitParent = objectMapper.readValue(service.getWithOneArg(getApplicationContext(), "unit/getUnitByName", "APHP"), Unit.class);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            saveNewUnit(etName.getText().toString(), etDescription.getText().toString(), typeOfCreated, unitParent);
        });
    }

    private void saveNewUnit(String strName, String strDescription, String strType, Unit unitParent) {
        if (!validate() ||
                unitParent == null) {
            Toast.makeText(this, getResources().getString(R.string.errCreateUserForm),
                    Toast.LENGTH_SHORT).show();
            return;
        }
        Gson gson = new Gson();
        System.out.println("Parent: " + unitParent.getName());
        Unit unit = new Unit(strName, strDescription, strType, unitParent, new ArrayList<>(), new ArrayList<>());
        if(ContentUtils.isNetworkAvailable(this)){
            String pwd = service.postObject(this, "admin/createUnit", gson.toJson(unit));
            Toast.makeText(this, getResources().getString(R.string.successfullyAdded),
                    Toast.LENGTH_SHORT).show();
        } else {
            Snackbar.make(coordinatorLayout,
                    getResources().getString(R.string.connectionFailed),
                    Snackbar.LENGTH_LONG).show();
        }
    }

    public boolean validate() {
        boolean valid = true;
        String name = etName.getText().toString();
        String description = etDescription.getText().toString();

        etName.setError(null);
        etDescription.setError(null);

        if (name.isEmpty()) {
            etName.setError(getResources().getString(R.string.requiredField));
            valid = false;
        }
        if (description.isEmpty()) {
            etDescription.setError(getResources().getString(R.string.requiredField));
            valid = false;
        }

        return valid;
    }
}
