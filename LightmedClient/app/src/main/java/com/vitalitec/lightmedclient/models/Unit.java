package com.vitalitec.lightmedclient.models;

import java.io.Serializable;
import java.util.List;

public class Unit implements Serializable {

    private static final long serialVersionUID = 1L;
    private long id;

    private String name;
    private String description;
    private String type;

    private Unit parent;

    private List<AppUser> users;

    private List<Patient> patients;

    public Unit() {}

    public Unit(String name, String description, String type, List<AppUser> users, List<Patient> patients) {
        this.name = name;
        this.description = description;
        this.type = type;
        this.users = users;
        this.patients = patients;
    }

    public Unit(String name, String description, String type, Unit parent, List<AppUser> users, List<Patient> patients) {
        this.name = name;
        this.description = description;
        this.type = type;
        this.users = users;
        this.patients = patients;
        this.parent = parent;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Unit getParent() {
        return parent;
    }

    public void setParent(Unit parent) {
        this.parent = parent;
    }

    public List<AppUser> getUsers() {
        return users;
    }

    public void setUsers(List<AppUser> users) {
        this.users = users;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }
}
