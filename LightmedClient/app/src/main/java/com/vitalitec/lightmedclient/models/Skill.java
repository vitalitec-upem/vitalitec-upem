package com.vitalitec.lightmedclient.models;

import java.io.Serializable;
import java.util.List;
import java.util.Set;


public class Skill implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	
	private String name;
	
    private Set<AppUser> users;

	private Unit parent;

	public Skill() {}

	public Skill(String name, Unit parent, Set<AppUser> users) {
		this.parent = parent;
		this.name = name;
		this.users = users;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<AppUser> getUsers() {
		return users;
	}

	public void setUsers(Set<AppUser> users) {
		this.users = users;
	}

	public Unit getParent() {
		return parent;
	}

	public void setParent(Unit parent) {
		this.parent = parent;
	}
}
