package com.vitalitec.lightmedclient;

import android.content.Context;

import com.vitalitec.lightmedclient.models.MedicalFile;
import com.vitalitec.lightmedclient.service.CacheService;

import java.util.LinkedList;

public class RecentlyOpenedMedicalFiles {
    private static final int MAX_SIZE = 20;
    private final Context context;

    public RecentlyOpenedMedicalFiles(Context context) {
        this.context = context;
    }

    public LinkedList<MedicalFile> getMedicalFiles() {
        LinkedList<MedicalFile> medicalFiles = (LinkedList<MedicalFile>) CacheService.getObject(context, "recentlyOpened");
        if (medicalFiles == null) {
            return new LinkedList<>();
        } else {
            return medicalFiles;
        }
    }

    public void addMedicalFile(MedicalFile medicalFile) {
        LinkedList<MedicalFile> medicalFiles = getMedicalFiles();
        System.out.println(medicalFiles);
        System.out.println(medicalFile);
        boolean inList = medicalFiles.remove(medicalFile);
        if (!inList && medicalFiles.size() == MAX_SIZE) {
            medicalFiles.removeFirst();
        }
        medicalFiles.add(medicalFile);
        CacheService.saveObject(context, medicalFiles, "recentlyOpened");
    }

}
