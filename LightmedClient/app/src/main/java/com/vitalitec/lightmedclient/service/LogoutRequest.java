package com.vitalitec.lightmedclient.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.vitalitec.lightmedclient.service.HttpClientService.REST_API_ADDRESS;

public class LogoutRequest {

    public static Call<ResponseBody> createLogoutRequest() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(REST_API_ADDRESS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        LogoutClient client = retrofit.create(LogoutClient.class);

        return client.logout();
    }
}
