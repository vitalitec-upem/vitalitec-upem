package com.vitalitec.lightmedclient.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class Act implements Serializable {

    protected long id;
    private static final long serialVersionUID = 1L;
    protected int actType;
    protected String note;
    protected Date createdOn;
    protected Date realisationDate;
    protected boolean published;
    protected boolean result;

    private MedicalFile medicalFile;

    private AppUser createdBy;

    private AppUser assignated;

    private List<Document> documents;

    public Act() {
    }

    public Act(int actType, String note, Date createdOn, Date realisationDate, boolean published, boolean result, MedicalFile medicalFile, AppUser createdBy, AppUser assignated, List<Document> documents) {
        super();
        this.actType = actType;
        this.note = note;
        this.createdOn = createdOn;
        this.realisationDate = realisationDate;
        this.published = published;
        this.result = result;
        this.medicalFile = medicalFile;
        this.createdBy = createdBy;
        this.assignated = assignated;
        this.documents = documents;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getRealisationDate() {
        return realisationDate;
    }

    public void setRealisationDate(Date realisationDate) {
        this.realisationDate = realisationDate;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public MedicalFile getMedicalFile() {
        return medicalFile;
    }

    public void setMedicalFile(MedicalFile medicalFile) {
        this.medicalFile = medicalFile;
    }

    public AppUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(AppUser createdBy) {
        this.createdBy = createdBy;
    }

    public AppUser getAssignated() {
        return assignated;
    }

    public void setAssignated(AppUser assignated) {
        this.assignated = assignated;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public int getActType() {
        return actType;
    }

    public void setActType(int actType) {
        this.actType = actType;
    }
}
