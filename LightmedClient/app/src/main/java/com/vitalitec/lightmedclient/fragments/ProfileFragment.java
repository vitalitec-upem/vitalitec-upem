package com.vitalitec.lightmedclient.fragments;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.vitalitec.lightmedclient.R;
import com.vitalitec.lightmedclient.models.AppUser;
import com.vitalitec.lightmedclient.service.HttpClientService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

public class ProfileFragment extends Fragment {

    public static final int GEOLOCATION_REQUEST_CODE = 1;
    public static final int LOCATION_TIMEOUT = 5000;
    private Handler handler;
    private TextView positionText;
    private TextView fullName;
    private TextView mail;
    Geocoder geocoder;
    private TextView role;
    private TextView unit;
    private TextView phoneNumber;
    private static final String UNDEFINED = "N/A";
    private static final Logger LOG = Logger.getLogger(ProfileFragment.class.getName());

    public ProfileFragment() {
    }

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.handler = new Handler();
        geocoder = new Geocoder(getContext(), Locale.getDefault());
        super.onCreate(savedInstanceState);

    }

    public void treatMessage(Location loc) {
        List<Address> addresses;

        if (loc != null) {
            try {
                addresses = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                System.out.println(loc.getLatitude() + ", " + loc.getLongitude());
                positionText.setText("Position: " + address + ", " + city + "\n" + country + "\n" + postalCode);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    public void setLocalization() {
        handler.post(() -> {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                System.out.println("Passed authorization");
                LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                Location loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER); // no effort since we use an already know location
                // getLastKnownLocation is not precise enough, it could register a location from more than 10 hours ago if its taken from GPS_PROVIDER
                // if it's taken from NETWORK_PROVIDER then it's okay.

                if (loc != null) {
                    treatMessage(loc);
                    return;
                }
                // we must ask Android to watch for the location
                LocationListener listener = new LocationListener() {

                    @Override
                    public void onLocationChanged(Location location) {
                        System.out.println("Location changed!!");
                        treatMessage(location);

                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {
                        System.out.println("status changed");
                    }

                    @Override
                    public void onProviderEnabled(String provider) {
                        System.out.println("provider enabled");
                    }

                    @Override
                    public void onProviderDisabled(String provider) {
                        System.out.println("provider disabled");
                    }
                };

                locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, listener, null /* using main thread */);
                handler.postDelayed(() -> {
                    System.out.println("DELAYED 1");
                        System.out.println("NO TREATED MESSAGE");
                        locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, listener, null /* using main thread */);
                        System.out.println("STARTING NETWORK PROVIDER");
                        handler.postDelayed(() -> {
                            treatMessage(null);
                        }, LOCATION_TIMEOUT);
                }, LOCATION_TIMEOUT);

            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sp.edit();
        boolean geoActive = sp.getBoolean("filterLocalization", false);
        ObjectMapper objectMapper = new ObjectMapper();
        AppUser user;
        positionText = getView().findViewById(R.id.position);

        Switch filterLocalization = getView().findViewById(R.id.activateFilterLocalization);
        filterLocalization.setTextColor(Color.BLACK);
        if (geoActive){
            filterLocalization.setChecked(true);
            setLocalization();
        }
        filterLocalization.setOnClickListener((View w) -> {
            if (filterLocalization.isChecked()) {
                System.out.println("CHECKING PERMISSION!");
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // Permission is not granted
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, GEOLOCATION_REQUEST_CODE);
                    filterLocalization.toggle();
                    Toast.makeText(getActivity(), "Veuillez autoriser la géolocalisation et recliquer sur le bouton d'activation.",
                            Toast.LENGTH_SHORT).show();
                    editor.putBoolean("filterLocalization", true);

                }
                setLocalization();
                //check permission
            }
        });
        fullName = getView().findViewById(R.id.profilFullName);
        fullName.setGravity(Gravity.CENTER_HORIZONTAL);
        mail = getView().findViewById(R.id.profilMail);
        role = getView().findViewById(R.id.profilRole);
        unit = getView().findViewById(R.id.profilUnit);
        phoneNumber = getView().findViewById(R.id.profilPhoneNumber);


        HttpClientService service = new HttpClientService();
        try {
            JSONObject res = new JSONObject(service.get(getContext(), "getProfile"));
            user = objectMapper.readValue(res.toString(), AppUser.class);

        } catch (JSONException | IOException e) {
            LOG.severe("Exception occured " + e);
            return;
        }
        fullName.setText(user.getFirstName() + " " + user.getLastName());
        mail.setText("Email : " + user.getEmail());
        phoneNumber.setText("Phone number : " + user.getPhoneNumber());
        role.setText(   "Role : " + (user.getRole() == null ? UNDEFINED : user.getRole().getName()));
        unit.setText("Unit : " + (user.getUnit() == null ? UNDEFINED : user.getUnit().getName()));

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
