package com.vitalitec.lightmedclient;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.vitalitec.lightmedclient.models.Address;
import com.vitalitec.lightmedclient.models.MedicalFile;
import com.vitalitec.lightmedclient.models.Patient;
import com.vitalitec.lightmedclient.service.HttpClientService;
import com.vitalitec.lightmedclient.service.IMedicalFile;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DMPSearchResultsActivity extends AppCompatActivity {

    private List<MedicalFile> medicalFiles;
    private LinearLayout listDMP;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setTitle(getResources().getString(R.string.titleSearchResults));

        setContentView(R.layout.activity_dmp_search_results);
        listDMP = findViewById(R.id.listDMP);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);

        Bundle bundle = getIntent().getExtras();
        String id = bundle.getString("id");
        String secuSociale = bundle.getString("secuSociale");
        String firstName = bundle.getString("firstName");
        String lastName = bundle.getString("lastName");
        String phone = bundle.getString("phone");
        String address = bundle.getString("address");
        Boolean sex = bundle.containsKey("sex") ? bundle.getBoolean("sex") : null;
        Patient p = new Patient(secuSociale, lastName, firstName, sex,
                phone, null, new Address(address), null, null, null, null);

        FloatingActionButton createButton = findViewById(R.id.createButton);
        createButton.setOnClickListener(view -> {
            Intent i = new Intent(this, CreateMedicalFileActivity.class)
                    .putExtra("secuSociale", secuSociale)
                    .putExtra("lastName", lastName)
                    .putExtra("firstName", firstName)
                    .putExtra("phone", phone)
                    .putExtra("address", address);
            if (sex != null) {
                i.putExtra("sex", sex);
            }
            startActivity(i);
        });

        searchMedicalFilesByIdOrPatient(id, p);
    }

    private void fillListDmp() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        MedicalFileCardViewGenerator cardViewGenerator = new MedicalFileCardViewGenerator(this);
        if (medicalFiles == null || medicalFiles.isEmpty()) {
            View view = inflater.inflate(R.layout.search_medical_file_no_results, null);
            listDMP.addView(view);
        } else {
            for (MedicalFile medicalFile : medicalFiles) {
                View view = cardViewGenerator.generate(medicalFile);
                listDMP.addView(view);
            }
        }
    }

    private void searchMedicalFilesByIdOrPatient(String id, Patient patient) {
        if (ContentUtils.isNetworkAvailable(this)) {
            Retrofit retrofit = HttpClientService.getRetrofit(this);
            if (id != null && !id.isEmpty()) {
                searchMedicalFilesById(retrofit, Long.valueOf(id));
            } else {
                searchMedicalFilesByPatient(retrofit, patient);
            }
        } else {
            Snackbar.make(coordinatorLayout,
                    getResources().getString(R.string.connectionFailed),
                    Snackbar.LENGTH_LONG).show();
        }

    }

    private void searchMedicalFilesById(Retrofit retrofit, Long id) {

        IMedicalFile client = retrofit.create(IMedicalFile.class);
        Call<MedicalFile> call = client.findById(id);
        call.enqueue(new Callback<MedicalFile>() {
            @Override
            public void onResponse(Call<MedicalFile> call, Response<MedicalFile> response) {
                if (response.isSuccessful()) {
                    ArrayList<MedicalFile> list = new ArrayList<>();
                    list.add(response.body());
                    medicalFiles = list;
                    fillListDmp();
                }
            }

            @Override
            public void onFailure(Call<MedicalFile> call, Throwable t) {
                onError();
            }
        });
    }


    private void searchMedicalFilesByPatient(Retrofit retrofit, Patient patient) {
        IMedicalFile client = retrofit.create(IMedicalFile.class);
        Call<List<MedicalFile>> call = client.searchByPatient(patient);

        call.enqueue(new Callback<List<MedicalFile>>() {
            @Override
            public void onResponse(Call<List<MedicalFile>> call, Response<List<MedicalFile>> response) {
                if (response.isSuccessful()) {
                    medicalFiles = response.body();
                    fillListDmp();
                }
            }

            @Override
            public void onFailure(Call<List<MedicalFile>> call, Throwable t) {
                onError();
            }
        });
    }

    private void onError() {
        Snackbar.make(coordinatorLayout,
                getResources().getString(R.string.error),
                Snackbar.LENGTH_LONG).show();
        fillListDmp();
    }
}