package com.vitalitec.lightmedclient.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PostObjectTask extends AsyncTask<String, Void, String> {

    private final HttpClientService.CallbackFuture future = new HttpClientService.CallbackFuture();
    private final Context context;
    private final String cookie;

    PostObjectTask(Context context) {
        this.context = context;
        SharedPreferences sharedPref =  PreferenceManager.getDefaultSharedPreferences(context);
        cookie = sharedPref.getString("auth", "none");
    }

    @Override
    protected String doInBackground(String... strings) {
        String route = strings[0];
        String json = strings[1];
        OkHttpClient client = new OkHttpClient().newBuilder()
                .addInterceptor(chain -> {
                    final Request original = chain.request();
                    final Request authorized = original.newBuilder()
                            .addHeader("Cookie", cookie)
                            .build();

                    return chain.proceed(authorized);
                })
                .build();

        String url = String.format("%s%s", HttpClientService.REST_API_ADDRESS, route);
        System.out.println("Url: " + url);
        final MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        client.newCall(request).enqueue(future);

        try {
            return future.get(5, TimeUnit.SECONDS).body().string();
        } catch (InterruptedException | ExecutionException | IOException | TimeoutException e) {
            return "";
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}