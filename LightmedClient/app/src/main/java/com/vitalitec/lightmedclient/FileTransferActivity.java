package com.vitalitec.lightmedclient;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.button.MaterialButton;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.vitalitec.lightmedclient.service.UploadClient;
import com.vitalitec.lightmedclient.service.UploadResponse;

import java.io.File;
import java.net.ConnectException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.vitalitec.lightmedclient.ContentUtils.getPath;

public class FileTransferActivity extends AppCompatActivity {
    private final int STORAGE_REQUEST_CODE = 120;

    private boolean storagePermission;
    private Uri fileUri;
    private TextInputLayout filePath;
    private String path;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.file_transfer);

        filePath = findViewById(R.id.filePath);
        coordinatorLayout = findViewById(R.id.loginCoordinatorLayout);
        MaterialButton addButton = findViewById(R.id.addButton);
        addButton.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");
            startActivityForResult(intent, 2);
        });

        MaterialButton transferButton = findViewById(R.id.transferButton);
        transferButton.setOnClickListener(view -> {
            if(fileUri != null){
                uploadFile();
                fileUri = null;
                filePath.getEditText().setText("");
            }

        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_REQUEST_CODE);
        } else {
            storagePermission = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case STORAGE_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    storagePermission = true;
                } else {
                    storagePermission = false;
                }
                return;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        if (requestCode == 2 && resultCode == RESULT_OK) {
            if (resultData != null) {
                fileUri = resultData.getData();
                path = getPath(this, fileUri);
                filePath.getEditText().setText(path);

            }
        }
    }

    private void uploadFile(){
        if(ContentUtils.isNetworkAvailable((this))) {
            File originalFile = new File(path);

            RequestBody descriptionPart = RequestBody.create(MultipartBody.FORM, originalFile.getName());
            RequestBody filePart = RequestBody.create(MediaType.parse(getContentResolver().getType(fileUri)), originalFile);

            MultipartBody.Part file = MultipartBody.Part.createFormData("file", originalFile.getName(), filePart);
            Retrofit retrofit = new Retrofit.Builder().baseUrl("http://192.168.0.15:8080/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build(); //mettre l'adresse de son localhost

            UploadClient client = retrofit.create(UploadClient.class);

            Call<UploadResponse> call = client.uploadPhoto(descriptionPart, file);
            call.enqueue(new Callback<UploadResponse>() {
                @Override
                public void onResponse(Call<UploadResponse> call, Response<UploadResponse> response) {
                    UploadResponse uploadReponse = response.body();
                    if (response.code() == 200 && uploadReponse.getSuccess()) {
                        Toast.makeText(FileTransferActivity.this, "success", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(FileTransferActivity.this, uploadReponse.getCause(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<UploadResponse> call, Throwable t) {
                    Toast.makeText(FileTransferActivity.this, "ourge", Toast.LENGTH_SHORT).show();
                }

            });
        } else {
            Snackbar.make(coordinatorLayout,
                    getResources().getString(R.string.connectionFailed),
                    Snackbar.LENGTH_LONG).show();
        }


    }

}
