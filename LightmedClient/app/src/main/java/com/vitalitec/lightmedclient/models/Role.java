package com.vitalitec.lightmedclient.models;

import java.io.Serializable;
import java.util.List;


public class Role implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;

	private String name;
	
	private List<AppUser> users;
	
	public Role() {}

	public Role(String name, List<AppUser> users) {
		this.name = name;
		this.users = users;
	}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<AppUser> getUsers() {
		return users;
	}

	public void setUsers(List<AppUser> users) {
		this.users = users;
	}

}
