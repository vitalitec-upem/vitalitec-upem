package com.vitalitec.lightmedclient.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.vitalitec.lightmedclient.MedicalFileCardViewGenerator;
import com.vitalitec.lightmedclient.R;
import com.vitalitec.lightmedclient.RecentlyOpenedMedicalFiles;
import com.vitalitec.lightmedclient.models.AppUser;
import com.vitalitec.lightmedclient.models.MedicalFile;
import com.vitalitec.lightmedclient.service.CacheService;
import com.vitalitec.lightmedclient.service.HttpClientService;
import com.vitalitec.lightmedclient.service.IMedicalFile;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DashboardFragment extends Fragment {

    private List<MedicalFile> recentlyOpenedMedicalFile;

    public DashboardFragment() {
    }

    public static DashboardFragment newInstance() {
        return new DashboardFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    private void findAllMedicalFiles() {
        AppUser user = (AppUser) CacheService.getObject(Objects.requireNonNull(getActivity()).getApplicationContext(), "AppUser");
        if (!user.getRole().getName().equals("ROLE_ADMIN")) {
            LinearLayout recentlyOpened = getView().findViewById(R.id.recentlyOpened);
            recentlyOpenedMedicalFile = new RecentlyOpenedMedicalFiles(getActivity().getApplicationContext()).getMedicalFiles();
            MedicalFileCardViewGenerator cardViewGenerator = new MedicalFileCardViewGenerator(getActivity());
            recentlyOpenedMedicalFile.forEach(medicalFile -> {
                getView().findViewById(R.id.recentlyOpenedText).setVisibility(View.GONE);
                View card = cardViewGenerator.generate(medicalFile);
                synchronized (recentlyOpenedMedicalFile) {
                    recentlyOpened.addView(card);
                }
            });
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        LinearLayout recentlyOpened = getView().findViewById(R.id.recentlyOpened);
        recentlyOpened.removeViews(1, recentlyOpened.getChildCount() - 1);
        getView().findViewById(R.id.recentlyOpenedText).setVisibility(View.VISIBLE);
        findAllMedicalFiles();
        super.onResume();
    }
}
