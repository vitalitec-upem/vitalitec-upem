package com.vitalitec.lightmedclient;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.button.MaterialButton;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;

import com.vitalitec.lightmedclient.models.AppUser;
import com.vitalitec.lightmedclient.service.CacheService;
import com.vitalitec.lightmedclient.service.HttpClientService;

public class LoginActivity extends AppCompatActivity {

    private TextInputLayout usernameInput;
    private TextInputLayout passwordInput;
    private MaterialButton loginButton;
    private ProgressDialog dialog;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        dialog = new ProgressDialog(LoginActivity.this);
        usernameInput = findViewById(R.id.loginUsername);
        passwordInput = findViewById(R.id.loginPassword);
        loginButton = findViewById(R.id.loginButton);
        coordinatorLayout = findViewById(R.id.loginCoordinatorLayout);

        dialog.setIndeterminate(true);
        dialog.setMessage(getResources().getString(R.string.connection));

        loginButton.setOnClickListener(v -> login());
    }

    public void login() {

        if (!validate()) {
            return;
        }

        loginButton.setEnabled(false);
        dialog.show();

        new Handler().postDelayed(
                () -> {
                    String username = usernameInput.getEditText().getText().toString();
                    String password = passwordInput.getEditText().getText().toString();

                    AppUser user = new HttpClientService().loginRequest(getApplicationContext(), username, password);
                    if (user == null) {
                        onLoginFailed();
                    } else {
                        onLoginSuccess(user);
                    }
                    loginButton.setEnabled(true);
                    dialog.dismiss();
                }, 3000);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    public void onLoginSuccess(AppUser user) {
        Intent mainIntent = new Intent(this, MainActivity.class);
        CacheService.saveObject(this.getApplicationContext(), user, "AppUser");
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(mainIntent);
        finish();
    }

    public void onLoginFailed() {
        Snackbar.make(coordinatorLayout,
                getResources().getString(R.string.connectionFailed),
                Snackbar.LENGTH_LONG).show();
    }

    public boolean validate() {
        boolean valid = true;

        String username = usernameInput.getEditText().getText().toString();
        String password = passwordInput.getEditText().getText().toString();

        usernameInput.setError(null);
        passwordInput.setError(null);

        if (username.isEmpty()) {
            usernameInput.setError(getResources().getString(R.string.requiredField));
            valid = false;
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(username).matches()) {
            usernameInput.setError(getResources().getString(R.string.invalidAddress));
            valid = false;
        }
        if (password.isEmpty()) {
            passwordInput.setError(getResources().getString(R.string.requiredField));
            valid = false;
        }

        return valid;
    }
}
