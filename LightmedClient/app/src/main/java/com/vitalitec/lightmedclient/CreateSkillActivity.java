package com.vitalitec.lightmedclient;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.vitalitec.lightmedclient.models.Skill;
import com.vitalitec.lightmedclient.models.Unit;
import com.vitalitec.lightmedclient.service.HttpClientService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

public class CreateSkillActivity extends AppCompatActivity {

    HttpClientService service = new HttpClientService();
    TextInputEditText etSkill;
    CoordinatorLayout coordinatorLayout;
    Spinner spinnerCreateParentList;
    ObjectMapper objectMapper = new ObjectMapper();
    Unit unitParent = null;

    private <T> void setSpinner(int id, String url, Class<T> classType) {
        Spinner spinnerListAdminModel = findViewById(id);
        ArrayList<String> listAdminModel = new ArrayList<>();
        if(ContentUtils.isNetworkAvailable(this)){
            String response = service.get(this, url);
            System.out.println("URL: " + url + ", RESPONSE: " + response);
            try {
                System.out.println("RESPONSE: " + response);
                List<T> myObjects = objectMapper.readValue(response, new TypeReference<List<T>>(){});
                System.out.println(myObjects);
                for (T a : myObjects) {
                    LinkedHashMap<String, String> test = (LinkedHashMap)a;
                    listAdminModel.add(test.get("name"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            String[] arrRoles = new String[listAdminModel.size()];
            arrRoles = listAdminModel.toArray(arrRoles);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrRoles);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerListAdminModel.setAdapter(adapter);
        } else {
            Snackbar.make(coordinatorLayout,
                    getResources().getString(R.string.connectionFailed),
                    Snackbar.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_skill);

        etSkill = findViewById(R.id.SkillNameText);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        spinnerCreateParentList = findViewById(R.id.createParentList);
        setSpinner(R.id.createParentList, "admin/getAllServices", Unit.class);
        try {
            if (spinnerCreateParentList.getSelectedItem() != null)
                if(ContentUtils.isNetworkAvailable(this)) {
                    unitParent = objectMapper.readValue(service.getWithOneArg(this, "unit/getUnitByName", spinnerCreateParentList.getSelectedItem().toString()), Unit.class);
                } else {
                    Snackbar.make(coordinatorLayout,
                            getResources().getString(R.string.connectionFailed),
                            Snackbar.LENGTH_LONG).show();
                }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Button saveBtn = findViewById(R.id.createSkillSendBtn);
        saveBtn.setOnClickListener(v -> saveNewSkill(etSkill.getText().toString()));
    }

    private void saveNewSkill(String strSkill) {
        if (!ContentUtils.isNetworkAvailable(this)) {
            Toast.makeText(this, getResources().getString(R.string.noInternet),
                    Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            if (spinnerCreateParentList.getSelectedItem() != null) {
                unitParent = objectMapper.readValue(service.getWithOneArg(getApplicationContext(), "unit/getUnitByName", spinnerCreateParentList.getSelectedItem().toString()), Unit.class);
                System.out.println(unitParent.getName());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!validate()) {
            Toast.makeText(this, getResources().getString(R.string.errCreateUserForm),
                    Toast.LENGTH_SHORT).show();
            return;
        }
        Gson gson = new Gson();
        Skill skill = new Skill(strSkill, unitParent, new HashSet<>());
        if(ContentUtils.isNetworkAvailable(this)){
            String pwd = service.postObject(this, "admin/createSkill", gson.toJson(skill));
            System.out.println("response:" + pwd);
            Toast.makeText(this, getResources().getString(R.string.successfullyAdded),
                    Toast.LENGTH_SHORT).show();
        } else {
            Snackbar.make(coordinatorLayout,
                    getResources().getString(R.string.connectionFailed),
                    Snackbar.LENGTH_LONG).show();
        }
    }

    public boolean validate() {
        boolean valid = true;
        String skillName = etSkill.getText().toString();

        etSkill.setError(null);

        if (skillName.isEmpty()) {
            etSkill.setError(getResources().getString(R.string.requiredField));
            valid = false;
        }

        return valid;
    }
}