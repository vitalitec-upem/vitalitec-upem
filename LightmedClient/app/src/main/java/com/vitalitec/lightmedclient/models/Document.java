package com.vitalitec.lightmedclient.models;

import java.io.Serializable;

public class Document implements Serializable {
	
	private long id;
	
	private String path;
	private String type;
	
	private Act act;
	
	public Document() {}
	public Document(String path, String type, Act act) {
		super();
		this.path = path;
		this.type = type;
		this.act = act;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public Act getAct() {
		return act;
	}
	
	public void setAct(Act act) {
		this.act = act;
	}

}
