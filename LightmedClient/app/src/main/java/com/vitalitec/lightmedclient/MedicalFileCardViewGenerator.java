package com.vitalitec.lightmedclient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;

import com.vitalitec.lightmedclient.models.MedicalFile;

import java.util.Objects;

public class MedicalFileCardViewGenerator {

    private final Context context;
    private final LayoutInflater inflater;

    public MedicalFileCardViewGenerator(Context context) {
        this.context = Objects.requireNonNull(context);
        this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public View generate(MedicalFile medicalFile) {
        Long id = medicalFile.getId();
        View card = inflater.inflate(R.layout.card_dmp, null);
        ((AppCompatTextView) card.findViewById(R.id.CardTitle))
                .setText(medicalFile.getPatient().getFirstName() + " " + medicalFile.getPatient().getLastName());
        card.findViewById(R.id.card_layout).setTransitionName("transitionCard" + id);
        card.findViewById(R.id.dmp_image).setTransitionName("transitionImage" + id);
        card.setOnClickListener(generateCardOnClickListener(card, id));
        return card;
    }

    private View.OnClickListener generateCardOnClickListener(View card, Long id) {
        return v -> {
            Intent i = new Intent(this.context, DMPActivity.class)
                    .putExtra("transitionCardName", "transitionCard" + id)
                    .putExtra("transitionImageName", "transitionImage" + id)
                    .putExtra("transitionTextName", "transitionText" + id)
                    .putExtra("id", id);

            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(
                            (Activity) this.context,
                            Pair.create(card.findViewById(R.id.card_layout), "transitionCard" + id),
                            Pair.create(card.findViewById(R.id.dmp_image), "transitionImage" + id),
                            Pair.create(card.findViewById(R.id.CardTitle), "transitionText" + id)
                    );
            this.context.startActivity(i, options.toBundle());
        };
    }
}
