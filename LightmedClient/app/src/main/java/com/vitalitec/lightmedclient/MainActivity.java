package com.vitalitec.lightmedclient;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.vitalitec.lightmedclient.fragments.AdminFragment;
import com.vitalitec.lightmedclient.fragments.AgendaFragment;
import com.vitalitec.lightmedclient.fragments.DMPFragment;
import com.vitalitec.lightmedclient.fragments.DashboardFragment;
import com.vitalitec.lightmedclient.fragments.ProfileFragment;
import com.vitalitec.lightmedclient.models.AppUser;
import com.vitalitec.lightmedclient.service.CacheService;
import com.vitalitec.lightmedclient.service.HttpClientService;
import com.vitalitec.lightmedclient.service.LogoutClient;
import com.vitalitec.lightmedclient.service.LogoutRequest;
import com.vitalitec.lightmedclient.service.MessageNotificationService;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.vitalitec.lightmedclient.ContentUtils;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.vitalitec.lightmedclient.service.CacheService.deleteObject;

public class MainActivity extends AppCompatActivity {

    private Fragment active;
    private final FragmentManager fragmentManager = getSupportFragmentManager();
    private Toolbar toolbar;
    private BottomNavigationView navigation;
    private TextView toolbarTitle;
    public static final String CHANNEL_ID = "1234";
    private HashMap<Integer, Pair<Fragment, Integer>> pages = new HashMap<>();

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {;
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "NotificationChannel", importance);
            channel.setDescription("Channel of Notification app");
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager == null) return ;
            notificationManager.createNotificationChannel(channel);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createNotificationChannel();
        System.out.println("=++++STARTING SERVER+++++=");
        Intent serviceNotification = new Intent(this, MessageNotificationService.class);
        serviceNotification.setAction(MessageNotificationService.START_WATCH_ACTION);
        startService(serviceNotification);
        System.out.println("Service started");
        toolbarTitle = findViewById(R.id.toolbarTitle);
        toolbar = findViewById(R.id.toolbar);
        navigation = findViewById(R.id.navigation);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        pages.put(R.id.navigation_home, Pair.create(DashboardFragment.newInstance(), R.string.titleHome));
        pages.put(R.id.navigation_dmp, Pair.create(DMPFragment.newInstance(), R.string.titleDmp));
        pages.put(R.id.navigation_admin, Pair.create(AdminFragment.newInstance(), R.string.titleAdmin));
        // pages.put(R.id.navigation_agenda, Pair.create(AgendaFragment.newInstance(), R.string.titleAgenda));
        pages.put(R.id.navigation_profile, Pair.create(ProfileFragment.newInstance(), R.string.titleProfile));

        pages.values().forEach(fragmentIntegerPair ->
                fragmentManager.beginTransaction()
                        .add(R.id.frame_container, fragmentIntegerPair.first)
                        .hide(fragmentIntegerPair.first)
                        .commit()
        );

        active = pages.get(R.id.navigation_home).first;
        fragmentManager.beginTransaction().show(active).commit();
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_home);

        AppUser user = (AppUser)CacheService.getObject(this, "AppUser");
        if (!user.getRole().getName().equals("ROLE_ADMIN")) {
            navigation.getMenu().removeItem(R.id.navigation_admin);
        } else {
            navigation.getMenu().removeItem(R.id.navigation_dmp);
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        private void loadFragment(Fragment fragment) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.hide(active).show(fragment).commit();
            active = fragment;
        }

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Pair Fragment = pages.get(item.getItemId());
            toolbarTitle.setText((Integer) Fragment.second);
            loadFragment((Fragment) Fragment.first);
            return true;
        }
    };

    @Override
    public void onBackPressed() {
        if (R.id.navigation_home != navigation.getSelectedItemId()) {
            navigation.setSelectedItemId(R.id.navigation_home);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.logout:
                logout();
                break;
        }
        return true;
    }

    private void logout() {
        MessageNotificationService.stop(this);
        deleteObject(getApplicationContext(), "AppUser");
        Call<ResponseBody> call = LogoutRequest.createLogoutRequest();
        call.enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Intent activityIntent = new Intent(MainActivity.this, LoginActivity.class);
                activityIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(activityIntent);
                finish();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(MainActivity.this, getResources().getString(R.string.noInternet), Toast.LENGTH_SHORT).show();
                Intent activityIntent = new Intent(MainActivity.this, LoginActivity.class);
                activityIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(activityIntent);
                finish();
            }

        });
    }

}
