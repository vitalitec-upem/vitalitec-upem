package com.vitalitec.lightmedclient.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.vitalitec.lightmedclient.models.AppUser;
import com.vitalitec.lightmedclient.models.UserDetails;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


import static com.vitalitec.lightmedclient.service.HttpClientService.REST_API_ADDRESS;

public class LoginTask extends AsyncTask<String, Void, String> {

    private final OkHttpClient http = new OkHttpClient();
    private final HttpClientService.CallbackFuture future = new HttpClientService.CallbackFuture();
    private final Context context;

    LoginTask(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... strings) {
        String username = strings[0];
        String password = strings[1];
        String url = String.format("%slogin?username=%s&password=%s", HttpClientService.REST_API_ADDRESS, username, password);

        RequestBody body = new FormBody.Builder().build();
        final Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        http.newCall(request).enqueue(future);

        try {
            Response resp = future.get(5, TimeUnit.SECONDS);
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPref.edit();
            for (String cookie : resp.headers("Set-Cookie")) {
                if (cookie.contains("JSESSIONID")) {
                    System.out.println("added cookie: " + cookie);
                    editor.putString("auth", cookie);
                    editor.commit();
                }
            }
            return resp.body().string();
        } catch (InterruptedException | ExecutionException | IOException | TimeoutException e) {
            return "";
        }

        /*String username = strings[0];
        String password = strings[1];
        final HttpClientService.CallbackFuture future = new HttpClientService.CallbackFuture();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(REST_API_ADDRESS + "login/").addConverterFactory(GsonConverterFactory.create()).build();

        LightmedApiService service = retrofit.create(LightmedApiService.class);
        Call<AppUser> myUserCall = service.login(username, password);
        System.out.println("Starting request");
        myUserCall.enqueue(future);
        try {
            System.out.println("Request enqued");
            Response ret = future.get(5, TimeUnit.SECONDS);
            System.out.println("Request done");
            System.out.println(ret);
            System.out.println("Bye");
            return ret.body().toString();
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            return "";
        }*/
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
