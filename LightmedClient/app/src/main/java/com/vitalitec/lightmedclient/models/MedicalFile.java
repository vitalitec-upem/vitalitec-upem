package com.vitalitec.lightmedclient.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class MedicalFile implements Serializable {
	
	private long id;
	private static final long serialVersionUID = 8958875808050070853L;
	private boolean published;
	private boolean archived;
	private Date archivedOn;
	private Date updatedOn;
	private Date createdOn;
	
	private Patient patient;
	
	private AppUser createdBy;
	
	private List<Act> acts;
	
	public MedicalFile() {}

	public MedicalFile(boolean published, boolean archived, Date archivedOn, Date updatedOn, Date createdOn, Patient patient, AppUser createdBy, List<Act> acts) {
		this.published = published;
		this.archived = archived;
		this.archivedOn = archivedOn;
		this.updatedOn = updatedOn;
		this.createdOn = createdOn;
		this.patient = patient;
		this.createdBy = createdBy;
		this.acts = acts;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isPublished() {
		return published;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public Date getArchivedOn() {
		return archivedOn;
	}

	public void setArchivedOn(Date archivedOn) {
		this.archivedOn = archivedOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public AppUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(AppUser createdBy) {
		this.createdBy = createdBy;
	}

	public List<Act> getActs() {
		return acts;
	}

	public void setActs(List<Act> acts) {
		this.acts = acts;
	}

	@Override
	public boolean equals(Object mf){
		if(mf == this){
			return true;
		}
		if(!(mf instanceof MedicalFile)){
			return false;
		}
		MedicalFile medicalFile = (MedicalFile) mf;
		return id == medicalFile.getId();
	}
}
