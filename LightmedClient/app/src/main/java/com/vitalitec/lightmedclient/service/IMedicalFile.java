package com.vitalitec.lightmedclient.service;

import com.vitalitec.lightmedclient.models.MedicalFile;
import com.vitalitec.lightmedclient.models.Patient;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface IMedicalFile {

    @POST("medicalFile/createMedicalFile")
    Call<Long> createMedicalFile(@Body Patient Patient);

    @GET("medicalFile/findById/{id}")
    Call<MedicalFile> findById(@Path("id") Long id);

    @POST("medicalFile/searchByPatient")
    Call<List<MedicalFile>> searchByPatient(@Body Patient patient);

    @POST("medicalFile/updatePatient")
    Call<Void> updatePatient(@Body Patient patient);
}
