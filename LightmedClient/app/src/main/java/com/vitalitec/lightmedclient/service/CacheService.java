package com.vitalitec.lightmedclient.service;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class CacheService {

    public static void saveObject(Context mContext, Object object, String className) {
        File file = new File(mContext.getCacheDir(), className);
        FileOutputStream outputStream = null;

        try {
            outputStream = mContext.openFileOutput(className, Context.MODE_PRIVATE);
            ObjectOutputStream dout = new ObjectOutputStream(outputStream);
            dout.writeObject(object);
            dout.flush();
            outputStream.getFD().sync();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Object getObject(Context mContext, String className) {
        FileInputStream inputStream = null;
        try {
            inputStream = mContext.openFileInput(className);
            ObjectInputStream din = new ObjectInputStream(inputStream);
            Object ret = din.readObject();
            if (ret == null) {
                System.out.println("this is null");
            }
            inputStream.close();
            return ret;
        } catch(FileNotFoundException e) {
            System.out.println("No such file or directory: " + className);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void deleteObject(Context mContext, String className) {
        mContext.deleteFile(className);
    }
}
