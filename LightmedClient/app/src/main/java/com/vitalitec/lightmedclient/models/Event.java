package com.vitalitec.lightmedclient.models;

import java.util.List;

public class Event {
	
	public Long id;
	public String uid;
	public List<AppUser> users;
	public Patient patient;
	
	public Event(Long id, String uid, List<AppUser> users, Patient patient) {
		this.id = id;
		this.uid = uid;
		this.users = users;
		this.patient = patient;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getUid() {
		return uid;
	}
	
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public List<AppUser> getUsers() {
		return users;
	}
	
	public void setUsers(List<AppUser> users) {
		this.users = users;
	}
	
	public Patient getPatient() {
		return patient;
	}
	
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	
	
}
