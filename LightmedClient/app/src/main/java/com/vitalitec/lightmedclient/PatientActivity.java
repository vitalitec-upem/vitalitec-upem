package com.vitalitec.lightmedclient;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.vitalitec.lightmedclient.models.Address;
import com.vitalitec.lightmedclient.models.Patient;
import com.vitalitec.lightmedclient.service.HttpClientService;
import com.vitalitec.lightmedclient.service.IMedicalFile;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PatientActivity extends AppCompatActivity {

    private CoordinatorLayout coordinatorLayout;
    FloatingActionButton saveButton;
    private EditText socialSec;
    private EditText firstName;
    private EditText lastName;
    private EditText phone;
    private EditText description;
    private Boolean sex = null;
    private Long id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient);
        getSupportActionBar().setTitle(
                getResources().getString(R.string.patientActivityTitle)
        );
        Bundle bundle = getIntent().getExtras();
        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        id = bundle.getLong("id");
        socialSec = findViewById(R.id.socialSec);
        socialSec.setText(bundle.getString("socialSec"));
        firstName = findViewById(R.id.firstName);
        firstName.setText(bundle.getString("firstName"));
        lastName = findViewById(R.id.lastName);
        lastName.setText(bundle.getString("lastName"));
        phone = findViewById(R.id.phone);
        phone.setText(bundle.getString("phone"));
        description = findViewById(R.id.description);
        description.setText(bundle.getString("description"));
        RadioGroup radioGroup = findViewById(R.id.radioGroup);
        if (bundle.containsKey("sex")) {
            if (bundle.getBoolean("sex") == Patient.MALE) {
                radioGroup.check(R.id.radioMale);
                sex = Patient.MALE;
            } else {
                radioGroup.check(R.id.radioFemale);
                sex = Patient.FEMALE;
            }
        }
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.radioUnknown:
                    sex = null;
                    break;
                case R.id.radioMale:
                    sex = Patient.MALE;
                    break;
                case R.id.radioFemale:
                    sex = Patient.FEMALE;
                    break;
            }
        });
        saveButton = findViewById(R.id.saveButton);
        saveButton.setOnClickListener(v -> {
            Patient patient = new Patient(socialSec.getText().toString(), lastName.getText().toString(),
                    firstName.getText().toString(), sex, phone.getText().toString(), description.getText().toString(),
                    new Address(""), null, null, null, null);
            patient.setId(id);
            savePatient(patient);
        });
    }

    private void savePatient(Patient patient) {
        if (ContentUtils.isNetworkAvailable(this)) {
            Retrofit retrofit = HttpClientService.getRetrofit(this);
            IMedicalFile client = retrofit.create(IMedicalFile.class);
            Call<Void> call = client.updatePatient(patient);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.isSuccessful()) {
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Snackbar.make(coordinatorLayout,
                            getResources().getString(R.string.medicalFileLoadingFailed),
                            Snackbar.LENGTH_LONG).show();
                }
            });
        } else {
            Snackbar.make(coordinatorLayout,
                    getResources().getString(R.string.noInternet),
                    Snackbar.LENGTH_LONG).show();
            saveButton.hide();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }
}
