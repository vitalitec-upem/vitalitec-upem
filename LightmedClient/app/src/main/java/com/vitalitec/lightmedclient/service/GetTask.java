package com.vitalitec.lightmedclient.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class GetTask extends AsyncTask<String, Void, String> {

    private final HttpClientService.CallbackFuture future = new HttpClientService.CallbackFuture();
    private final Context context;
    private final String cookie;

    GetTask(Context context) {
        this.context = context;
        SharedPreferences sharedPref =  PreferenceManager.getDefaultSharedPreferences(context);
        cookie = sharedPref.getString("auth", "none");
    }

    @Override
    protected String doInBackground(String... strings) {
        String route = strings[0];
        String url = String.format("%s%s", HttpClientService.REST_API_ADDRESS, route);
        OkHttpClient client = new OkHttpClient().newBuilder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        final Request original = chain.request();
                        System.out.println("Using cookie: " + cookie);
                        final Request authorized = original.newBuilder()
                                .addHeader("Cookie", cookie)
                                .build();

                        return chain.proceed(authorized);
                    }
                })
                .build();

        final Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        client.newCall(request).enqueue(future);

        try {
            return future.get(5, TimeUnit.SECONDS).body().string();
        } catch (InterruptedException | ExecutionException | IOException | TimeoutException e) {
            return "";
        }

        /*String username = strings[0];
        String password = strings[1];
        final HttpClientService.CallbackFuture future = new HttpClientService.CallbackFuture();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(REST_API_ADDRESS + "login/").addConverterFactory(GsonConverterFactory.create()).build();

        LightmedApiService service = retrofit.create(LightmedApiService.class);
        Call<AppUser> myUserCall = service.login(username, password);
        System.out.println("Starting request");
        myUserCall.enqueue(future);
        try {
            System.out.println("Request enqued");
            Response ret = future.get(5, TimeUnit.SECONDS);
            System.out.println("Request done");
            System.out.println(ret);
            System.out.println("Bye");
            return ret.body().toString();
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            return "";
        }*/
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
