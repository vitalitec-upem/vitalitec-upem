package com.vitalitec.lightmedclient;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.vitalitec.lightmedclient.models.Address;
import com.vitalitec.lightmedclient.models.Patient;
import com.vitalitec.lightmedclient.models.Unit;
import com.vitalitec.lightmedclient.service.HttpClientService;
import com.vitalitec.lightmedclient.service.IMedicalFile;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CreateMedicalFileActivity extends AppCompatActivity {

    private CoordinatorLayout coordinatorLayout;
    private Boolean sex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setTitle(getResources().getString(R.string.titleCreateMedicalFile));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_create_medical_file);

        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        EditText socialNumber = ((TextInputLayout) findViewById(R.id.socialNumber)).getEditText();
        EditText firstName = ((TextInputLayout) findViewById(R.id.firstName)).getEditText();
        EditText lastName = ((TextInputLayout) findViewById(R.id.lastName)).getEditText();
        RadioGroup radioGroup = findViewById(R.id.radioGroup);
        EditText phoneNumber = ((TextInputLayout) findViewById(R.id.phoneNumber)).getEditText();
        EditText description = ((TextInputLayout) findViewById(R.id.description)).getEditText();
        EditText address = findViewById(R.id.place_autocomplete_search_input);
        FloatingActionButton createButton = findViewById(R.id.createButton);
        Bundle bundle = getIntent().getExtras();

        socialNumber.setText(bundle.getString("secuSociale", ""));
        firstName.setText(bundle.getString("firstName", ""));
        lastName.setText(bundle.getString("lastName", ""));
        phoneNumber.setText(bundle.getString("phone", ""));
        address.setText(bundle.getString("address", ""));
        address.setHint("Adresse");

        if (bundle.containsKey("sex")) {
            if (bundle.getBoolean("sex") == Patient.MALE) {
                radioGroup.check(R.id.radioMale);
                sex = Patient.MALE;
            } else {
                radioGroup.check(R.id.radioFemale);
                sex = Patient.FEMALE;
            }
        }
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.radioUnknown:
                    sex = null;
                    break;
                case R.id.radioMale:
                    sex = Patient.MALE;
                    break;
                case R.id.radioFemale:
                    sex = Patient.FEMALE;
                    break;
            }
        });

        createButton.setOnClickListener(view -> {
            Patient patient = new Patient(socialNumber.getText().toString(),
                    lastName.getText().toString(),
                    firstName.getText().toString(),
                    sex,
                    phoneNumber.getText().toString(),
                    description.getText().toString(),
                    new Address(address.getText().toString()),
                    new Unit(),
                    null,
                    null,
                    new ArrayList<>());
            createMedicalFile(patient);
        });
    }

    private void createMedicalFile(Patient patient) {
        Retrofit retrofit = HttpClientService.getRetrofit(this);
        IMedicalFile client = retrofit.create(IMedicalFile.class);
        Call<Long> call = client.createMedicalFile(patient);

        call.enqueue(new Callback<Long>() {
            @Override
            public void onResponse(Call<Long> call, Response<Long> response) {
                if (response.isSuccessful()) {
                    Intent i = new Intent(CreateMedicalFileActivity.this, DMPActivity.class);
                    i.putExtra("id", response.body());
                    startActivity(i);
                    finish();
                } else {
                    Snackbar.make(coordinatorLayout,
                            getResources().getString(R.string.creationFailed),
                            Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Long> call, Throwable t) {
                Snackbar.make(coordinatorLayout,
                        getResources().getString(R.string.creationFailed),
                        Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }
}
