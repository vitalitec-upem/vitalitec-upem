package com.vitalitec.lightmedclient;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.vitalitec.lightmedclient.models.AppUser;
import com.vitalitec.lightmedclient.service.CacheService;
import com.vitalitec.lightmedclient.service.HttpClientService;

public class StartActivity extends AppCompatActivity {

    HttpClientService service = new HttpClientService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start);
        new Handler().postDelayed(this::checkLogin, 500);
    }

    private void checkLogin() {
        Intent activityIntent;
        AppUser user = (AppUser) CacheService.getObject(this.getApplicationContext(), "AppUser");
        System.out.println("GETUSER ===============>");
        System.out.println(service.get(this, "getUser"));
        if (user != null) {
            activityIntent = new Intent(this, MainActivity.class);
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(activityIntent);
        } else {
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this,
                            findViewById(R.id.appLogo),
                            "appLogoTransition");
            activityIntent = new Intent(this, LoginActivity.class);
            startActivity(activityIntent, options.toBundle());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}