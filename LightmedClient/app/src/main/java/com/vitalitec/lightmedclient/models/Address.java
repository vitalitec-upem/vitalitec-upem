package com.vitalitec.lightmedclient.models;

import java.io.Serializable;

public class Address implements Serializable {
    private static final long serialVersionUID = 1L;

    private long id;
    private String num;
    private String street;
    private String city;
    private String zip;
    private String country;
    private String fullAddress;

    public Address() { }

    public Address(String num, String street, String city, String zip, String country, String fullAddress) {
        this.num = num;
        this.street = street;
        this.city = city;
        this.zip = zip;
        this.country = country;
        this.fullAddress = fullAddress;
    }

    public Address(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNum() { return num; }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getZip() {
        return zip;
    }

    public String getCountry() {
        return country;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getFullAddress() {
        if( fullAddress != null )
            return fullAddress;
        else
            return new StringBuilder().append(num).append(", ").append(street)
                    .append(", ").append(zip).append(", ").append(city).append(", ").append(country)
                    .toString();
    }
}
